#---
#title: "Feature Extraction"
#output: network features csv
#---

library('igraph')
library(dplyr)

data_input_dir <- '/home/g/gb/gb293/phd/data/networks/input'
data_output_dir <- '/home/g/gb/gb293/phd/data/networks/output'
organism_name = 'human'

## Read data
print('Reading data')
interactions_file = sprintf("%s/%s_interactions.csv", data_output_dir, organism_name)
data <- read.csv(interactions_file, sep = ',', header=T, as.is=T)

data <- data[data$experimental > 800,]
data$gene1 <- gsub('9606.', "", data$gene1)
data$gene2 <- gsub('9606.', "", data$gene2)
data <- data[c('gene1', 'gene2')]

print('Building graph')
nodes <- unique(c(data$gene1,  data$gene2))
ind_nodes <- seq(1, length(nodes))

ind_data <- data %>%
  rowwise() %>%
  mutate(g1_id = which(nodes == gene1), g2_id = which(nodes == gene2))

ind_data <- ind_data %>%
  select(c(g1_id, g2_id))
  
g <- graph_from_data_frame(d=data, vertices=nodes, directed=F)
g <- simplify(g)

E(g)$weight=as.numeric(1)

# get the subgraph correspondent to just the giant component
g.components <- clusters(g)
ix <- which.max(g.components$csize)
g <- induced.subgraph(g, which(g.components$membership == ix))

# Get just nth neighbours
vhl <- 'ENSG00000134086'
tp53 <- 'ENSG00000141510'
PTEN <- 'ENSG00000171862'
n <- unlist(neighborhood(g, 1, nodes=PTEN))
g2 <- induced.subgraph(graph=g,vids=n)
E(g2)$weight
V(g2)




## Calculate graphlets
gl <- graphlets(g2, niter=1000)

# ## Plot graphlets
# for (i in 1:length(gl$cliques)) {
#   glet <- induced.subgraph(graph=g2,vids=gl$cliques[[i]])
#   plot(glet, layout=co, vertex.label.dist=0.5, vertex.label.cex=0.7, vertex.size=5  )
# }

gl$cliques

layout(matrix(1:24, nrow=4, byrow=TRUE))
co <- layout_with_kk(g2)
par(mar=c(1,1,1,1))

## Plot graphlets
for (i in 1:length(gl$cliques)) {
  sel <- gl$cliques[[i]]
  V(g2)$color <- "white"
  V(g2)[sel]$color <- "#E495A5"
  V(g2)$label.color <- "white"
  V(g2)[sel]$label.color <- "black"
  V(g2)$label.cex <- 0.01
  V(g2)[sel]$label.cex <- 0.7
  E(g2)$width <- 0
  E(g2)[ V(g2)[sel] %--% V(g2)[sel] ]$width <- 1
  E(g2)$label <- ""
  #E(g2)[ width == 2 ]$label <- round(gl$Mu[i], 2)
  #E(g2)$color <- "white"
  E(g2)[ width == 1 ]$color <- "#E495A5"
  plot(g2, layout=co, vertex.label.dist=0.5, vertex.size=5  )
}


