library(igraph)
library(dplyr)
data_dir <- '/home/g/gb/gb293/phd/data/slorth'

# Preprocessing

source('./preprocessing/process_pombe_id_map.R')
source('./preprocessing/process_sl_files.R')
source('./preprocessing/all_sl_genes_list.R')
source('./preprocessing/make_gene_list.R')
source('./preprocessing/go_term_features.R')

# Processing features

source('./feature_processing/process_features.R')
