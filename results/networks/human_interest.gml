Creator "igraph version @VERSION@ Mon Jul 10 12:03:20 2017"
Version 1
graph
[
  directed 0
  node
  [
    id 0
    name "SMC6"
  ]
  node
  [
    id 1
    name "FEN1"
  ]
  node
  [
    id 2
    name "BLM"
  ]
  node
  [
    id 3
    name "PBRM1"
  ]
  node
  [
    id 4
    name "ERBB2"
  ]
  node
  [
    id 5
    name "SKP2"
  ]
  node
  [
    id 6
    name "ZRANB1"
  ]
  node
  [
    id 7
    name "RFC1"
  ]
  node
  [
    id 8
    name "TGFBR1"
  ]
  node
  [
    id 9
    name "MUS81"
  ]
  node
  [
    id 10
    name "AKT1"
  ]
  node
  [
    id 11
    name "TPR"
  ]
  node
  [
    id 12
    name "CHEK2"
  ]
  node
  [
    id 13
    name "EPS15"
  ]
  node
  [
    id 14
    name "EXOSC10"
  ]
  node
  [
    id 15
    name "PAK1"
  ]
  node
  [
    id 16
    name "HSPD1"
  ]
  node
  [
    id 17
    name "POLR1A"
  ]
  node
  [
    id 18
    name "CHAF1A"
  ]
  node
  [
    id 19
    name "RAD18"
  ]
  node
  [
    id 20
    name "KPNB1"
  ]
  node
  [
    id 21
    name "HRAS"
  ]
  node
  [
    id 22
    name "PRKACA"
  ]
  node
  [
    id 23
    name "RPF1"
  ]
  node
  [
    id 24
    name "RB1"
  ]
  node
  [
    id 25
    name "UBA1"
  ]
  node
  [
    id 26
    name "DMC1"
  ]
  node
  [
    id 27
    name "KRAS"
  ]
  node
  [
    id 28
    name "RAF1"
  ]
  node
  [
    id 29
    name "CDC7"
  ]
  node
  [
    id 30
    name "KDM1A"
  ]
  node
  [
    id 31
    name "TAF1"
  ]
  node
  [
    id 32
    name "HDAC2"
  ]
  node
  [
    id 33
    name "TNPO1"
  ]
  node
  [
    id 34
    name "ITGB1"
  ]
  node
  [
    id 35
    name "JUNB"
  ]
  node
  [
    id 36
    name "YWHAB"
  ]
  node
  [
    id 37
    name "IQGAP1"
  ]
  node
  [
    id 38
    name "TP53BP2"
  ]
  node
  [
    id 39
    name "BUB1B"
  ]
  node
  [
    id 40
    name "AMFR"
  ]
  node
  [
    id 41
    name "QARS"
  ]
  node
  [
    id 42
    name "CCT3"
  ]
  node
  [
    id 43
    name "PXN"
  ]
  node
  [
    id 44
    name "CDH1"
  ]
  node
  [
    id 45
    name "UBA2"
  ]
  node
  [
    id 46
    name "TBL3"
  ]
  node
  [
    id 47
    name "SMARCE1"
  ]
  node
  [
    id 48
    name "AURKB"
  ]
  node
  [
    id 49
    name "NAT10"
  ]
  node
  [
    id 50
    name "BMS1"
  ]
  node
  [
    id 51
    name "PDS5B"
  ]
  node
  [
    id 52
    name "CDC25A"
  ]
  node
  [
    id 53
    name "RUNX1"
  ]
  node
  [
    id 54
    name "JUN"
  ]
  node
  [
    id 55
    name "PINK1"
  ]
  node
  [
    id 56
    name "MCM7"
  ]
  node
  [
    id 57
    name "APC"
  ]
  node
  [
    id 58
    name "BRCA2"
  ]
  node
  [
    id 59
    name "EPAS1"
  ]
  node
  [
    id 60
    name "SAP18"
  ]
  node
  [
    id 61
    name "UBE2G2"
  ]
  node
  [
    id 62
    name "SF3B2"
  ]
  node
  [
    id 63
    name "HCK"
  ]
  node
  [
    id 64
    name "RAD1"
  ]
  node
  [
    id 65
    name "CPSF2"
  ]
  node
  [
    id 66
    name "BECN1"
  ]
  node
  [
    id 67
    name "MAPK1"
  ]
  node
  [
    id 68
    name "CDK2"
  ]
  node
  [
    id 69
    name "MAP2K1"
  ]
  node
  [
    id 70
    name "MEN1"
  ]
  node
  [
    id 71
    name "WWP1"
  ]
  node
  [
    id 72
    name "ETS1"
  ]
  node
  [
    id 73
    name "GRB2"
  ]
  node
  [
    id 74
    name "CHTF18"
  ]
  node
  [
    id 75
    name "TRIP12"
  ]
  node
  [
    id 76
    name "VHL"
  ]
  node
  [
    id 77
    name "CDC16"
  ]
  node
  [
    id 78
    name "FGFR2"
  ]
  node
  [
    id 79
    name "CCNA2"
  ]
  node
  [
    id 80
    name "FANCA"
  ]
  node
  [
    id 81
    name "UBR5"
  ]
  node
  [
    id 82
    name "EP400"
  ]
  node
  [
    id 83
    name "LARS"
  ]
  node
  [
    id 84
    name "SH3KBP1"
  ]
  node
  [
    id 85
    name "MORF4L2"
  ]
  node
  [
    id 86
    name "DYNLL1"
  ]
  node
  [
    id 87
    name "MMS19"
  ]
  node
  [
    id 88
    name "EIF3A"
  ]
  node
  [
    id 89
    name "ERCC4"
  ]
  node
  [
    id 90
    name "NUP85"
  ]
  node
  [
    id 91
    name "SF3A3"
  ]
  node
  [
    id 92
    name "DDB1"
  ]
  node
  [
    id 93
    name "CKS1B"
  ]
  node
  [
    id 94
    name "BUB1"
  ]
  node
  [
    id 95
    name "RIOK2"
  ]
  node
  [
    id 96
    name "NCL"
  ]
  node
  [
    id 97
    name "RAE1"
  ]
  node
  [
    id 98
    name "TRAF6"
  ]
  node
  [
    id 99
    name "ING4"
  ]
  node
  [
    id 100
    name "CDC40"
  ]
  node
  [
    id 101
    name "SUPT16H"
  ]
  node
  [
    id 102
    name "PRPF19"
  ]
  node
  [
    id 103
    name "TCEB1"
  ]
  node
  [
    id 104
    name "SMARCA1"
  ]
  node
  [
    id 105
    name "RUNX2"
  ]
  node
  [
    id 106
    name "ORC1"
  ]
  node
  [
    id 107
    name "POLE"
  ]
  node
  [
    id 108
    name "KAT7"
  ]
  node
  [
    id 109
    name "UBE2D3"
  ]
  node
  [
    id 110
    name "JAK2"
  ]
  node
  [
    id 111
    name "RARS"
  ]
  node
  [
    id 112
    name "COPS2"
  ]
  node
  [
    id 113
    name "MAP3K4"
  ]
  node
  [
    id 114
    name "CCT2"
  ]
  node
  [
    id 115
    name "BRCA1"
  ]
  node
  [
    id 116
    name "RCHY1"
  ]
  node
  [
    id 117
    name "MCM4"
  ]
  node
  [
    id 118
    name "NFE2L2"
  ]
  node
  [
    id 119
    name "SRSF6"
  ]
  node
  [
    id 120
    name "GSK3B"
  ]
  node
  [
    id 121
    name "RAD17"
  ]
  node
  [
    id 122
    name "SUV39H1"
  ]
  node
  [
    id 123
    name "STAT1"
  ]
  node
  [
    id 124
    name "UBE2I"
  ]
  node
  [
    id 125
    name "POLA1"
  ]
  node
  [
    id 126
    name "RBM22"
  ]
  node
  [
    id 127
    name "PDGFRA"
  ]
  node
  [
    id 128
    name "IGF1R"
  ]
  node
  [
    id 129
    name "KARS"
  ]
  node
  [
    id 130
    name "HDAC1"
  ]
  node
  [
    id 131
    name "ZPR1"
  ]
  node
  [
    id 132
    name "PSEN1"
  ]
  node
  [
    id 133
    name "EIF4A3"
  ]
  node
  [
    id 134
    name "SPATA5"
  ]
  node
  [
    id 135
    name "ARRB2"
  ]
  node
  [
    id 136
    name "ASF1A"
  ]
  node
  [
    id 137
    name "CARM1"
  ]
  node
  [
    id 138
    name "MRPL15"
  ]
  node
  [
    id 139
    name "THOC1"
  ]
  node
  [
    id 140
    name "SUPT6H"
  ]
  node
  [
    id 141
    name "YWHAZ"
  ]
  node
  [
    id 142
    name "PRKCD"
  ]
  node
  [
    id 143
    name "POLR2J"
  ]
  node
  [
    id 144
    name "IARS"
  ]
  node
  [
    id 145
    name "EEF2"
  ]
  node
  [
    id 146
    name "RAD52"
  ]
  node
  [
    id 147
    name "NXF1"
  ]
  node
  [
    id 148
    name "WEE1"
  ]
  node
  [
    id 149
    name "FBXW7"
  ]
  node
  [
    id 150
    name "EIF3B"
  ]
  node
  [
    id 151
    name "COPS5"
  ]
  node
  [
    id 152
    name "MET"
  ]
  node
  [
    id 153
    name "SKP1"
  ]
  node
  [
    id 154
    name "PPP1CC"
  ]
  node
  [
    id 155
    name "HSPA9"
  ]
  node
  [
    id 156
    name "RAB1A"
  ]
  node
  [
    id 157
    name "CCT7"
  ]
  node
  [
    id 158
    name "MSH6"
  ]
  node
  [
    id 159
    name "ATM"
  ]
  node
  [
    id 160
    name "PSEN2"
  ]
  node
  [
    id 161
    name "MAX"
  ]
  node
  [
    id 162
    name "PLK1"
  ]
  node
  [
    id 163
    name "EPRS"
  ]
  node
  [
    id 164
    name "RAB11A"
  ]
  node
  [
    id 165
    name "ACTL6A"
  ]
  node
  [
    id 166
    name "RBX1"
  ]
  node
  [
    id 167
    name "CLNS1A"
  ]
  node
  [
    id 168
    name "STIP1"
  ]
  node
  [
    id 169
    name "HMGB2"
  ]
  node
  [
    id 170
    name "SMAD1"
  ]
  node
  [
    id 171
    name "G3BP1"
  ]
  node
  [
    id 172
    name "NPLOC4"
  ]
  node
  [
    id 173
    name "MAP3K1"
  ]
  node
  [
    id 174
    name "RNF20"
  ]
  node
  [
    id 175
    name "MARK2"
  ]
  node
  [
    id 176
    name "SNRPB2"
  ]
  node
  [
    id 177
    name "WRNIP1"
  ]
  node
  [
    id 178
    name "LATS2"
  ]
  node
  [
    id 179
    name "PRKCI"
  ]
  node
  [
    id 180
    name "FBL"
  ]
  node
  [
    id 181
    name "CDC5L"
  ]
  node
  [
    id 182
    name "RPS6KA1"
  ]
  node
  [
    id 183
    name "CDK9"
  ]
  node
  [
    id 184
    name "DDX56"
  ]
  node
  [
    id 185
    name "SRSF1"
  ]
  node
  [
    id 186
    name "EGFR"
  ]
  node
  [
    id 187
    name "SNRNP70"
  ]
  node
  [
    id 188
    name "CDC27"
  ]
  node
  [
    id 189
    name "UBE2V2"
  ]
  node
  [
    id 190
    name "ZC3H15"
  ]
  node
  [
    id 191
    name "SHC1"
  ]
  node
  [
    id 192
    name "RPS6KB1"
  ]
  node
  [
    id 193
    name "EIF4A1"
  ]
  node
  [
    id 194
    name "ABL1"
  ]
  node
  [
    id 195
    name "CDT1"
  ]
  node
  [
    id 196
    name "UBE2M"
  ]
  node
  [
    id 197
    name "CDK6"
  ]
  node
  [
    id 198
    name "XPO1"
  ]
  node
  [
    id 199
    name "USP10"
  ]
  node
  [
    id 200
    name "TRIP13"
  ]
  node
  [
    id 201
    name "CDK4"
  ]
  node
  [
    id 202
    name "ERCC5"
  ]
  node
  [
    id 203
    name "PARD3"
  ]
  node
  [
    id 204
    name "DIS3"
  ]
  node
  [
    id 205
    name "UBTF"
  ]
  node
  [
    id 206
    name "MLH1"
  ]
  node
  [
    id 207
    name "ARPC2"
  ]
  node
  [
    id 208
    name "SNRPD2"
  ]
  node
  [
    id 209
    name "RRP12"
  ]
  node
  [
    id 210
    name "DCAF13"
  ]
  node
  [
    id 211
    name "ARID1A"
  ]
  node
  [
    id 212
    name "DKC1"
  ]
  node
  [
    id 213
    name "MAP4K1"
  ]
  node
  [
    id 214
    name "VPS35"
  ]
  node
  [
    id 215
    name "COPS3"
  ]
  node
  [
    id 216
    name "CTNNA1"
  ]
  node
  [
    id 217
    name "UBE2B"
  ]
  node
  [
    id 218
    name "SNRPF"
  ]
  node
  [
    id 219
    name "CANX"
  ]
  node
  [
    id 220
    name "HSP90AA1"
  ]
  node
  [
    id 221
    name "CUL4B"
  ]
  node
  [
    id 222
    name "MYC"
  ]
  node
  [
    id 223
    name "UTP18"
  ]
  node
  [
    id 224
    name "PMS2"
  ]
  node
  [
    id 225
    name "ANAPC11"
  ]
  node
  [
    id 226
    name "PKMYT1"
  ]
  node
  [
    id 227
    name "SOS1"
  ]
  node
  [
    id 228
    name "COPS6"
  ]
  node
  [
    id 229
    name "RBMX"
  ]
  node
  [
    id 230
    name "CDKN1B"
  ]
  node
  [
    id 231
    name "NOP56"
  ]
  node
  [
    id 232
    name "JAK1"
  ]
  node
  [
    id 233
    name "DNM1"
  ]
  node
  [
    id 234
    name "DNA2"
  ]
  node
  [
    id 235
    name "POLR3B"
  ]
  node
  [
    id 236
    name "EXOSC2"
  ]
  node
  [
    id 237
    name "HNRNPM"
  ]
  node
  [
    id 238
    name "CTDP1"
  ]
  node
  [
    id 239
    name "ACTR2"
  ]
  node
  [
    id 240
    name "STK26"
  ]
  node
  [
    id 241
    name "SNRPG"
  ]
  node
  [
    id 242
    name "PARP1"
  ]
  node
  [
    id 243
    name "RHOA"
  ]
  node
  [
    id 244
    name "SMAD4"
  ]
  node
  [
    id 245
    name "NCOR1"
  ]
  node
  [
    id 246
    name "SLC9A3R1"
  ]
  node
  [
    id 247
    name "ATP6V1F"
  ]
  node
  [
    id 248
    name "KMT2D"
  ]
  node
  [
    id 249
    name "VPS25"
  ]
  node
  [
    id 250
    name "UBE2D1"
  ]
  node
  [
    id 251
    name "JUND"
  ]
  node
  [
    id 252
    name "RFWD2"
  ]
  node
  [
    id 253
    name "SNRPA"
  ]
  node
  [
    id 254
    name "ARPC3"
  ]
  node
  [
    id 255
    name "CDC73"
  ]
  node
  [
    id 256
    name "WDR3"
  ]
  node
  [
    id 257
    name "CREBBP"
  ]
  node
  [
    id 258
    name "NCAPD2"
  ]
  node
  [
    id 259
    name "BRAF"
  ]
  node
  [
    id 260
    name "RNF40"
  ]
  node
  [
    id 261
    name "SP1"
  ]
  node
  [
    id 262
    name "FLNC"
  ]
  node
  [
    id 263
    name "SNW1"
  ]
  node
  [
    id 264
    name "SAE1"
  ]
  node
  [
    id 265
    name "ARID1B"
  ]
  node
  [
    id 266
    name "ASF1B"
  ]
  node
  [
    id 267
    name "MSN"
  ]
  node
  [
    id 268
    name "PEX14"
  ]
  node
  [
    id 269
    name "PPP2R5D"
  ]
  node
  [
    id 270
    name "PPP1R13B"
  ]
  node
  [
    id 271
    name "GAK"
  ]
  node
  [
    id 272
    name "RRP9"
  ]
  node
  [
    id 273
    name "SUMO2"
  ]
  node
  [
    id 274
    name "CDK1"
  ]
  node
  [
    id 275
    name "CBFA2T3"
  ]
  node
  [
    id 276
    name "SSRP1"
  ]
  node
  [
    id 277
    name "UTP20"
  ]
  node
  [
    id 278
    name "SNAP23"
  ]
  node
  [
    id 279
    name "NFKB1"
  ]
  node
  [
    id 280
    name "BIRC5"
  ]
  node
  [
    id 281
    name "SMARCB1"
  ]
  node
  [
    id 282
    name "BIRC6"
  ]
  node
  [
    id 283
    name "CSNK2A1"
  ]
  node
  [
    id 284
    name "SRSF5"
  ]
  node
  [
    id 285
    name "USP8"
  ]
  node
  [
    id 286
    name "BRD4"
  ]
  node
  [
    id 287
    name "TOP2A"
  ]
  node
  [
    id 288
    name "EEF1A1"
  ]
  node
  [
    id 289
    name "NOTCH1"
  ]
  node
  [
    id 290
    name "PPP1R8"
  ]
  node
  [
    id 291
    name "RAN"
  ]
  node
  [
    id 292
    name "SREBF1"
  ]
  node
  [
    id 293
    name "CNOT1"
  ]
  node
  [
    id 294
    name "EGR1"
  ]
  node
  [
    id 295
    name "CHEK1"
  ]
  node
  [
    id 296
    name "AP1B1"
  ]
  node
  [
    id 297
    name "REV1"
  ]
  node
  [
    id 298
    name "PRDX1"
  ]
  node
  [
    id 299
    name "RPA2"
  ]
  node
  [
    id 300
    name "SMARCA2"
  ]
  node
  [
    id 301
    name "PPP3CA"
  ]
  node
  [
    id 302
    name "EIF5B"
  ]
  node
  [
    id 303
    name "KIT"
  ]
  node
  [
    id 304
    name "VPS4A"
  ]
  node
  [
    id 305
    name "SMG1"
  ]
  node
  [
    id 306
    name "RPTOR"
  ]
  node
  [
    id 307
    name "PIN1"
  ]
  node
  [
    id 308
    name "FAF2"
  ]
  node
  [
    id 309
    name "SRSF9"
  ]
  node
  [
    id 310
    name "UBE4B"
  ]
  node
  [
    id 311
    name "FANCD2"
  ]
  node
  [
    id 312
    name "CBX1"
  ]
  node
  [
    id 313
    name "USP2"
  ]
  node
  [
    id 314
    name "UBA3"
  ]
  node
  [
    id 315
    name "ARHGDIA"
  ]
  node
  [
    id 316
    name "DCUN1D1"
  ]
  node
  [
    id 317
    name "SFN"
  ]
  node
  [
    id 318
    name "PPP2CB"
  ]
  node
  [
    id 319
    name "GNL3L"
  ]
  node
  [
    id 320
    name "SMARCC1"
  ]
  node
  [
    id 321
    name "CASP8"
  ]
  node
  [
    id 322
    name "ING5"
  ]
  node
  [
    id 323
    name "TAF5L"
  ]
  node
  [
    id 324
    name "CALR"
  ]
  node
  [
    id 325
    name "TLK2"
  ]
  node
  [
    id 326
    name "BCL2"
  ]
  node
  [
    id 327
    name "HUWE1"
  ]
  node
  [
    id 328
    name "RAD23A"
  ]
  node
  [
    id 329
    name "UBB"
  ]
  node
  [
    id 330
    name "PSMA5"
  ]
  node
  [
    id 331
    name "SRC"
  ]
  node
  [
    id 332
    name "CRY2"
  ]
  node
  [
    id 333
    name "FBXW11"
  ]
  node
  [
    id 334
    name "NUP93"
  ]
  node
  [
    id 335
    name "TBK1"
  ]
  node
  [
    id 336
    name "SEC61G"
  ]
  node
  [
    id 337
    name "HGS"
  ]
  node
  [
    id 338
    name "NDC80"
  ]
  node
  [
    id 339
    name "NCOR2"
  ]
  node
  [
    id 340
    name "SUMO3"
  ]
  node
  [
    id 341
    name "FANCM"
  ]
  node
  [
    id 342
    name "SNAI1"
  ]
  node
  [
    id 343
    name "ZC3H13"
  ]
  node
  [
    id 344
    name "TAF6"
  ]
  node
  [
    id 345
    name "GNL3"
  ]
  node
  [
    id 346
    name "NSMCE1"
  ]
  node
  [
    id 347
    name "SENP3"
  ]
  node
  [
    id 348
    name "TAF12"
  ]
  node
  [
    id 349
    name "POLR2F"
  ]
  node
  [
    id 350
    name "STRN"
  ]
  node
  [
    id 351
    name "ACTA1"
  ]
  node
  [
    id 352
    name "USP1"
  ]
  node
  [
    id 353
    name "WDR36"
  ]
  node
  [
    id 354
    name "PTEN"
  ]
  node
  [
    id 355
    name "GRK2"
  ]
  node
  [
    id 356
    name "SKIL"
  ]
  node
  [
    id 357
    name "NOC2L"
  ]
  node
  [
    id 358
    name "ACTR5"
  ]
  node
  [
    id 359
    name "RELA"
  ]
  node
  [
    id 360
    name "TAF9"
  ]
  node
  [
    id 361
    name "LYN"
  ]
  node
  [
    id 362
    name "PRKDC"
  ]
  node
  [
    id 363
    name "ERCC6"
  ]
  node
  [
    id 364
    name "VAMP2"
  ]
  node
  [
    id 365
    name "CSNK1E"
  ]
  node
  [
    id 366
    name "SUPT4H1"
  ]
  node
  [
    id 367
    name "NUP62"
  ]
  node
  [
    id 368
    name "SNX2"
  ]
  node
  [
    id 369
    name "DCTN1"
  ]
  node
  [
    id 370
    name "MAP3K3"
  ]
  node
  [
    id 371
    name "TOP1"
  ]
  node
  [
    id 372
    name "MTOR"
  ]
  node
  [
    id 373
    name "IPO9"
  ]
  node
  [
    id 374
    name "CTNND1"
  ]
  node
  [
    id 375
    name "WDHD1"
  ]
  node
  [
    id 376
    name "GPN1"
  ]
  node
  [
    id 377
    name "WWP2"
  ]
  node
  [
    id 378
    name "PRIM1"
  ]
  node
  [
    id 379
    name "SND1"
  ]
  node
  [
    id 380
    name "PPIA"
  ]
  node
  [
    id 381
    name "EPOR"
  ]
  node
  [
    id 382
    name "NAP1L1"
  ]
  node
  [
    id 383
    name "RAB11B"
  ]
  node
  [
    id 384
    name "TOP1MT"
  ]
  node
  [
    id 385
    name "YOD1"
  ]
  node
  [
    id 386
    name "ERN1"
  ]
  node
  [
    id 387
    name "TBL1XR1"
  ]
  node
  [
    id 388
    name "TYMS"
  ]
  node
  [
    id 389
    name "TOM1L2"
  ]
  node
  [
    id 390
    name "CTBP2"
  ]
  node
  [
    id 391
    name "CFL1"
  ]
  node
  [
    id 392
    name "RRM2"
  ]
  node
  [
    id 393
    name "GABARAP"
  ]
  node
  [
    id 394
    name "TOMM20"
  ]
  node
  [
    id 395
    name "RAB1B"
  ]
  node
  [
    id 396
    name "ERCC6L"
  ]
  node
  [
    id 397
    name "ARID2"
  ]
  node
  [
    id 398
    name "TMED10"
  ]
  node
  [
    id 399
    name "SEPT2"
  ]
  node
  [
    id 400
    name "EIF3C"
  ]
  node
  [
    id 401
    name "VPS16"
  ]
  node
  [
    id 402
    name "IST1"
  ]
  node
  [
    id 403
    name "COPB2"
  ]
  node
  [
    id 404
    name "KIAA0101"
  ]
  node
  [
    id 405
    name "AP2A1"
  ]
  node
  [
    id 406
    name "CYC1"
  ]
  edge
  [
    source 360
    target 0
    weight 0.889956961247948
  ]
  edge
  [
    source 360
    target 1
    weight 0.913795853341979
  ]
  edge
  [
    source 361
    target 0
    weight 0.92786624248015
  ]
  edge
  [
    source 361
    target 2
    weight 0.953546545388061
  ]
  edge
  [
    source 362
    target 1
    weight 0.945852295119945
  ]
  edge
  [
    source 362
    target 0
    weight 0.761912565366783
  ]
  edge
  [
    source 363
    target 2
    weight 0.898601904474038
  ]
  edge
  [
    source 364
    target 1
    weight 0.861906334513937
  ]
  edge
  [
    source 365
    target 0
    weight 0.831054231560237
  ]
  edge
  [
    source 365
    target 2
    weight 0.946930864177271
  ]
  edge
  [
    source 365
    target 1
    weight 0.943493517282152
  ]
  edge
  [
    source 366
    target 3
    weight 0.849241944147674
  ]
  edge
  [
    source 366
    target 1
    weight 0.859598807115514
  ]
  edge
  [
    source 367
    target 1
    weight 0.759688520020474
  ]
  edge
  [
    source 367
    target 3
    weight 0.869691324046934
  ]
  edge
  [
    source 367
    target 0
    weight 0.799085910617625
  ]
  edge
  [
    source 368
    target 2
    weight 0.906694808510258
  ]
  edge
  [
    source 369
    target 2
    weight 0.887463499888021
  ]
  edge
  [
    source 369
    target 1
    weight 0.863068613620469
  ]
  edge
  [
    source 369
    target 3
    weight 0.756069674912952
  ]
  edge
  [
    source 370
    target 2
    weight 0.812861418630983
  ]
  edge
  [
    source 370
    target 1
    weight 0.753812676001897
  ]
  edge
  [
    source 371
    target 2
    weight 0.953336538424813
  ]
  edge
  [
    source 371
    target 1
    weight 0.963055514048825
  ]
  edge
  [
    source 372
    target 2
    weight 0.951421905316333
  ]
  edge
  [
    source 372
    target 1
    weight 0.950729012941392
  ]
  edge
  [
    source 373
    target 2
    weight 0.80967506075929
  ]
  edge
  [
    source 373
    target 1
    weight 0.851970943792747
  ]
  edge
  [
    source 374
    target 1
    weight 0.901230912535845
  ]
  edge
  [
    source 374
    target 2
    weight 0.835415337707826
  ]
  edge
  [
    source 375
    target 0
    weight 0.756788295056641
  ]
  edge
  [
    source 375
    target 1
    weight 0.946560505057205
  ]
  edge
  [
    source 375
    target 3
    weight 0.775184941870138
  ]
  edge
  [
    source 376
    target 2
    weight 0.820402340099135
  ]
  edge
  [
    source 376
    target 0
    weight 0.836462953949372
  ]
  edge
  [
    source 376
    target 1
    weight 0.861220208396885
  ]
  edge
  [
    source 377
    target 2
    weight 0.777758247301937
  ]
  edge
  [
    source 377
    target 1
    weight 0.857859887962695
  ]
  edge
  [
    source 4
    target 3
    weight 0.883470771482099
  ]
  edge
  [
    source 378
    target 0
    weight 0.77455342244974
  ]
  edge
  [
    source 5
    target 2
    weight 0.835674607868178
  ]
  edge
  [
    source 6
    target 2
    weight 0.751251344054211
  ]
  edge
  [
    source 7
    target 3
    weight 0.881837513776443
  ]
  edge
  [
    source 8
    target 2
    weight 0.936189212129566
  ]
  edge
  [
    source 9
    target 2
    weight 0.912901841807244
  ]
  edge
  [
    source 10
    target 2
    weight 0.942906605547735
  ]
  edge
  [
    source 11
    target 2
    weight 0.93288431613166
  ]
  edge
  [
    source 7
    target 2
    weight 0.787984883799641
  ]
  edge
  [
    source 12
    target 2
    weight 0.953965463530075
  ]
  edge
  [
    source 13
    target 2
    weight 0.869365243329414
  ]
  edge
  [
    source 14
    target 2
    weight 0.860849409680769
  ]
  edge
  [
    source 15
    target 2
    weight 0.801414962747776
  ]
  edge
  [
    source 16
    target 2
    weight 0.753334165063562
  ]
  edge
  [
    source 17
    target 2
    weight 0.870716566983408
  ]
  edge
  [
    source 18
    target 2
    weight 0.809819326031864
  ]
  edge
  [
    source 19
    target 2
    weight 0.965090751956028
  ]
  edge
  [
    source 20
    target 2
    weight 0.838870015495739
  ]
  edge
  [
    source 21
    target 2
    weight 0.849220318671873
  ]
  edge
  [
    source 22
    target 2
    weight 0.901667858544019
  ]
  edge
  [
    source 23
    target 2
    weight 0.794854201455063
  ]
  edge
  [
    source 24
    target 2
    weight 0.912358722892264
  ]
  edge
  [
    source 25
    target 2
    weight 0.898420691438556
  ]
  edge
  [
    source 26
    target 2
    weight 0.835795391931749
  ]
  edge
  [
    source 27
    target 2
    weight 0.84205984588022
  ]
  edge
  [
    source 28
    target 2
    weight 0.922390908905003
  ]
  edge
  [
    source 29
    target 2
    weight 0.937218147438549
  ]
  edge
  [
    source 30
    target 2
    weight 0.932431237401764
  ]
  edge
  [
    source 31
    target 2
    weight 0.947140801698704
  ]
  edge
  [
    source 32
    target 2
    weight 0.918991023513562
  ]
  edge
  [
    source 33
    target 2
    weight 0.778690085228232
  ]
  edge
  [
    source 34
    target 2
    weight 0.803853749053864
  ]
  edge
  [
    source 35
    target 2
    weight 0.835540131026777
  ]
  edge
  [
    source 36
    target 2
    weight 0.845576055072918
  ]
  edge
  [
    source 37
    target 2
    weight 0.82212641431736
  ]
  edge
  [
    source 38
    target 2
    weight 0.786914007153413
  ]
  edge
  [
    source 39
    target 2
    weight 0.90683352076656
  ]
  edge
  [
    source 40
    target 2
    weight 0.872091172828869
  ]
  edge
  [
    source 41
    target 2
    weight 0.801644618708338
  ]
  edge
  [
    source 42
    target 2
    weight 0.910125634860291
  ]
  edge
  [
    source 43
    target 2
    weight 0.860749962198512
  ]
  edge
  [
    source 44
    target 2
    weight 0.925441843144896
  ]
  edge
  [
    source 45
    target 2
    weight 0.948060226958978
  ]
  edge
  [
    source 46
    target 2
    weight 0.770156336213691
  ]
  edge
  [
    source 47
    target 2
    weight 0.857672365103765
  ]
  edge
  [
    source 48
    target 2
    weight 0.951169146565615
  ]
  edge
  [
    source 49
    target 2
    weight 0.850603707104398
  ]
  edge
  [
    source 50
    target 2
    weight 0.84128857044165
  ]
  edge
  [
    source 51
    target 2
    weight 0.825695202321124
  ]
  edge
  [
    source 52
    target 2
    weight 0.914600482126951
  ]
  edge
  [
    source 53
    target 2
    weight 0.927558054540344
  ]
  edge
  [
    source 54
    target 2
    weight 0.924082244171404
  ]
  edge
  [
    source 55
    target 2
    weight 0.811584467844005
  ]
  edge
  [
    source 56
    target 2
    weight 0.931586425717376
  ]
  edge
  [
    source 57
    target 2
    weight 0.944268159069237
  ]
  edge
  [
    source 58
    target 2
    weight 0.866238784469492
  ]
  edge
  [
    source 59
    target 2
    weight 0.806948977562097
  ]
  edge
  [
    source 60
    target 2
    weight 0.829485952760582
  ]
  edge
  [
    source 61
    target 2
    weight 0.816570270650269
  ]
  edge
  [
    source 62
    target 2
    weight 0.84318868241026
  ]
  edge
  [
    source 63
    target 2
    weight 0.797445510878851
  ]
  edge
  [
    source 64
    target 2
    weight 0.882220919711569
  ]
  edge
  [
    source 65
    target 2
    weight 0.815500544255103
  ]
  edge
  [
    source 66
    target 2
    weight 0.818243873744349
  ]
  edge
  [
    source 67
    target 2
    weight 0.945640393832119
  ]
  edge
  [
    source 68
    target 2
    weight 0.951428006506268
  ]
  edge
  [
    source 69
    target 2
    weight 0.948302503192547
  ]
  edge
  [
    source 70
    target 2
    weight 0.814459422820747
  ]
  edge
  [
    source 71
    target 2
    weight 0.83350256716802
  ]
  edge
  [
    source 72
    target 2
    weight 0.90677683439847
  ]
  edge
  [
    source 73
    target 2
    weight 0.915443615222218
  ]
  edge
  [
    source 74
    target 2
    weight 0.871226160263792
  ]
  edge
  [
    source 75
    target 2
    weight 0.942714020715843
  ]
  edge
  [
    source 76
    target 2
    weight 0.924891265028295
  ]
  edge
  [
    source 77
    target 2
    weight 0.78909534142514
  ]
  edge
  [
    source 78
    target 2
    weight 0.866841473309427
  ]
  edge
  [
    source 79
    target 2
    weight 0.920845614952653
  ]
  edge
  [
    source 80
    target 2
    weight 0.957458277100999
  ]
  edge
  [
    source 81
    target 2
    weight 0.777444362157045
  ]
  edge
  [
    source 82
    target 2
    weight 0.841036154423992
  ]
  edge
  [
    source 83
    target 2
    weight 0.791320319037435
  ]
  edge
  [
    source 84
    target 2
    weight 0.850419474359987
  ]
  edge
  [
    source 85
    target 2
    weight 0.93127942481271
  ]
  edge
  [
    source 86
    target 2
    weight 0.792355458038304
  ]
  edge
  [
    source 87
    target 2
    weight 0.750747517620929
  ]
  edge
  [
    source 88
    target 2
    weight 0.791826970139069
  ]
  edge
  [
    source 89
    target 2
    weight 0.884390052683547
  ]
  edge
  [
    source 90
    target 2
    weight 0.75414738903126
  ]
  edge
  [
    source 91
    target 2
    weight 0.824040010263259
  ]
  edge
  [
    source 92
    target 2
    weight 0.92468820150198
  ]
  edge
  [
    source 93
    target 2
    weight 0.893537818919536
  ]
  edge
  [
    source 94
    target 2
    weight 0.754956436858108
  ]
  edge
  [
    source 95
    target 2
    weight 0.852821124413631
  ]
  edge
  [
    source 96
    target 2
    weight 0.868541222391912
  ]
  edge
  [
    source 97
    target 2
    weight 0.883969123072323
  ]
  edge
  [
    source 98
    target 2
    weight 0.921173487882707
  ]
  edge
  [
    source 99
    target 2
    weight 0.801814751849936
  ]
  edge
  [
    source 100
    target 2
    weight 0.784826506323938
  ]
  edge
  [
    source 101
    target 2
    weight 0.914211486616364
  ]
  edge
  [
    source 102
    target 2
    weight 0.859878840518814
  ]
  edge
  [
    source 103
    target 2
    weight 0.814709503350649
  ]
  edge
  [
    source 104
    target 2
    weight 0.827533535631176
  ]
  edge
  [
    source 105
    target 2
    weight 0.928198062077168
  ]
  edge
  [
    source 106
    target 2
    weight 0.920841546139886
  ]
  edge
  [
    source 107
    target 2
    weight 0.90075389552564
  ]
  edge
  [
    source 108
    target 2
    weight 0.761100254637699
  ]
  edge
  [
    source 109
    target 2
    weight 0.937482395866306
  ]
  edge
  [
    source 110
    target 2
    weight 0.925980202602937
  ]
  edge
  [
    source 111
    target 2
    weight 0.813821642922917
  ]
  edge
  [
    source 112
    target 2
    weight 0.870903884216449
  ]
  edge
  [
    source 113
    target 2
    weight 0.772213920974676
  ]
  edge
  [
    source 379
    target 0
    weight 0.849211452767065
  ]
  edge
  [
    source 379
    target 3
    weight 0.790847363950849
  ]
  edge
  [
    source 114
    target 1
    weight 0.819763429767441
  ]
  edge
  [
    source 115
    target 2
    weight 0.907082401519477
  ]
  edge
  [
    source 116
    target 3
    weight 0.786256163388695
  ]
  edge
  [
    source 49
    target 1
    weight 0.896606826744116
  ]
  edge
  [
    source 117
    target 2
    weight 0.943483846249815
  ]
  edge
  [
    source 118
    target 2
    weight 0.871199058509574
  ]
  edge
  [
    source 360
    target 2
    weight 0.952570597222371
  ]
  edge
  [
    source 380
    target 1
    weight 0.751581446895445
  ]
  edge
  [
    source 32
    target 1
    weight 0.792708100261006
  ]
  edge
  [
    source 45
    target 3
    weight 0.760959372082425
  ]
  edge
  [
    source 119
    target 0
    weight 0.819124638179236
  ]
  edge
  [
    source 120
    target 2
    weight 0.928258167812369
  ]
  edge
  [
    source 109
    target 3
    weight 0.801692087523086
  ]
  edge
  [
    source 144
    target 1
    weight 0.858384956842667
  ]
  edge
  [
    source 121
    target 3
    weight 0.863112697718755
  ]
  edge
  [
    source 122
    target 1
    weight 0.778782319810191
  ]
  edge
  [
    source 123
    target 2
    weight 0.925065143246242
  ]
  edge
  [
    source 124
    target 2
    weight 0.961560703722575
  ]
  edge
  [
    source 125
    target 0
    weight 0.928599420603029
  ]
  edge
  [
    source 126
    target 1
    weight 0.905881420312782
  ]
  edge
  [
    source 273
    target 0
    weight 0.798692625749941
  ]
  edge
  [
    source 354
    target 1
    weight 0.959110030747424
  ]
  edge
  [
    source 127
    target 2
    weight 0.841839927612661
  ]
  edge
  [
    source 128
    target 1
    weight 0.900805898694813
  ]
  edge
  [
    source 80
    target 0
    weight 0.841310141566732
  ]
  edge
  [
    source 129
    target 2
    weight 0.827847849104948
  ]
  edge
  [
    source 80
    target 1
    weight 0.923727956296684
  ]
  edge
  [
    source 381
    target 1
    weight 0.788656706499045
  ]
  edge
  [
    source 26
    target 0
    weight 0.823908591878185
  ]
  edge
  [
    source 375
    target 2
    weight 0.896159857551837
  ]
  edge
  [
    source 382
    target 1
    weight 0.935821909602943
  ]
  edge
  [
    source 2
    target 1
    weight 0.965692481254562
  ]
  edge
  [
    source 172
    target 0
    weight 0.84818707692933
  ]
  edge
  [
    source 154
    target 0
    weight 0.820962858282875
  ]
  edge
  [
    source 130
    target 2
    weight 0.921735535438852
  ]
  edge
  [
    source 261
    target 1
    weight 0.832136766076696
  ]
  edge
  [
    source 131
    target 2
    weight 0.869088555447316
  ]
  edge
  [
    source 132
    target 2
    weight 0.929702757044121
  ]
  edge
  [
    source 77
    target 1
    weight 0.899784999935271
  ]
  edge
  [
    source 90
    target 0
    weight 0.876541509701468
  ]
  edge
  [
    source 133
    target 2
    weight 0.910870029880601
  ]
  edge
  [
    source 56
    target 0
    weight 0.888690259482507
  ]
  edge
  [
    source 383
    target 1
    weight 0.898867141415951
  ]
  edge
  [
    source 134
    target 2
    weight 0.819802192605026
  ]
  edge
  [
    source 24
    target 1
    weight 0.887737924325261
  ]
  edge
  [
    source 135
    target 1
    weight 0.915000143034714
  ]
  edge
  [
    source 136
    target 2
    weight 0.849998225710943
  ]
  edge
  [
    source 340
    target 1
    weight 0.896112511528576
  ]
  edge
  [
    source 137
    target 1
    weight 0.778724496662811
  ]
  edge
  [
    source 61
    target 1
    weight 0.812333058146755
  ]
  edge
  [
    source 4
    target 2
    weight 0.924418130320334
  ]
  edge
  [
    source 108
    target 1
    weight 0.878523435380215
  ]
  edge
  [
    source 138
    target 1
    weight 0.855341386077019
  ]
  edge
  [
    source 154
    target 1
    weight 0.879745732396771
  ]
  edge
  [
    source 139
    target 2
    weight 0.786897171327225
  ]
  edge
  [
    source 384
    target 1
    weight 0.789628852295304
  ]
  edge
  [
    source 132
    target 1
    weight 0.948259557479378
  ]
  edge
  [
    source 140
    target 3
    weight 0.787051516914312
  ]
  edge
  [
    source 12
    target 3
    weight 0.879927031598035
  ]
  edge
  [
    source 12
    target 1
    weight 0.9547305518788
  ]
  edge
  [
    source 141
    target 2
    weight 0.880498923486106
  ]
  edge
  [
    source 142
    target 2
    weight 0.896067581901521
  ]
  edge
  [
    source 143
    target 3
    weight 0.761140516066308
  ]
  edge
  [
    source 82
    target 3
    weight 0.797969607627199
  ]
  edge
  [
    source 82
    target 0
    weight 0.830023949548062
  ]
  edge
  [
    source 144
    target 2
    weight 0.823257912373512
  ]
  edge
  [
    source 8
    target 0
    weight 0.826422799423231
  ]
  edge
  [
    source 145
    target 2
    weight 0.76231645249008
  ]
  edge
  [
    source 146
    target 1
    weight 0.948734294941901
  ]
  edge
  [
    source 91
    target 1
    weight 0.852609110912309
  ]
  edge
  [
    source 147
    target 3
    weight 0.771108082323782
  ]
  edge
  [
    source 148
    target 2
    weight 0.81357209357112
  ]
  edge
  [
    source 72
    target 1
    weight 0.940854902231034
  ]
  edge
  [
    source 149
    target 2
    weight 0.874196813568949
  ]
  edge
  [
    source 150
    target 2
    weight 0.776905488711481
  ]
  edge
  [
    source 151
    target 2
    weight 0.876534363466745
  ]
  edge
  [
    source 152
    target 0
    weight 0.850618542756111
  ]
  edge
  [
    source 172
    target 1
    weight 0.906166270002911
  ]
  edge
  [
    source 153
    target 2
    weight 0.854490151826084
  ]
  edge
  [
    source 154
    target 2
    weight 0.868610424979504
  ]
  edge
  [
    source 337
    target 1
    weight 0.809332891216654
  ]
  edge
  [
    source 385
    target 1
    weight 0.807009747819836
  ]
  edge
  [
    source 155
    target 2
    weight 0.823819898008254
  ]
  edge
  [
    source 29
    target 0
    weight 0.886950518529289
  ]
  edge
  [
    source 170
    target 1
    weight 0.897919166665966
  ]
  edge
  [
    source 92
    target 0
    weight 0.76656051102739
  ]
  edge
  [
    source 324
    target 0
    weight 0.948845156319799
  ]
  edge
  [
    source 156
    target 2
    weight 0.750740284195105
  ]
  edge
  [
    source 157
    target 2
    weight 0.907686041288167
  ]
  edge
  [
    source 158
    target 2
    weight 0.912004735406448
  ]
  edge
  [
    source 48
    target 3
    weight 0.758033133885767
  ]
  edge
  [
    source 271
    target 0
    weight 0.784637558721528
  ]
  edge
  [
    source 386
    target 1
    weight 0.929624170530937
  ]
  edge
  [
    source 159
    target 2
    weight 0.965868040527551
  ]
  edge
  [
    source 160
    target 0
    weight 0.769004635926008
  ]
  edge
  [
    source 50
    target 1
    weight 0.798458826411572
  ]
  edge
  [
    source 161
    target 2
    weight 0.887897587458411
  ]
  edge
  [
    source 54
    target 1
    weight 0.813881593654822
  ]
  edge
  [
    source 387
    target 1
    weight 0.84113462888155
  ]
  edge
  [
    source 387
    target 3
    weight 0.852054653103103
  ]
  edge
  [
    source 162
    target 2
    weight 0.927940635623108
  ]
  edge
  [
    source 107
    target 3
    weight 0.816398259020364
  ]
  edge
  [
    source 388
    target 1
    weight 0.827598025375301
  ]
  edge
  [
    source 163
    target 2
    weight 0.894736682348453
  ]
  edge
  [
    source 133
    target 0
    weight 0.848946176335236
  ]
  edge
  [
    source 164
    target 0
    weight 0.777255615658945
  ]
  edge
  [
    source 165
    target 2
    weight 0.922530489597904
  ]
  edge
  [
    source 389
    target 1
    weight 0.779715972585354
  ]
  edge
  [
    source 389
    target 0
    weight 0.844157628682595
  ]
  edge
  [
    source 89
    target 1
    weight 0.916409914367402
  ]
  edge
  [
    source 166
    target 3
    weight 0.812154089164586
  ]
  edge
  [
    source 167
    target 2
    weight 0.878123259243066
  ]
  edge
  [
    source 168
    target 1
    weight 0.755828408078052
  ]
  edge
  [
    source 169
    target 1
    weight 0.757725279453556
  ]
  edge
  [
    source 390
    target 3
    weight 0.849486035740451
  ]
  edge
  [
    source 390
    target 1
    weight 0.85739754055274
  ]
  edge
  [
    source 390
    target 0
    weight 0.87383955042763
  ]
  edge
  [
    source 170
    target 2
    weight 0.893324509854173
  ]
  edge
  [
    source 21
    target 1
    weight 0.838524898332872
  ]
  edge
  [
    source 171
    target 2
    weight 0.847677751835956
  ]
  edge
  [
    source 273
    target 1
    weight 0.789210911068108
  ]
  edge
  [
    source 143
    target 2
    weight 0.872676929709129
  ]
  edge
  [
    source 372
    target 0
    weight 0.864993348103805
  ]
  edge
  [
    source 122
    target 2
    weight 0.76094817828468
  ]
  edge
  [
    source 320
    target 3
    weight 0.938053538221922
  ]
  edge
  [
    source 320
    target 1
    weight 0.910219405689564
  ]
  edge
  [
    source 172
    target 2
    weight 0.841492881401103
  ]
  edge
  [
    source 23
    target 1
    weight 0.887780169345262
  ]
  edge
  [
    source 173
    target 1
    weight 0.823168351679176
  ]
  edge
  [
    source 93
    target 1
    weight 0.866352338513482
  ]
  edge
  [
    source 359
    target 1
    weight 0.840889364168045
  ]
  edge
  [
    source 355
    target 0
    weight 0.78916951523892
  ]
  edge
  [
    source 391
    target 1
    weight 0.824636927657876
  ]
  edge
  [
    source 1
    target 0
    weight 0.916132080436559
  ]
  edge
  [
    source 44
    target 0
    weight 0.77254095395752
  ]
  edge
  [
    source 174
    target 2
    weight 0.810873715251381
  ]
  edge
  [
    source 41
    target 1
    weight 0.862230075916087
  ]
  edge
  [
    source 126
    target 2
    weight 0.877197749251428
  ]
  edge
  [
    source 175
    target 0
    weight 0.864158233839322
  ]
  edge
  [
    source 392
    target 1
    weight 0.811480306342134
  ]
  edge
  [
    source 51
    target 1
    weight 0.932672141098449
  ]
  edge
  [
    source 176
    target 3
    weight 0.787914804014436
  ]
  edge
  [
    source 29
    target 3
    weight 0.76244681554515
  ]
  edge
  [
    source 177
    target 1
    weight 0.946836512310675
  ]
  edge
  [
    source 326
    target 0
    weight 0.841517082049908
  ]
  edge
  [
    source 34
    target 1
    weight 0.843535635528588
  ]
  edge
  [
    source 35
    target 3
    weight 0.820528976761822
  ]
  edge
  [
    source 178
    target 0
    weight 0.814249450983894
  ]
  edge
  [
    source 179
    target 2
    weight 0.916439044274143
  ]
  edge
  [
    source 47
    target 0
    weight 0.788772898965943
  ]
  edge
  [
    source 180
    target 2
    weight 0.833330599286499
  ]
  edge
  [
    source 181
    target 2
    weight 0.877647470884037
  ]
  edge
  [
    source 182
    target 2
    weight 0.936388286562836
  ]
  edge
  [
    source 183
    target 3
    weight 0.765837886613121
  ]
  edge
  [
    source 184
    target 2
    weight 0.806353533338942
  ]
  edge
  [
    source 24
    target 0
    weight 0.868655828276127
  ]
  edge
  [
    source 274
    target 1
    weight 0.891849395378111
  ]
  edge
  [
    source 393
    target 1
    weight 0.844827148972187
  ]
  edge
  [
    source 185
    target 2
    weight 0.864862272327804
  ]
  edge
  [
    source 186
    target 2
    weight 0.889216383093401
  ]
  edge
  [
    source 394
    target 1
    weight 0.835212825119974
  ]
  edge
  [
    source 187
    target 0
    weight 0.783601732664303
  ]
  edge
  [
    source 188
    target 3
    weight 0.772333609359215
  ]
  edge
  [
    source 136
    target 0
    weight 0.780703400500193
  ]
  edge
  [
    source 189
    target 2
    weight 0.913146003817014
  ]
  edge
  [
    source 190
    target 1
    weight 0.780373741497381
  ]
  edge
  [
    source 189
    target 1
    weight 0.892596549540148
  ]
  edge
  [
    source 114
    target 2
    weight 0.908270301993281
  ]
  edge
  [
    source 40
    target 1
    weight 0.904383048627947
  ]
  edge
  [
    source 191
    target 2
    weight 0.787507454084985
  ]
  edge
  [
    source 69
    target 1
    weight 0.906044485410642
  ]
  edge
  [
    source 139
    target 1
    weight 0.849463950696689
  ]
  edge
  [
    source 102
    target 1
    weight 0.873147159735334
  ]
  edge
  [
    source 192
    target 1
    weight 0.908454638579859
  ]
  edge
  [
    source 184
    target 1
    weight 0.769392288078447
  ]
  edge
  [
    source 193
    target 1
    weight 0.760866989573707
  ]
  edge
  [
    source 157
    target 1
    weight 0.902147114811214
  ]
  edge
  [
    source 194
    target 1
    weight 0.905051528044108
  ]
  edge
  [
    source 71
    target 1
    weight 0.905162344758004
  ]
  edge
  [
    source 195
    target 1
    weight 0.96236578935532
  ]
  edge
  [
    source 196
    target 1
    weight 0.790310822750857
  ]
  edge
  [
    source 141
    target 1
    weight 0.872546957419589
  ]
  edge
  [
    source 197
    target 1
    weight 0.892383305807834
  ]
  edge
  [
    source 30
    target 1
    weight 0.908203214305019
  ]
  edge
  [
    source 120
    target 1
    weight 0.901845587483609
  ]
  edge
  [
    source 198
    target 1
    weight 0.913544399122927
  ]
  edge
  [
    source 199
    target 1
    weight 0.904759620002255
  ]
  edge
  [
    source 167
    target 1
    weight 0.918375573693172
  ]
  edge
  [
    source 200
    target 1
    weight 0.872859398218122
  ]
  edge
  [
    source 201
    target 1
    weight 0.924773823844941
  ]
  edge
  [
    source 202
    target 1
    weight 0.935011145460281
  ]
  edge
  [
    source 203
    target 1
    weight 0.872205494981798
  ]
  edge
  [
    source 147
    target 1
    weight 0.863442221033707
  ]
  edge
  [
    source 204
    target 1
    weight 0.887868617991807
  ]
  edge
  [
    source 205
    target 1
    weight 0.835167092652724
  ]
  edge
  [
    source 179
    target 1
    weight 0.898545600781425
  ]
  edge
  [
    source 206
    target 1
    weight 0.8534916005825
  ]
  edge
  [
    source 186
    target 1
    weight 0.889224873772765
  ]
  edge
  [
    source 207
    target 1
    weight 0.753744153777047
  ]
  edge
  [
    source 18
    target 1
    weight 0.902094192243015
  ]
  edge
  [
    source 208
    target 1
    weight 0.863033246017389
  ]
  edge
  [
    source 148
    target 1
    weight 0.90089605915648
  ]
  edge
  [
    source 209
    target 1
    weight 0.783809421264088
  ]
  edge
  [
    source 28
    target 1
    weight 0.874315264675919
  ]
  edge
  [
    source 210
    target 1
    weight 0.795746612777015
  ]
  edge
  [
    source 211
    target 1
    weight 0.768724844601656
  ]
  edge
  [
    source 212
    target 1
    weight 0.758296970812534
  ]
  edge
  [
    source 213
    target 1
    weight 0.759746883414978
  ]
  edge
  [
    source 4
    target 1
    weight 0.910902988506338
  ]
  edge
  [
    source 150
    target 1
    weight 0.89155373809578
  ]
  edge
  [
    source 214
    target 1
    weight 0.873009269939391
  ]
  edge
  [
    source 215
    target 1
    weight 0.907181678754233
  ]
  edge
  [
    source 88
    target 1
    weight 0.901402217183031
  ]
  edge
  [
    source 19
    target 1
    weight 0.958350112350625
  ]
  edge
  [
    source 216
    target 1
    weight 0.835553246760169
  ]
  edge
  [
    source 142
    target 1
    weight 0.927151268831521
  ]
  edge
  [
    source 217
    target 1
    weight 0.959791066307712
  ]
  edge
  [
    source 103
    target 1
    weight 0.854115283894987
  ]
  edge
  [
    source 218
    target 1
    weight 0.861214793963584
  ]
  edge
  [
    source 219
    target 1
    weight 0.853041365027637
  ]
  edge
  [
    source 220
    target 1
    weight 0.811071662771135
  ]
  edge
  [
    source 22
    target 1
    weight 0.924156608414418
  ]
  edge
  [
    source 221
    target 1
    weight 0.871406919957088
  ]
  edge
  [
    source 222
    target 1
    weight 0.92042665043431
  ]
  edge
  [
    source 223
    target 1
    weight 0.800720288628818
  ]
  edge
  [
    source 160
    target 1
    weight 0.899841536604124
  ]
  edge
  [
    source 39
    target 1
    weight 0.832978633606201
  ]
  edge
  [
    source 111
    target 1
    weight 0.856293859306091
  ]
  edge
  [
    source 224
    target 1
    weight 0.941808995728943
  ]
  edge
  [
    source 225
    target 1
    weight 0.783851673084922
  ]
  edge
  [
    source 56
    target 1
    weight 0.964312009936951
  ]
  edge
  [
    source 153
    target 1
    weight 0.822630094493061
  ]
  edge
  [
    source 149
    target 1
    weight 0.906105603672486
  ]
  edge
  [
    source 25
    target 1
    weight 0.871355772244579
  ]
  edge
  [
    source 226
    target 1
    weight 0.771665413928581
  ]
  edge
  [
    source 8
    target 1
    weight 0.896504304984186
  ]
  edge
  [
    source 65
    target 1
    weight 0.902434246412565
  ]
  edge
  [
    source 227
    target 1
    weight 0.94463370177297
  ]
  edge
  [
    source 37
    target 1
    weight 0.902894825144293
  ]
  edge
  [
    source 86
    target 1
    weight 0.802239186680055
  ]
  edge
  [
    source 20
    target 1
    weight 0.868845402990849
  ]
  edge
  [
    source 131
    target 1
    weight 0.91931391869993
  ]
  edge
  [
    source 228
    target 1
    weight 0.852526340913372
  ]
  edge
  [
    source 229
    target 1
    weight 0.850029246960272
  ]
  edge
  [
    source 230
    target 1
    weight 0.915904649233865
  ]
  edge
  [
    source 33
    target 1
    weight 0.84303869488872
  ]
  edge
  [
    source 116
    target 1
    weight 0.759807352695506
  ]
  edge
  [
    source 231
    target 1
    weight 0.855516044100687
  ]
  edge
  [
    source 232
    target 1
    weight 0.884898619922238
  ]
  edge
  [
    source 74
    target 1
    weight 0.88275568723002
  ]
  edge
  [
    source 233
    target 1
    weight 0.865784606577066
  ]
  edge
  [
    source 81
    target 1
    weight 0.894359502535723
  ]
  edge
  [
    source 181
    target 1
    weight 0.897305609576241
  ]
  edge
  [
    source 125
    target 1
    weight 0.969114084909077
  ]
  edge
  [
    source 45
    target 1
    weight 0.89471602029575
  ]
  edge
  [
    source 64
    target 1
    weight 0.955353716555789
  ]
  edge
  [
    source 234
    target 1
    weight 0.962090617505787
  ]
  edge
  [
    source 235
    target 1
    weight 0.899969547044383
  ]
  edge
  [
    source 13
    target 1
    weight 0.87622415978041
  ]
  edge
  [
    source 175
    target 1
    weight 0.934765897659994
  ]
  edge
  [
    source 236
    target 1
    weight 0.871822819661449
  ]
  edge
  [
    source 237
    target 1
    weight 0.813368199249729
  ]
  edge
  [
    source 238
    target 1
    weight 0.761494623584496
  ]
  edge
  [
    source 159
    target 1
    weight 0.967342982710769
  ]
  edge
  [
    source 239
    target 1
    weight 0.768518708863577
  ]
  edge
  [
    source 240
    target 1
    weight 0.775840393683683
  ]
  edge
  [
    source 241
    target 1
    weight 0.885238009559046
  ]
  edge
  [
    source 242
    target 1
    weight 0.964171019414792
  ]
  edge
  [
    source 243
    target 1
    weight 0.802006605396386
  ]
  edge
  [
    source 55
    target 1
    weight 0.905773661009034
  ]
  edge
  [
    source 53
    target 1
    weight 0.88487616539103
  ]
  edge
  [
    source 42
    target 1
    weight 0.902836280514771
  ]
  edge
  [
    source 244
    target 1
    weight 0.915474075488824
  ]
  edge
  [
    source 112
    target 1
    weight 0.897506563460634
  ]
  edge
  [
    source 245
    target 1
    weight 0.875340354905782
  ]
  edge
  [
    source 183
    target 1
    weight 0.93655891509566
  ]
  edge
  [
    source 246
    target 1
    weight 0.898838004507907
  ]
  edge
  [
    source 247
    target 1
    weight 0.9066079123388
  ]
  edge
  [
    source 11
    target 1
    weight 0.888212740683501
  ]
  edge
  [
    source 248
    target 1
    weight 0.821606643365037
  ]
  edge
  [
    source 110
    target 1
    weight 0.888873572304994
  ]
  edge
  [
    source 249
    target 1
    weight 0.82568265225042
  ]
  edge
  [
    source 250
    target 1
    weight 0.867441640893509
  ]
  edge
  [
    source 83
    target 1
    weight 0.77083201360871
  ]
  edge
  [
    source 187
    target 1
    weight 0.912114516233656
  ]
  edge
  [
    source 130
    target 1
    weight 0.811881508532014
  ]
  edge
  [
    source 17
    target 1
    weight 0.943689331165582
  ]
  edge
  [
    source 251
    target 1
    weight 0.854354841490186
  ]
  edge
  [
    source 84
    target 1
    weight 0.874622505209098
  ]
  edge
  [
    source 252
    target 1
    weight 0.891701053462693
  ]
  edge
  [
    source 253
    target 1
    weight 0.910000806210084
  ]
  edge
  [
    source 254
    target 1
    weight 0.871109016678979
  ]
  edge
  [
    source 255
    target 1
    weight 0.859759577996254
  ]
  edge
  [
    source 256
    target 1
    weight 0.797892622161439
  ]
  edge
  [
    source 59
    target 1
    weight 0.850214252570025
  ]
  edge
  [
    source 257
    target 1
    weight 0.88709461170472
  ]
  edge
  [
    source 258
    target 1
    weight 0.778794712621271
  ]
  edge
  [
    source 29
    target 1
    weight 0.950666752478421
  ]
  edge
  [
    source 57
    target 1
    weight 0.923120875702075
  ]
  edge
  [
    source 129
    target 1
    weight 0.905607661288685
  ]
  edge
  [
    source 259
    target 2
    weight 0.907386205465319
  ]
  edge
  [
    source 196
    target 2
    weight 0.883527611257754
  ]
  edge
  [
    source 260
    target 3
    weight 0.767392611556447
  ]
  edge
  [
    source 395
    target 1
    weight 0.836275337000179
  ]
  edge
  [
    source 261
    target 2
    weight 0.922733414996447
  ]
  edge
  [
    source 124
    target 0
    weight 0.924515794338741
  ]
  edge
  [
    source 396
    target 1
    weight 0.834352862661433
  ]
  edge
  [
    source 206
    target 0
    weight 0.817972625283282
  ]
  edge
  [
    source 107
    target 1
    weight 0.96284233280982
  ]
  edge
  [
    source 85
    target 0
    weight 0.873644179797945
  ]
  edge
  [
    source 377
    target 0
    weight 0.812857708439981
  ]
  edge
  [
    source 224
    target 2
    weight 0.817381022238552
  ]
  edge
  [
    source 62
    target 1
    weight 0.871412878929697
  ]
  edge
  [
    source 170
    target 3
    weight 0.798214996793273
  ]
  edge
  [
    source 43
    target 3
    weight 0.801792573134571
  ]
  edge
  [
    source 386
    target 3
    weight 0.821237309386698
  ]
  edge
  [
    source 26
    target 1
    weight 0.901279914949499
  ]
  edge
  [
    source 161
    target 1
    weight 0.839650372306862
  ]
  edge
  [
    source 354
    target 0
    weight 0.76315693553441
  ]
  edge
  [
    source 119
    target 3
    weight 0.782085439880114
  ]
  edge
  [
    source 262
    target 1
    weight 0.779056828170771
  ]
  edge
  [
    source 136
    target 1
    weight 0.859692154910942
  ]
  edge
  [
    source 191
    target 1
    weight 0.881825675456869
  ]
  edge
  [
    source 6
    target 1
    weight 0.845006837138154
  ]
  edge
  [
    source 230
    target 2
    weight 0.918808456933884
  ]
  edge
  [
    source 162
    target 3
    weight 0.784822160480509
  ]
  edge
  [
    source 263
    target 2
    weight 0.812271807905817
  ]
  edge
  [
    source 264
    target 1
    weight 0.765952652264912
  ]
  edge
  [
    source 72
    target 0
    weight 0.813394615148096
  ]
  edge
  [
    source 265
    target 1
    weight 0.776301904343948
  ]
  edge
  [
    source 198
    target 2
    weight 0.884163052640738
  ]
  edge
  [
    source 266
    target 0
    weight 0.858768132271311
  ]
  edge
  [
    source 173
    target 2
    weight 0.924376105207587
  ]
  edge
  [
    source 148
    target 3
    weight 0.845525578577687
  ]
  edge
  [
    source 224
    target 3
    weight 0.801313556639079
  ]
  edge
  [
    source 148
    target 0
    weight 0.888238400177772
  ]
  edge
  [
    source 267
    target 0
    weight 0.852035212350404
  ]
  edge
  [
    source 268
    target 1
    weight 0.754842692138688
  ]
  edge
  [
    source 98
    target 0
    weight 0.796831085321268
  ]
  edge
  [
    source 92
    target 1
    weight 0.953174140961323
  ]
  edge
  [
    source 106
    target 1
    weight 0.958007808743493
  ]
  edge
  [
    source 269
    target 1
    weight 0.790243102386274
  ]
  edge
  [
    source 317
    target 0
    weight 0.800302232209202
  ]
  edge
  [
    source 130
    target 3
    weight 0.82080593930829
  ]
  edge
  [
    source 270
    target 2
    weight 0.796739686710864
  ]
  edge
  [
    source 271
    target 2
    weight 0.836653959809592
  ]
  edge
  [
    source 263
    target 1
    weight 0.780744535554462
  ]
  edge
  [
    source 98
    target 1
    weight 0.779788483677259
  ]
  edge
  [
    source 272
    target 1
    weight 0.802812019425215
  ]
  edge
  [
    source 115
    target 1
    weight 0.949069245139747
  ]
  edge
  [
    source 273
    target 2
    weight 0.961743746986924
  ]
  edge
  [
    source 262
    target 2
    weight 0.763629530625186
  ]
  edge
  [
    source 192
    target 2
    weight 0.897949744899614
  ]
  edge
  [
    source 324
    target 1
    weight 0.942280106011213
  ]
  edge
  [
    source 274
    target 2
    weight 0.954492577507067
  ]
  edge
  [
    source 275
    target 3
    weight 0.785998114941627
  ]
  edge
  [
    source 276
    target 0
    weight 0.859958618157857
  ]
  edge
  [
    source 277
    target 2
    weight 0.853006609737802
  ]
  edge
  [
    source 60
    target 0
    weight 0.830725036667669
  ]
  edge
  [
    source 162
    target 0
    weight 0.86525652835263
  ]
  edge
  [
    source 52
    target 3
    weight 0.818637517487299
  ]
  edge
  [
    source 5
    target 1
    weight 0.913252019551805
  ]
  edge
  [
    source 278
    target 3
    weight 0.770195383212391
  ]
  edge
  [
    source 5
    target 3
    weight 0.809585957996793
  ]
  edge
  [
    source 279
    target 3
    weight 0.833841399152247
  ]
  edge
  [
    source 99
    target 3
    weight 0.758871760705734
  ]
  edge
  [
    source 158
    target 3
    weight 0.84582245864945
  ]
  edge
  [
    source 153
    target 3
    weight 0.769053561607317
  ]
  edge
  [
    source 246
    target 3
    weight 0.857118545219837
  ]
  edge
  [
    source 226
    target 3
    weight 0.899940276978381
  ]
  edge
  [
    source 280
    target 3
    weight 0.913091088946993
  ]
  edge
  [
    source 281
    target 3
    weight 0.939052723951251
  ]
  edge
  [
    source 125
    target 3
    weight 0.792112485602175
  ]
  edge
  [
    source 79
    target 3
    weight 0.900516185151448
  ]
  edge
  [
    source 165
    target 3
    weight 0.931480701951625
  ]
  edge
  [
    source 198
    target 3
    weight 0.809687769470789
  ]
  edge
  [
    source 11
    target 3
    weight 0.900529461072547
  ]
  edge
  [
    source 245
    target 3
    weight 0.902480851999445
  ]
  edge
  [
    source 96
    target 3
    weight 0.784513511072259
  ]
  edge
  [
    source 282
    target 3
    weight 0.86604470920705
  ]
  edge
  [
    source 123
    target 3
    weight 0.910753993101681
  ]
  edge
  [
    source 283
    target 3
    weight 0.892779199415119
  ]
  edge
  [
    source 284
    target 3
    weight 0.775508877063592
  ]
  edge
  [
    source 124
    target 3
    weight 0.891595123854068
  ]
  edge
  [
    source 285
    target 3
    weight 0.775520372711574
  ]
  edge
  [
    source 161
    target 3
    weight 0.7668706880565
  ]
  edge
  [
    source 149
    target 3
    weight 0.760274564575991
  ]
  edge
  [
    source 234
    target 3
    weight 0.864335194468596
  ]
  edge
  [
    source 77
    target 3
    weight 0.870708265943148
  ]
  edge
  [
    source 286
    target 3
    weight 0.819456073680979
  ]
  edge
  [
    source 257
    target 3
    weight 0.821050645320686
  ]
  edge
  [
    source 287
    target 3
    weight 0.808905133545986
  ]
  edge
  [
    source 17
    target 3
    weight 0.806056040501914
  ]
  edge
  [
    source 276
    target 3
    weight 0.857170343642878
  ]
  edge
  [
    source 115
    target 3
    weight 0.801227270394238
  ]
  edge
  [
    source 230
    target 3
    weight 0.901624242969786
  ]
  edge
  [
    source 85
    target 3
    weight 0.911313855095585
  ]
  edge
  [
    source 244
    target 3
    weight 0.901097641801529
  ]
  edge
  [
    source 177
    target 3
    weight 0.782966641569112
  ]
  edge
  [
    source 242
    target 3
    weight 0.833000870329844
  ]
  edge
  [
    source 22
    target 3
    weight 0.778314413608634
  ]
  edge
  [
    source 250
    target 3
    weight 0.783107738113237
  ]
  edge
  [
    source 10
    target 3
    weight 0.772673740527467
  ]
  edge
  [
    source 171
    target 3
    weight 0.75334134945294
  ]
  edge
  [
    source 249
    target 3
    weight 0.885148685451437
  ]
  edge
  [
    source 222
    target 3
    weight 0.885087012862032
  ]
  edge
  [
    source 288
    target 3
    weight 0.882662019187536
  ]
  edge
  [
    source 118
    target 3
    weight 0.761719232553309
  ]
  edge
  [
    source 218
    target 3
    weight 0.757564054024735
  ]
  edge
  [
    source 289
    target 3
    weight 0.912514457386487
  ]
  edge
  [
    source 136
    target 3
    weight 0.817137793389167
  ]
  edge
  [
    source 51
    target 3
    weight 0.874889901999288
  ]
  edge
  [
    source 110
    target 3
    weight 0.908336272573209
  ]
  edge
  [
    source 68
    target 3
    weight 0.883306504782665
  ]
  edge
  [
    source 269
    target 3
    weight 0.777593378522054
  ]
  edge
  [
    source 108
    target 3
    weight 0.829278819555325
  ]
  edge
  [
    source 3
    target 0
    weight 0.771208783109832
  ]
  edge
  [
    source 201
    target 3
    weight 0.793006418938265
  ]
  edge
  [
    source 57
    target 3
    weight 0.896187118252559
  ]
  edge
  [
    source 126
    target 3
    weight 0.762670700433461
  ]
  edge
  [
    source 67
    target 3
    weight 0.862202685146904
  ]
  edge
  [
    source 266
    target 3
    weight 0.912275230668678
  ]
  edge
  [
    source 62
    target 3
    weight 0.753265635019877
  ]
  edge
  [
    source 142
    target 3
    weight 0.759404840118295
  ]
  edge
  [
    source 290
    target 3
    weight 0.810659775473036
  ]
  edge
  [
    source 131
    target 3
    weight 0.794830229229074
  ]
  edge
  [
    source 291
    target 3
    weight 0.880056503676743
  ]
  edge
  [
    source 104
    target 3
    weight 0.912376626547247
  ]
  edge
  [
    source 292
    target 3
    weight 0.778441724302952
  ]
  edge
  [
    source 28
    target 3
    weight 0.878874876148296
  ]
  edge
  [
    source 211
    target 3
    weight 0.878014104118425
  ]
  edge
  [
    source 293
    target 3
    weight 0.854469450783487
  ]
  edge
  [
    source 277
    target 3
    weight 0.856825767225034
  ]
  edge
  [
    source 105
    target 3
    weight 0.79555797225206
  ]
  edge
  [
    source 8
    target 3
    weight 0.859171603106649
  ]
  edge
  [
    source 167
    target 3
    weight 0.78620911858964
  ]
  edge
  [
    source 237
    target 3
    weight 0.758637573477027
  ]
  edge
  [
    source 122
    target 3
    weight 0.898100036065934
  ]
  edge
  [
    source 199
    target 3
    weight 0.807766977323291
  ]
  edge
  [
    source 202
    target 3
    weight 0.791458537868034
  ]
  edge
  [
    source 53
    target 3
    weight 0.77792731990256
  ]
  edge
  [
    source 70
    target 3
    weight 0.831290086213704
  ]
  edge
  [
    source 31
    target 3
    weight 0.776704940619539
  ]
  edge
  [
    source 142
    target 0
    weight 0.912821417059523
  ]
  edge
  [
    source 280
    target 2
    weight 0.922488200636483
  ]
  edge
  [
    source 82
    target 1
    weight 0.932055350919545
  ]
  edge
  [
    source 76
    target 1
    weight 0.921957250942561
  ]
  edge
  [
    source 294
    target 2
    weight 0.758077238457644
  ]
  edge
  [
    source 295
    target 1
    weight 0.961488217640758
  ]
  edge
  [
    source 397
    target 3
    weight 0.865061541847355
  ]
  edge
  [
    source 16
    target 1
    weight 0.815983244378362
  ]
  edge
  [
    source 296
    target 0
    weight 0.759549986290597
  ]
  edge
  [
    source 297
    target 3
    weight 0.797726969909866
  ]
  edge
  [
    source 179
    target 0
    weight 0.891303979520391
  ]
  edge
  [
    source 317
    target 1
    weight 0.890051638463868
  ]
  edge
  [
    source 72
    target 3
    weight 0.887969119391241
  ]
  edge
  [
    source 236
    target 2
    weight 0.792618353767322
  ]
  edge
  [
    source 106
    target 3
    weight 0.758597952746814
  ]
  edge
  [
    source 292
    target 1
    weight 0.95257182763714
  ]
  edge
  [
    source 294
    target 1
    weight 0.835330521505899
  ]
  edge
  [
    source 374
    target 3
    weight 0.82583001036059
  ]
  edge
  [
    source 287
    target 0
    weight 0.904764574562681
  ]
  edge
  [
    source 165
    target 0
    weight 0.786177287547226
  ]
  edge
  [
    source 204
    target 0
    weight 0.785478568677071
  ]
  edge
  [
    source 37
    target 0
    weight 0.78757390680189
  ]
  edge
  [
    source 298
    target 0
    weight 0.876832546098302
  ]
  edge
  [
    source 143
    target 0
    weight 0.81906079783652
  ]
  edge
  [
    source 16
    target 0
    weight 0.864731274997752
  ]
  edge
  [
    source 151
    target 0
    weight 0.808432232811672
  ]
  edge
  [
    source 110
    target 0
    weight 0.83382344868482
  ]
  edge
  [
    source 196
    target 0
    weight 0.858066519678535
  ]
  edge
  [
    source 260
    target 0
    weight 0.835174468437682
  ]
  edge
  [
    source 75
    target 0
    weight 0.90271029609439
  ]
  edge
  [
    source 106
    target 0
    weight 0.818840728423626
  ]
  edge
  [
    source 293
    target 0
    weight 0.842903630167671
  ]
  edge
  [
    source 43
    target 0
    weight 0.80858392614344
  ]
  edge
  [
    source 117
    target 0
    weight 0.829115683977921
  ]
  edge
  [
    source 281
    target 0
    weight 0.799495846342732
  ]
  edge
  [
    source 157
    target 0
    weight 0.830953946019775
  ]
  edge
  [
    source 299
    target 0
    weight 0.918819905444898
  ]
  edge
  [
    source 300
    target 0
    weight 0.842058740253992
  ]
  edge
  [
    source 301
    target 0
    weight 0.785833476919731
  ]
  edge
  [
    source 17
    target 0
    weight 0.855702670173189
  ]
  edge
  [
    source 109
    target 0
    weight 0.829762994638761
  ]
  edge
  [
    source 233
    target 0
    weight 0.78275134104403
  ]
  edge
  [
    source 177
    target 0
    weight 0.887723903286232
  ]
  edge
  [
    source 213
    target 0
    weight 0.81709894530661
  ]
  edge
  [
    source 97
    target 0
    weight 0.838653184008609
  ]
  edge
  [
    source 302
    target 0
    weight 0.780179685150627
  ]
  edge
  [
    source 194
    target 0
    weight 0.924412399726902
  ]
  edge
  [
    source 127
    target 0
    weight 0.84622534979294
  ]
  edge
  [
    source 250
    target 0
    weight 0.842797417356589
  ]
  edge
  [
    source 283
    target 0
    weight 0.87124697981061
  ]
  edge
  [
    source 39
    target 0
    weight 0.797434515126079
  ]
  edge
  [
    source 103
    target 0
    weight 0.798337346175969
  ]
  edge
  [
    source 118
    target 0
    weight 0.86694543785338
  ]
  edge
  [
    source 183
    target 0
    weight 0.792983426603106
  ]
  edge
  [
    source 6
    target 0
    weight 0.824007841052617
  ]
  edge
  [
    source 101
    target 0
    weight 0.825023373751953
  ]
  edge
  [
    source 303
    target 0
    weight 0.823692854477549
  ]
  edge
  [
    source 158
    target 0
    weight 0.869001799012838
  ]
  edge
  [
    source 128
    target 0
    weight 0.816683680533878
  ]
  edge
  [
    source 249
    target 0
    weight 0.782729122250548
  ]
  edge
  [
    source 78
    target 0
    weight 0.851586575021468
  ]
  edge
  [
    source 304
    target 0
    weight 0.780939868607241
  ]
  edge
  [
    source 220
    target 0
    weight 0.776998719852931
  ]
  edge
  [
    source 268
    target 0
    weight 0.803002820156767
  ]
  edge
  [
    source 289
    target 0
    weight 0.842240710725672
  ]
  edge
  [
    source 193
    target 0
    weight 0.791447991333326
  ]
  edge
  [
    source 95
    target 0
    weight 0.850426518377103
  ]
  edge
  [
    source 66
    target 0
    weight 0.910432846823022
  ]
  edge
  [
    source 284
    target 0
    weight 0.822121477165789
  ]
  edge
  [
    source 96
    target 0
    weight 0.853006687598981
  ]
  edge
  [
    source 305
    target 0
    weight 0.904735758850079
  ]
  edge
  [
    source 181
    target 0
    weight 0.755210893199606
  ]
  edge
  [
    source 104
    target 0
    weight 0.892450546890966
  ]
  edge
  [
    source 201
    target 0
    weight 0.848051638985413
  ]
  edge
  [
    source 306
    target 0
    weight 0.852100196496798
  ]
  edge
  [
    source 291
    target 0
    weight 0.796543278394304
  ]
  edge
  [
    source 77
    target 0
    weight 0.800072251744269
  ]
  edge
  [
    source 245
    target 0
    weight 0.820576837623295
  ]
  edge
  [
    source 234
    target 0
    weight 0.942615736455909
  ]
  edge
  [
    source 307
    target 0
    weight 0.863052961607817
  ]
  edge
  [
    source 131
    target 0
    weight 0.848389161354074
  ]
  edge
  [
    source 63
    target 0
    weight 0.80497210044311
  ]
  edge
  [
    source 111
    target 0
    weight 0.75480578197344
  ]
  edge
  [
    source 84
    target 0
    weight 0.755617707252
  ]
  edge
  [
    source 4
    target 0
    weight 0.858587556316728
  ]
  edge
  [
    source 232
    target 0
    weight 0.863726661503814
  ]
  edge
  [
    source 74
    target 0
    weight 0.824957512171489
  ]
  edge
  [
    source 153
    target 0
    weight 0.805175196449658
  ]
  edge
  [
    source 280
    target 0
    weight 0.929964204703277
  ]
  edge
  [
    source 214
    target 0
    weight 0.758823602595634
  ]
  edge
  [
    source 135
    target 0
    weight 0.858925444357861
  ]
  edge
  [
    source 253
    target 0
    weight 0.754252599449899
  ]
  edge
  [
    source 57
    target 0
    weight 0.916690700449664
  ]
  edge
  [
    source 186
    target 0
    weight 0.799692332660033
  ]
  edge
  [
    source 45
    target 0
    weight 0.925619902437055
  ]
  edge
  [
    source 308
    target 0
    weight 0.772086753704621
  ]
  edge
  [
    source 192
    target 0
    weight 0.903065904189893
  ]
  edge
  [
    source 215
    target 0
    weight 0.813961666413892
  ]
  edge
  [
    source 146
    target 0
    weight 0.924242254591405
  ]
  edge
  [
    source 309
    target 0
    weight 0.825090337463315
  ]
  edge
  [
    source 123
    target 0
    weight 0.799066867732443
  ]
  edge
  [
    source 20
    target 0
    weight 0.786924828618721
  ]
  edge
  [
    source 19
    target 0
    weight 0.922477929947729
  ]
  edge
  [
    source 120
    target 0
    weight 0.844582633823413
  ]
  edge
  [
    source 182
    target 0
    weight 0.917788207270932
  ]
  edge
  [
    source 132
    target 0
    weight 0.914280084211995
  ]
  edge
  [
    source 156
    target 0
    weight 0.794777721809916
  ]
  edge
  [
    source 55
    target 0
    weight 0.811379015927055
  ]
  edge
  [
    source 64
    target 0
    weight 0.807755562835189
  ]
  edge
  [
    source 166
    target 0
    weight 0.837713654538014
  ]
  edge
  [
    source 53
    target 0
    weight 0.84443666430258
  ]
  edge
  [
    source 222
    target 0
    weight 0.918240988632357
  ]
  edge
  [
    source 310
    target 0
    weight 0.780700587273881
  ]
  edge
  [
    source 311
    target 0
    weight 0.876233012573167
  ]
  edge
  [
    source 159
    target 0
    weight 0.926757644195141
  ]
  edge
  [
    source 22
    target 0
    weight 0.909867429584848
  ]
  edge
  [
    source 246
    target 0
    weight 0.816509421595627
  ]
  edge
  [
    source 226
    target 0
    weight 0.842228860702055
  ]
  edge
  [
    source 312
    target 0
    weight 0.810665122581573
  ]
  edge
  [
    source 313
    target 0
    weight 0.768743713459168
  ]
  edge
  [
    source 314
    target 0
    weight 0.827950370154396
  ]
  edge
  [
    source 300
    target 3
    weight 0.917892102993232
  ]
  edge
  [
    source 315
    target 0
    weight 0.761999911411623
  ]
  edge
  [
    source 214
    target 2
    weight 0.754416460711372
  ]
  edge
  [
    source 10
    target 0
    weight 0.91240365851978
  ]
  edge
  [
    source 44
    target 1
    weight 0.890189045064999
  ]
  edge
  [
    source 205
    target 3
    weight 0.845209016575172
  ]
  edge
  [
    source 277
    target 1
    weight 0.887257167859004
  ]
  edge
  [
    source 311
    target 2
    weight 0.93688283703082
  ]
  edge
  [
    source 107
    target 0
    weight 0.842198098838508
  ]
  edge
  [
    source 48
    target 1
    weight 0.882387989582678
  ]
  edge
  [
    source 58
    target 0
    weight 0.859183864572344
  ]
  edge
  [
    source 252
    target 0
    weight 0.867275111876914
  ]
  edge
  [
    source 316
    target 0
    weight 0.754696934502687
  ]
  edge
  [
    source 121
    target 2
    weight 0.932853205826416
  ]
  edge
  [
    source 393
    target 0
    weight 0.814123250210408
  ]
  edge
  [
    source 194
    target 3
    weight 0.901436700330525
  ]
  edge
  [
    source 137
    target 3
    weight 0.911657391284288
  ]
  edge
  [
    source 309
    target 1
    weight 0.780107150432496
  ]
  edge
  [
    source 197
    target 2
    weight 0.920972003810915
  ]
  edge
  [
    source 152
    target 1
    weight 0.883982392706132
  ]
  edge
  [
    source 152
    target 2
    weight 0.871241363496313
  ]
  edge
  [
    source 398
    target 1
    weight 0.772812510476298
  ]
  edge
  [
    source 21
    target 0
    weight 0.900369151413437
  ]
  edge
  [
    source 188
    target 1
    weight 0.901940501567387
  ]
  edge
  [
    source 217
    target 3
    weight 0.772808965814188
  ]
  edge
  [
    source 306
    target 2
    weight 0.874676333407389
  ]
  edge
  [
    source 133
    target 1
    weight 0.90376080412459
  ]
  edge
  [
    source 317
    target 2
    weight 0.895203177416407
  ]
  edge
  [
    source 36
    target 1
    weight 0.892218895908535
  ]
  edge
  [
    source 289
    target 2
    weight 0.938262587521896
  ]
  edge
  [
    source 326
    target 1
    weight 0.878250588229471
  ]
  edge
  [
    source 318
    target 0
    weight 0.769347143364347
  ]
  edge
  [
    source 297
    target 1
    weight 0.930362846820596
  ]
  edge
  [
    source 203
    target 0
    weight 0.767943262428285
  ]
  edge
  [
    source 303
    target 2
    weight 0.887816494884008
  ]
  edge
  [
    source 299
    target 3
    weight 0.768483494146727
  ]
  edge
  [
    source 319
    target 1
    weight 0.908661716350992
  ]
  edge
  [
    source 39
    target 3
    weight 0.854418742268747
  ]
  edge
  [
    source 324
    target 3
    weight 0.835194864814197
  ]
  edge
  [
    source 289
    target 1
    weight 0.919487532179248
  ]
  edge
  [
    source 167
    target 0
    weight 0.837672137492735
  ]
  edge
  [
    source 129
    target 0
    weight 0.769756026875552
  ]
  edge
  [
    source 183
    target 2
    weight 0.936650059350508
  ]
  edge
  [
    source 320
    target 2
    weight 0.893480497019321
  ]
  edge
  [
    source 68
    target 1
    weight 0.944691873340981
  ]
  edge
  [
    source 292
    target 0
    weight 0.843493179341394
  ]
  edge
  [
    source 321
    target 2
    weight 0.848472691009554
  ]
  edge
  [
    source 147
    target 0
    weight 0.835117513570812
  ]
  edge
  [
    source 322
    target 2
    weight 0.836521333698101
  ]
  edge
  [
    source 276
    target 1
    weight 0.966593282129615
  ]
  edge
  [
    source 117
    target 3
    weight 0.758885946893896
  ]
  edge
  [
    source 70
    target 0
    weight 0.816946157293407
  ]
  edge
  [
    source 323
    target 3
    weight 0.867985571358104
  ]
  edge
  [
    source 115
    target 0
    weight 0.902851873622088
  ]
  edge
  [
    source 162
    target 1
    weight 0.915484157390772
  ]
  edge
  [
    source 299
    target 1
    weight 0.969270160715518
  ]
  edge
  [
    source 399
    target 3
    weight 0.761597036094091
  ]
  edge
  [
    source 298
    target 1
    weight 0.817911535132221
  ]
  edge
  [
    source 219
    target 0
    weight 0.756541850656821
  ]
  edge
  [
    source 324
    target 2
    weight 0.825079493866344
  ]
  edge
  [
    source 14
    target 0
    weight 0.755259969522689
  ]
  edge
  [
    source 325
    target 0
    weight 0.799902921364657
  ]
  edge
  [
    source 374
    target 0
    weight 0.81836639276177
  ]
  edge
  [
    source 170
    target 0
    weight 0.792441073850041
  ]
  edge
  [
    source 364
    target 0
    weight 0.776956546450788
  ]
  edge
  [
    source 286
    target 0
    weight 0.757839488760861
  ]
  edge
  [
    source 326
    target 2
    weight 0.913736724078774
  ]
  edge
  [
    source 68
    target 0
    weight 0.872298435229202
  ]
  edge
  [
    source 327
    target 3
    weight 0.855465855750433
  ]
  edge
  [
    source 321
    target 3
    weight 0.795871100338017
  ]
  edge
  [
    source 188
    target 0
    weight 0.77506633124609
  ]
  edge
  [
    source 280
    target 1
    weight 0.89068933689101
  ]
  edge
  [
    source 206
    target 2
    weight 0.878694839422977
  ]
  edge
  [
    source 355
    target 1
    weight 0.828274977107391
  ]
  edge
  [
    source 85
    target 1
    weight 0.956847145883535
  ]
  edge
  [
    source 328
    target 2
    weight 0.853402924938387
  ]
  edge
  [
    source 156
    target 1
    weight 0.782057893317165
  ]
  edge
  [
    source 166
    target 1
    weight 0.888209342662968
  ]
  edge
  [
    source 335
    target 1
    weight 0.826442083506761
  ]
  edge
  [
    source 264
    target 2
    weight 0.800033730600661
  ]
  edge
  [
    source 215
    target 2
    weight 0.807228924418398
  ]
  edge
  [
    source 211
    target 2
    weight 0.761281540413008
  ]
  edge
  [
    source 158
    target 1
    weight 0.939944345538545
  ]
  edge
  [
    source 329
    target 2
    weight 0.900075471631117
  ]
  edge
  [
    source 330
    target 2
    weight 0.777414883997974
  ]
  edge
  [
    source 51
    target 0
    weight 0.850831023413781
  ]
  edge
  [
    source 305
    target 1
    weight 0.948626641527841
  ]
  edge
  [
    source 281
    target 2
    weight 0.941902690430567
  ]
  edge
  [
    source 312
    target 1
    weight 0.813495918254608
  ]
  edge
  [
    source 125
    target 2
    weight 0.924136887548572
  ]
  edge
  [
    source 182
    target 1
    weight 0.93951606147799
  ]
  edge
  [
    source 279
    target 0
    weight 0.798116686540347
  ]
  edge
  [
    source 231
    target 2
    weight 0.797391459549025
  ]
  edge
  [
    source 363
    target 1
    weight 0.890756347371797
  ]
  edge
  [
    source 143
    target 1
    weight 0.946308837655666
  ]
  edge
  [
    source 354
    target 3
    weight 0.862921857147621
  ]
  edge
  [
    source 25
    target 0
    weight 0.860066588111992
  ]
  edge
  [
    source 340
    target 0
    weight 0.89878159790377
  ]
  edge
  [
    source 9
    target 1
    weight 0.828032602386836
  ]
  edge
  [
    source 331
    target 2
    weight 0.927701089611766
  ]
  edge
  [
    source 224
    target 0
    weight 0.862467557078319
  ]
  edge
  [
    source 332
    target 1
    weight 0.820654488973
  ]
  edge
  [
    source 285
    target 0
    weight 0.885537152252137
  ]
  edge
  [
    source 361
    target 1
    weight 0.893464693474094
  ]
  edge
  [
    source 333
    target 2
    weight 0.916252029983093
  ]
  edge
  [
    source 368
    target 0
    weight 0.756426302866115
  ]
  edge
  [
    source 159
    target 3
    weight 0.833945517192783
  ]
  edge
  [
    source 121
    target 1
    weight 0.956368408674689
  ]
  edge
  [
    source 126
    target 0
    weight 0.81096048801046
  ]
  edge
  [
    source 147
    target 2
    weight 0.827216816254378
  ]
  edge
  [
    source 328
    target 1
    weight 0.903256581511457
  ]
  edge
  [
    source 14
    target 1
    weight 0.890592684193217
  ]
  edge
  [
    source 333
    target 1
    weight 0.885296450419014
  ]
  edge
  [
    source 194
    target 2
    weight 0.943623973011145
  ]
  edge
  [
    source 334
    target 1
    weight 0.767364606324533
  ]
  edge
  [
    source 27
    target 0
    weight 0.766704055743117
  ]
  edge
  [
    source 335
    target 0
    weight 0.76400187072581
  ]
  edge
  [
    source 5
    target 0
    weight 0.789207092410814
  ]
  edge
  [
    source 40
    target 0
    weight 0.831834669521972
  ]
  edge
  [
    source 195
    target 2
    weight 0.934949250473411
  ]
  edge
  [
    source 69
    target 3
    weight 0.785925267804684
  ]
  edge
  [
    source 368
    target 1
    weight 0.795569055066759
  ]
  edge
  [
    source 105
    target 1
    weight 0.911383652733943
  ]
  edge
  [
    source 228
    target 2
    weight 0.853740849781677
  ]
  edge
  [
    source 255
    target 3
    weight 0.907386096659485
  ]
  edge
  [
    source 299
    target 2
    weight 0.970661611465662
  ]
  edge
  [
    source 79
    target 1
    weight 0.91226664972674
  ]
  edge
  [
    source 341
    target 1
    weight 0.843053327507339
  ]
  edge
  [
    source 19
    target 3
    weight 0.781679512853394
  ]
  edge
  [
    source 234
    target 2
    weight 0.930765668059505
  ]
  edge
  [
    source 265
    target 3
    weight 0.920117821635791
  ]
  edge
  [
    source 335
    target 2
    weight 0.89405633774189
  ]
  edge
  [
    source 267
    target 1
    weight 0.756948703954456
  ]
  edge
  [
    source 217
    target 0
    weight 0.90504557056234
  ]
  edge
  [
    source 266
    target 2
    weight 0.836252299683658
  ]
  edge
  [
    source 31
    target 0
    weight 0.909331823852236
  ]
  edge
  [
    source 307
    target 2
    weight 0.934422622810398
  ]
  edge
  [
    source 310
    target 1
    weight 0.791572083206253
  ]
  edge
  [
    source 215
    target 3
    weight 0.797565975692181
  ]
  edge
  [
    source 251
    target 0
    weight 0.837088264735835
  ]
  edge
  [
    source 96
    target 1
    weight 0.907943157444521
  ]
  edge
  [
    source 336
    target 1
    weight 0.843251952381815
  ]
  edge
  [
    source 260
    target 1
    weight 0.802146164828286
  ]
  edge
  [
    source 387
    target 0
    weight 0.829153400212476
  ]
  edge
  [
    source 90
    target 1
    weight 0.779506114539072
  ]
  edge
  [
    source 220
    target 2
    weight 0.875093608342767
  ]
  edge
  [
    source 133
    target 3
    weight 0.804334273393476
  ]
  edge
  [
    source 337
    target 2
    weight 0.849425827839871
  ]
  edge
  [
    source 323
    target 1
    weight 0.801962295359711
  ]
  edge
  [
    source 283
    target 1
    weight 0.853124288220232
  ]
  edge
  [
    source 6
    target 3
    weight 0.777693060367034
  ]
  edge
  [
    source 305
    target 2
    weight 0.935379610130354
  ]
  edge
  [
    source 391
    target 0
    weight 0.831490521727974
  ]
  edge
  [
    source 338
    target 1
    weight 0.873477967428464
  ]
  edge
  [
    source 293
    target 1
    weight 0.77424426780365
  ]
  edge
  [
    source 61
    target 0
    weight 0.76352038499428
  ]
  edge
  [
    source 232
    target 2
    weight 0.909053337982049
  ]
  edge
  [
    source 378
    target 2
    weight 0.76603949365878
  ]
  edge
  [
    source 319
    target 0
    weight 0.780049350935672
  ]
  edge
  [
    source 257
    target 0
    weight 0.789272859825554
  ]
  edge
  [
    source 244
    target 0
    weight 0.814166163954885
  ]
  edge
  [
    source 346
    target 1
    weight 0.843112071140027
  ]
  edge
  [
    source 252
    target 3
    weight 0.809086766431158
  ]
  edge
  [
    source 339
    target 2
    weight 0.756178625108754
  ]
  edge
  [
    source 175
    target 2
    weight 0.871335470475192
  ]
  edge
  [
    source 116
    target 0
    weight 0.845624951495299
  ]
  edge
  [
    source 78
    target 1
    weight 0.848572157412478
  ]
  edge
  [
    source 302
    target 1
    weight 0.894687466994594
  ]
  edge
  [
    source 257
    target 2
    weight 0.927313916175881
  ]
  edge
  [
    source 400
    target 1
    weight 0.754207804903864
  ]
  edge
  [
    source 310
    target 2
    weight 0.797207195024325
  ]
  edge
  [
    source 333
    target 0
    weight 0.770230614547167
  ]
  edge
  [
    source 302
    target 2
    weight 0.806518481954783
  ]
  edge
  [
    source 166
    target 2
    weight 0.813794969400153
  ]
  edge
  [
    source 291
    target 1
    weight 0.906856395851828
  ]
  edge
  [
    source 151
    target 1
    weight 0.862100306631267
  ]
  edge
  [
    source 264
    target 0
    weight 0.926405028867313
  ]
  edge
  [
    source 43
    target 1
    weight 0.896438343142893
  ]
  edge
  [
    source 320
    target 0
    weight 0.757474996331528
  ]
  edge
  [
    source 89
    target 0
    weight 0.817136985632007
  ]
  edge
  [
    source 7
    target 0
    weight 0.907408168032062
  ]
  edge
  [
    source 76
    target 0
    weight 0.803057200691538
  ]
  edge
  [
    source 229
    target 0
    weight 0.825467164378088
  ]
  edge
  [
    source 242
    target 2
    weight 0.943030356774799
  ]
  edge
  [
    source 340
    target 2
    weight 0.915544822604393
  ]
  edge
  [
    source 199
    target 2
    weight 0.948376007029954
  ]
  edge
  [
    source 318
    target 2
    weight 0.839500998218334
  ]
  edge
  [
    source 341
    target 2
    weight 0.856567565781027
  ]
  edge
  [
    source 122
    target 0
    weight 0.874029400065252
  ]
  edge
  [
    source 342
    target 0
    weight 0.772755578982432
  ]
  edge
  [
    source 38
    target 3
    weight 0.778561848865827
  ]
  edge
  [
    source 188
    target 2
    weight 0.84201854454792
  ]
  edge
  [
    source 138
    target 2
    weight 0.761338331112002
  ]
  edge
  [
    source 128
    target 2
    weight 0.888151911325493
  ]
  edge
  [
    source 357
    target 3
    weight 0.759729946900358
  ]
  edge
  [
    source 296
    target 1
    weight 0.772874449575075
  ]
  edge
  [
    source 52
    target 1
    weight 0.951517830799111
  ]
  edge
  [
    source 300
    target 2
    weight 0.938394203010579
  ]
  edge
  [
    source 279
    target 1
    weight 0.919631855868187
  ]
  edge
  [
    source 401
    target 0
    weight 0.762388199691085
  ]
  edge
  [
    source 30
    target 3
    weight 0.880311375883248
  ]
  edge
  [
    source 101
    target 3
    weight 0.750282496435183
  ]
  edge
  [
    source 295
    target 3
    weight 0.805340632369095
  ]
  edge
  [
    source 69
    target 0
    weight 0.929118821975905
  ]
  edge
  [
    source 71
    target 0
    weight 0.83055317343708
  ]
  edge
  [
    source 279
    target 2
    weight 0.927638633423874
  ]
  edge
  [
    source 27
    target 3
    weight 0.753152204831071
  ]
  edge
  [
    source 343
    target 1
    weight 0.793278379349202
  ]
  edge
  [
    source 48
    target 0
    weight 0.937898284209429
  ]
  edge
  [
    source 185
    target 1
    weight 0.906755367863633
  ]
  edge
  [
    source 321
    target 0
    weight 0.83084147897731
  ]
  edge
  [
    source 244
    target 2
    weight 0.925224723471042
  ]
  edge
  [
    source 338
    target 0
    weight 0.87293937062836
  ]
  edge
  [
    source 105
    target 0
    weight 0.861573792625798
  ]
  edge
  [
    source 264
    target 3
    weight 0.755954169405604
  ]
  edge
  [
    source 47
    target 1
    weight 0.921980555796462
  ]
  edge
  [
    source 99
    target 1
    weight 0.830541932114075
  ]
  edge
  [
    source 124
    target 1
    weight 0.858285953371465
  ]
  edge
  [
    source 262
    target 0
    weight 0.76954499874084
  ]
  edge
  [
    source 344
    target 1
    weight 0.882870610021843
  ]
  edge
  [
    source 97
    target 1
    weight 0.756197459644056
  ]
  edge
  [
    source 2
    target 0
    weight 0.919839265948927
  ]
  edge
  [
    source 66
    target 1
    weight 0.894376497599247
  ]
  edge
  [
    source 81
    target 0
    weight 0.883018519195191
  ]
  edge
  [
    source 345
    target 1
    weight 0.874567336491287
  ]
  edge
  [
    source 203
    target 2
    weight 0.750116415406229
  ]
  edge
  [
    source 30
    target 0
    weight 0.865943707427209
  ]
  edge
  [
    source 288
    target 1
    weight 0.789092731228239
  ]
  edge
  [
    source 253
    target 3
    weight 0.756062542415129
  ]
  edge
  [
    source 338
    target 3
    weight 0.860860914517412
  ]
  edge
  [
    source 295
    target 2
    weight 0.953343693176151
  ]
  edge
  [
    source 109
    target 1
    weight 0.945682145789252
  ]
  edge
  [
    source 334
    target 0
    weight 0.822943402970824
  ]
  edge
  [
    source 121
    target 0
    weight 0.94969260255232
  ]
  edge
  [
    source 197
    target 3
    weight 0.883471361258899
  ]
  edge
  [
    source 236
    target 0
    weight 0.816063090893652
  ]
  edge
  [
    source 86
    target 0
    weight 0.84073225121459
  ]
  edge
  [
    source 346
    target 0
    weight 0.941580102943333
  ]
  edge
  [
    source 346
    target 2
    weight 0.766908644659995
  ]
  edge
  [
    source 402
    target 1
    weight 0.811068322935762
  ]
  edge
  [
    source 337
    target 3
    weight 0.78574938475992
  ]
  edge
  [
    source 155
    target 0
    weight 0.849455227051527
  ]
  edge
  [
    source 135
    target 2
    weight 0.818322077811757
  ]
  edge
  [
    source 379
    target 1
    weight 0.928236249057793
  ]
  edge
  [
    source 251
    target 2
    weight 0.787408673816586
  ]
  edge
  [
    source 357
    target 1
    weight 0.88181319950894
  ]
  edge
  [
    source 347
    target 0
    weight 0.760059706136995
  ]
  edge
  [
    source 276
    target 2
    weight 0.948457534304265
  ]
  edge
  [
    source 278
    target 0
    weight 0.841896836156705
  ]
  edge
  [
    source 287
    target 1
    weight 0.949542023830215
  ]
  edge
  [
    source 177
    target 2
    weight 0.93215278067996
  ]
  edge
  [
    source 344
    target 2
    weight 0.789962676580701
  ]
  edge
  [
    source 269
    target 0
    weight 0.858611374504292
  ]
  edge
  [
    source 192
    target 3
    weight 0.765861551058083
  ]
  edge
  [
    source 292
    target 2
    weight 0.864396269208093
  ]
  edge
  [
    source 58
    target 1
    weight 0.911191846719872
  ]
  edge
  [
    source 187
    target 2
    weight 0.840916579401113
  ]
  edge
  [
    source 307
    target 1
    weight 0.922125514700804
  ]
  edge
  [
    source 163
    target 1
    weight 0.895976249885509
  ]
  edge
  [
    source 311
    target 3
    weight 0.819049934611253
  ]
  edge
  [
    source 28
    target 0
    weight 0.859551041988372
  ]
  edge
  [
    source 176
    target 1
    weight 0.816153577608615
  ]
  edge
  [
    source 311
    target 1
    weight 0.950519338245594
  ]
  edge
  [
    source 259
    target 0
    weight 0.862908667067827
  ]
  edge
  [
    source 171
    target 1
    weight 0.91805243756884
  ]
  edge
  [
    source 229
    target 3
    weight 0.841088222929763
  ]
  edge
  [
    source 27
    target 1
    weight 0.905490495950566
  ]
  edge
  [
    source 230
    target 0
    weight 0.782459728367028
  ]
  edge
  [
    source 180
    target 1
    weight 0.860691431488103
  ]
  edge
  [
    source 208
    target 2
    weight 0.850436365026462
  ]
  edge
  [
    source 252
    target 2
    weight 0.877180109105329
  ]
  edge
  [
    source 348
    target 1
    weight 0.783379245015604
  ]
  edge
  [
    source 245
    target 2
    weight 0.860885154170135
  ]
  edge
  [
    source 250
    target 2
    weight 0.902672362678077
  ]
  edge
  [
    source 246
    target 2
    weight 0.868174262519096
  ]
  edge
  [
    source 149
    target 0
    weight 0.900122696082089
  ]
  edge
  [
    source 67
    target 1
    weight 0.895596511718997
  ]
  edge
  [
    source 319
    target 2
    weight 0.874145933678781
  ]
  edge
  [
    source 403
    target 1
    weight 0.784234171041604
  ]
  edge
  [
    source 75
    target 3
    weight 0.788434004684657
  ]
  edge
  [
    source 297
    target 0
    weight 0.856416121979879
  ]
  edge
  [
    source 367
    target 2
    weight 0.93065416694745
  ]
  edge
  [
    source 189
    target 0
    weight 0.833246582818973
  ]
  edge
  [
    source 366
    target 2
    weight 0.868382713507093
  ]
  edge
  [
    source 322
    target 1
    weight 0.826721471925908
  ]
  edge
  [
    source 185
    target 0
    weight 0.861640852500372
  ]
  edge
  [
    source 38
    target 1
    weight 0.755725225109347
  ]
  edge
  [
    source 123
    target 1
    weight 0.935753193828871
  ]
  edge
  [
    source 176
    target 0
    weight 0.788113018332487
  ]
  edge
  [
    source 218
    target 2
    weight 0.821225549259849
  ]
  edge
  [
    source 339
    target 1
    weight 0.756721833955806
  ]
  edge
  [
    source 117
    target 1
    weight 0.954537807704372
  ]
  edge
  [
    source 197
    target 0
    weight 0.889536469540948
  ]
  edge
  [
    source 237
    target 0
    weight 0.828634408447068
  ]
  edge
  [
    source 165
    target 1
    weight 0.944537088111125
  ]
  edge
  [
    source 306
    target 1
    weight 0.873361809235041
  ]
  edge
  [
    source 66
    target 3
    weight 0.877484391635923
  ]
  edge
  [
    source 404
    target 0
    weight 0.763185617223288
  ]
  edge
  [
    source 205
    target 0
    weight 0.843629798310687
  ]
  edge
  [
    source 60
    target 3
    weight 0.884239739443692
  ]
  edge
  [
    source 284
    target 1
    weight 0.852691306023889
  ]
  edge
  [
    source 312
    target 3
    weight 0.860576805730772
  ]
  edge
  [
    source 171
    target 0
    weight 0.913141523449625
  ]
  edge
  [
    source 7
    target 1
    weight 0.917921778835394
  ]
  edge
  [
    source 198
    target 0
    weight 0.790905677568003
  ]
  edge
  [
    source 363
    target 0
    weight 0.763034347938541
  ]
  edge
  [
    source 327
    target 1
    weight 0.894064050928876
  ]
  edge
  [
    source 278
    target 1
    weight 0.761514253384762
  ]
  edge
  [
    source 253
    target 2
    weight 0.872744824733104
  ]
  edge
  [
    source 349
    target 1
    weight 0.80061586861016
  ]
  edge
  [
    source 104
    target 1
    weight 0.864588779139335
  ]
  edge
  [
    source 297
    target 2
    weight 0.828004827815334
  ]
  edge
  [
    source 258
    target 0
    weight 0.783071937940091
  ]
  edge
  [
    source 35
    target 0
    weight 0.780095906392874
  ]
  edge
  [
    source 174
    target 3
    weight 0.816219078128228
  ]
  edge
  [
    source 35
    target 1
    weight 0.949695474951308
  ]
  edge
  [
    source 259
    target 1
    weight 0.925925748628941
  ]
  edge
  [
    source 119
    target 1
    weight 0.77376577793141
  ]
  edge
  [
    source 266
    target 1
    weight 0.897453253102396
  ]
  edge
  [
    source 70
    target 1
    weight 0.887115821555036
  ]
  edge
  [
    source 235
    target 3
    weight 0.753330985824374
  ]
  edge
  [
    source 12
    target 0
    weight 0.924112605826057
  ]
  edge
  [
    source 345
    target 2
    weight 0.799418820357156
  ]
  edge
  [
    source 75
    target 1
    weight 0.94048537633911
  ]
  edge
  [
    source 216
    target 0
    weight 0.761599347794831
  ]
  edge
  [
    source 31
    target 1
    weight 0.878796669098218
  ]
  edge
  [
    source 241
    target 2
    weight 0.839392673060052
  ]
  edge
  [
    source 350
    target 1
    weight 0.752406515855065
  ]
  edge
  [
    source 351
    target 0
    weight 0.822059576433236
  ]
  edge
  [
    source 151
    target 3
    weight 0.79991543482001
  ]
  edge
  [
    source 201
    target 2
    weight 0.927077514132808
  ]
  edge
  [
    source 47
    target 3
    weight 0.93384500847349
  ]
  edge
  [
    source 67
    target 0
    weight 0.919408333939115
  ]
  edge
  [
    source 10
    target 1
    weight 0.850047550144587
  ]
  edge
  [
    source 146
    target 2
    weight 0.876765762773604
  ]
  edge
  [
    source 352
    target 3
    weight 0.791655618080439
  ]
  edge
  [
    source 353
    target 1
    weight 0.770749875534496
  ]
  edge
  [
    source 362
    target 2
    weight 0.920466432666569
  ]
  edge
  [
    source 173
    target 0
    weight 0.850662168191474
  ]
  edge
  [
    source 295
    target 0
    weight 0.909349069408529
  ]
  edge
  [
    source 283
    target 2
    weight 0.943516851469704
  ]
  edge
  [
    source 46
    target 1
    weight 0.875265833115571
  ]
  edge
  [
    source 405
    target 1
    weight 0.857850316623365
  ]
  edge
  [
    source 363
    target 3
    weight 0.792740777411785
  ]
  edge
  [
    source 3
    target 1
    weight 0.820250918895127
  ]
  edge
  [
    source 101
    target 1
    weight 0.96264307270091
  ]
  edge
  [
    source 79
    target 0
    weight 0.793321757791217
  ]
  edge
  [
    source 103
    target 3
    weight 0.779878183271885
  ]
  edge
  [
    source 20
    target 3
    weight 0.778835990411677
  ]
  edge
  [
    source 221
    target 3
    weight 0.797247095983063
  ]
  edge
  [
    source 354
    target 2
    weight 0.920789711481518
  ]
  edge
  [
    source 11
    target 0
    weight 0.934480915490045
  ]
  edge
  [
    source 371
    target 0
    weight 0.908280457785402
  ]
  edge
  [
    source 271
    target 1
    weight 0.884254912354555
  ]
  edge
  [
    source 355
    target 2
    weight 0.778862254396756
  ]
  edge
  [
    source 199
    target 0
    weight 0.904932476319474
  ]
  edge
  [
    source 202
    target 0
    weight 0.831933516109491
  ]
  edge
  [
    source 356
    target 3
    weight 0.83299169139692
  ]
  edge
  [
    source 300
    target 1
    weight 0.909690277594762
  ]
  edge
  [
    source 303
    target 1
    weight 0.88717326592531
  ]
  edge
  [
    source 321
    target 1
    weight 0.884411416939699
  ]
  edge
  [
    source 357
    target 2
    weight 0.895427102105458
  ]
  edge
  [
    source 217
    target 2
    weight 0.942670596143197
  ]
  edge
  [
    source 358
    target 1
    weight 0.762715693358832
  ]
  edge
  [
    source 182
    target 3
    weight 0.809203336171578
  ]
  edge
  [
    source 222
    target 2
    weight 0.934881081230585
  ]
  edge
  [
    source 118
    target 1
    weight 0.938767007850268
  ]
  edge
  [
    source 342
    target 2
    weight 0.781106980856784
  ]
  edge
  [
    source 385
    target 0
    weight 0.779847726078342
  ]
  edge
  [
    source 24
    target 3
    weight 0.867564417372688
  ]
  edge
  [
    source 378
    target 1
    weight 0.799710560228681
  ]
  edge
  [
    source 406
    target 1
    weight 0.792728470501963
  ]
  edge
  [
    source 255
    target 0
    weight 0.808058395620618
  ]
  edge
  [
    source 242
    target 0
    weight 0.91918640812995
  ]
  edge
  [
    source 338
    target 2
    weight 0.820985931335228
  ]
  edge
  [
    source 163
    target 0
    weight 0.780617816318627
  ]
  edge
  [
    source 86
    target 3
    weight 0.889536861051382
  ]
  edge
  [
    source 318
    target 1
    weight 0.902750501157854
  ]
  edge
  [
    source 281
    target 1
    weight 0.961572263452128
  ]
  edge
  [
    source 13
    target 0
    weight 0.761510043961359
  ]
  edge
  [
    source 287
    target 2
    weight 0.968328257610786
  ]
  edge
  [
    source 38
    target 0
    weight 0.795162973768016
  ]
  edge
  [
    source 164
    target 1
    weight 0.883699655528826
  ]
  edge
  [
    source 60
    target 1
    weight 0.8838850169597
  ]
  edge
  [
    source 359
    target 2
    weight 0.926366893854323
  ]
  edge
  [
    source 134
    target 1
    weight 0.783946105447877
  ]
  edge
  [
    source 221
    target 0
    weight 0.826338897617825
  ]
  edge
  [
    source 291
    target 2
    weight 0.913804219643387
  ]
  edge
  [
    source 307
    target 3
    weight 0.789933711274267
  ]
]
