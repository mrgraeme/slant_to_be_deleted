Creator "igraph version @VERSION@ Thu Jul 20 09:50:53 2017"
Version 1
graph
[
  directed 0
  node
  [
    id 0
    name "PBRM1"
  ]
  node
  [
    id 1
    name "TBPL1"
  ]
  node
  [
    id 2
    name "MRPL15"
  ]
  node
  [
    id 3
    name "HDAC9"
  ]
  node
  [
    id 4
    name "ACTL6A"
  ]
  node
  [
    id 5
    name "LPXN"
  ]
  node
  [
    id 6
    name "GLI3"
  ]
  node
  [
    id 7
    name "STRN"
  ]
  node
  [
    id 8
    name "NDC80"
  ]
  node
  [
    id 9
    name "RPA2"
  ]
  node
  [
    id 10
    name "ZPR1"
  ]
  node
  [
    id 11
    name "NFE2L2"
  ]
  node
  [
    id 12
    name "PPP2CB"
  ]
  node
  [
    id 13
    name "TOP2A"
  ]
  node
  [
    id 14
    name "TRIP12"
  ]
  node
  [
    id 15
    name "USP2"
  ]
  node
  [
    id 16
    name "PSEN1"
  ]
  node
  [
    id 17
    name "JAK2"
  ]
  node
  [
    id 18
    name "TGFBR1"
  ]
  node
  [
    id 19
    name "GSK3B"
  ]
  node
  [
    id 20
    name "KMT2A"
  ]
  node
  [
    id 21
    name "SNRPB2"
  ]
  node
  [
    id 22
    name "ABL1"
  ]
  node
  [
    id 23
    name "SKP2"
  ]
  node
  [
    id 24
    name "MED19"
  ]
  node
  [
    id 25
    name "NFKB1"
  ]
  node
  [
    id 26
    name "RHOA"
  ]
  node
  [
    id 27
    name "MSH6"
  ]
  node
  [
    id 28
    name "GRAP2"
  ]
  node
  [
    id 29
    name "RBMX"
  ]
  node
  [
    id 30
    name "EYA4"
  ]
  node
  [
    id 31
    name "SLC9A3R1"
  ]
  node
  [
    id 32
    name "ACVRL1"
  ]
  node
  [
    id 33
    name "PKMYT1"
  ]
  node
  [
    id 34
    name "TWIST1"
  ]
  node
  [
    id 35
    name "SMARCB1"
  ]
  node
  [
    id 36
    name "POLA1"
  ]
  node
  [
    id 37
    name "RNF20"
  ]
  node
  [
    id 38
    name "ACTR6"
  ]
  node
  [
    id 39
    name "CDC73"
  ]
  node
  [
    id 40
    name "RB1"
  ]
  node
  [
    id 41
    name "TPR"
  ]
  node
  [
    id 42
    name "BIRC6"
  ]
  node
  [
    id 43
    name "TRA2B"
  ]
  node
  [
    id 44
    name "STAT1"
  ]
  node
  [
    id 45
    name "NCK2"
  ]
  node
  [
    id 46
    name "CSNK2A1"
  ]
  node
  [
    id 47
    name "DMC1"
  ]
  node
  [
    id 48
    name "MCM4"
  ]
  node
  [
    id 49
    name "SRSF5"
  ]
  node
  [
    id 50
    name "RPTOR"
  ]
  node
  [
    id 51
    name "USP8"
  ]
  node
  [
    id 52
    name "MAX"
  ]
  node
  [
    id 53
    name "FBXW7"
  ]
  node
  [
    id 54
    name "DNA2"
  ]
  node
  [
    id 55
    name "CCNC"
  ]
  node
  [
    id 56
    name "SRSF1"
  ]
  node
  [
    id 57
    name "MET"
  ]
  node
  [
    id 58
    name "SMARCE1"
  ]
  node
  [
    id 59
    name "RPS6KB1"
  ]
  node
  [
    id 60
    name "BRD4"
  ]
  node
  [
    id 61
    name "KRAS"
  ]
  node
  [
    id 62
    name "UBA2"
  ]
  node
  [
    id 63
    name "SSRP1"
  ]
  node
  [
    id 64
    name "BRCA1"
  ]
  node
  [
    id 65
    name "STAG1"
  ]
  node
  [
    id 66
    name "SAE1"
  ]
  node
  [
    id 67
    name "RCHY1"
  ]
  node
  [
    id 68
    name "CDKN1B"
  ]
  node
  [
    id 69
    name "SRSF6"
  ]
  node
  [
    id 70
    name "UBTF"
  ]
  node
  [
    id 71
    name "KPNB1"
  ]
  node
  [
    id 72
    name "CHEK1"
  ]
  node
  [
    id 73
    name "SMAD4"
  ]
  node
  [
    id 74
    name "FANCD2"
  ]
  node
  [
    id 75
    name "ARID1B"
  ]
  node
  [
    id 76
    name "IQGAP1"
  ]
  node
  [
    id 77
    name "PARP1"
  ]
  node
  [
    id 78
    name "RAD18"
  ]
  node
  [
    id 79
    name "UBE2D1"
  ]
  node
  [
    id 80
    name "ITGB1"
  ]
  node
  [
    id 81
    name "MYC"
  ]
  node
  [
    id 82
    name "EEF1A1"
  ]
  node
  [
    id 83
    name "RLIM"
  ]
  node
  [
    id 84
    name "TP53BP2"
  ]
  node
  [
    id 85
    name "NOTCH1"
  ]
  node
  [
    id 86
    name "ASF1A"
  ]
  node
  [
    id 87
    name "BECN1"
  ]
  node
  [
    id 88
    name "TAF6L"
  ]
  node
  [
    id 89
    name "HUWE1"
  ]
  node
  [
    id 90
    name "COPS4"
  ]
  node
  [
    id 91
    name "KAT7"
  ]
  node
  [
    id 92
    name "SMC6"
  ]
  node
  [
    id 93
    name "FGFR2"
  ]
  node
  [
    id 94
    name "APC"
  ]
  node
  [
    id 95
    name "RBX1"
  ]
  node
  [
    id 96
    name "BRCA2"
  ]
  node
  [
    id 97
    name "USP33"
  ]
  node
  [
    id 98
    name "MAPK1"
  ]
  node
  [
    id 99
    name "ASF1B"
  ]
  node
  [
    id 100
    name "ETS1"
  ]
  node
  [
    id 101
    name "HDAC1"
  ]
  node
  [
    id 102
    name "WASL"
  ]
  node
  [
    id 103
    name "CDC5L"
  ]
  node
  [
    id 104
    name "MLH1"
  ]
  node
  [
    id 105
    name "THOC1"
  ]
  node
  [
    id 106
    name "KLF10"
  ]
  node
  [
    id 107
    name "CBX1"
  ]
  node
  [
    id 108
    name "LMO3"
  ]
  node
  [
    id 109
    name "CUL4B"
  ]
  node
  [
    id 110
    name "RAN"
  ]
  node
  [
    id 111
    name "SMARCA1"
  ]
  node
  [
    id 112
    name "ENSA"
  ]
  node
  [
    id 113
    name "RAF1"
  ]
  node
  [
    id 114
    name "NCAPD2"
  ]
  node
  [
    id 115
    name "CNOT1"
  ]
  node
  [
    id 116
    name "UTP20"
  ]
  node
  [
    id 117
    name "RPS6KA1"
  ]
  node
  [
    id 118
    name "PRMT8"
  ]
  node
  [
    id 119
    name "CLNS1A"
  ]
  node
  [
    id 120
    name "HNRNPM"
  ]
  node
  [
    id 121
    name "EPS15"
  ]
  node
  [
    id 122
    name "USP10"
  ]
  node
  [
    id 123
    name "ERCC5"
  ]
  node
  [
    id 124
    name "MEN1"
  ]
  node
  [
    id 125
    name "MED7"
  ]
  node
  [
    id 126
    name "CHTF18"
  ]
  node
  [
    id 127
    name "PDS5B"
  ]
  node
  [
    id 128
    name "PPP4C"
  ]
  node
  [
    id 129
    name "DYNLL1"
  ]
  node
  [
    id 130
    name "UBR5"
  ]
  node
  [
    id 131
    name "NXF1"
  ]
  node
  [
    id 132
    name "PXN"
  ]
  node
  [
    id 133
    name "CDC27"
  ]
  node
  [
    id 134
    name "ARID1A"
  ]
  node
  [
    id 135
    name "RGCC"
  ]
  node
  [
    id 136
    name "CDH1"
  ]
  node
  [
    id 137
    name "PPP2R5C"
  ]
  node
  [
    id 138
    name "ANAPC4"
  ]
  node
  [
    id 139
    name "PPP2R5D"
  ]
  node
  [
    id 140
    name "USP1"
  ]
  node
  [
    id 141
    name "XPO1"
  ]
  node
  [
    id 142
    name "NCL"
  ]
  node
  [
    id 143
    name "SRSF9"
  ]
  node
  [
    id 144
    name "SETD1B"
  ]
  node
  [
    id 145
    name "SUV39H1"
  ]
  node
  [
    id 146
    name "DKC1"
  ]
  node
  [
    id 147
    name "PPP3CA"
  ]
  node
  [
    id 148
    name "ZRANB1"
  ]
  node
  [
    id 149
    name "BUB1B"
  ]
  node
  [
    id 150
    name "SKP1"
  ]
  node
  [
    id 151
    name "KDM1A"
  ]
  node
  [
    id 152
    name "VPS25"
  ]
  node
  [
    id 153
    name "SH3KBP1"
  ]
  node
  [
    id 154
    name "CANX"
  ]
  node
  [
    id 155
    name "ACTR5"
  ]
  node
  [
    id 156
    name "CARM1"
  ]
  node
  [
    id 157
    name "AKT1"
  ]
  node
  [
    id 158
    name "PRKACA"
  ]
  node
  [
    id 159
    name "REV1"
  ]
  node
  [
    id 160
    name "CASP8"
  ]
  node
  [
    id 161
    name "CRY2"
  ]
  node
  [
    id 162
    name "ING4"
  ]
  node
  [
    id 163
    name "CREBBP"
  ]
  node
  [
    id 164
    name "SAP18"
  ]
  node
  [
    id 165
    name "BIRC5"
  ]
  node
  [
    id 166
    name "PMS2"
  ]
  node
  [
    id 167
    name "RAD17"
  ]
  node
  [
    id 168
    name "ERBB2"
  ]
  node
  [
    id 169
    name "TAF1"
  ]
  node
  [
    id 170
    name "CCNA2"
  ]
  node
  [
    id 171
    name "TCEB1"
  ]
  node
  [
    id 172
    name "MORF4L2"
  ]
  node
  [
    id 173
    name "CBFA2T3"
  ]
  node
  [
    id 174
    name "SNW1"
  ]
  node
  [
    id 175
    name "SNAP23"
  ]
  node
  [
    id 176
    name "UBE2D3"
  ]
  node
  [
    id 177
    name "NCOR1"
  ]
  node
  [
    id 178
    name "SENP3"
  ]
  node
  [
    id 179
    name "EIF4A3"
  ]
  node
  [
    id 180
    name "CCT3"
  ]
  node
  [
    id 181
    name "SMARCA2"
  ]
  node
  [
    id 182
    name "ATM"
  ]
  node
  [
    id 183
    name "PPP1R8"
  ]
  node
  [
    id 184
    name "RFWD2"
  ]
  node
  [
    id 185
    name "JUND"
  ]
  node
  [
    id 186
    name "RNF40"
  ]
  node
  [
    id 187
    name "UBE2I"
  ]
  node
  [
    id 188
    name "MARK2"
  ]
  node
  [
    id 189
    name "CDK6"
  ]
  node
  [
    id 190
    name "IGF1R"
  ]
  node
  [
    id 191
    name "PRKCD"
  ]
  node
  [
    id 192
    name "CDK2"
  ]
  node
  [
    id 193
    name "TAF5L"
  ]
  node
  [
    id 194
    name "COPS5"
  ]
  node
  [
    id 195
    name "PIN1"
  ]
  node
  [
    id 196
    name "RFC1"
  ]
  node
  [
    id 197
    name "STAM2"
  ]
  node
  [
    id 198
    name "SKIL"
  ]
  node
  [
    id 199
    name "LATS2"
  ]
  node
  [
    id 200
    name "CDC16"
  ]
  node
  [
    id 201
    name "CSNK1E"
  ]
  node
  [
    id 202
    name "SUPT4H1"
  ]
  node
  [
    id 203
    name "MTOR"
  ]
  node
  [
    id 204
    name "WDHD1"
  ]
  node
  [
    id 205
    name "SND1"
  ]
  node
  [
    id 206
    name "NOC2L"
  ]
  node
  [
    id 207
    name "EP400"
  ]
  node
  [
    id 208
    name "ERN1"
  ]
  node
  [
    id 209
    name "CALR"
  ]
  node
  [
    id 210
    name "HGS"
  ]
  node
  [
    id 211
    name "POLE"
  ]
  node
  [
    id 212
    name "HRAS"
  ]
  node
  [
    id 213
    name "SMARCC1"
  ]
  node
  [
    id 214
    name "JUNB"
  ]
  node
  [
    id 215
    name "SMAD1"
  ]
  node
  [
    id 216
    name "COPS6"
  ]
  node
  [
    id 217
    name "PLK1"
  ]
  node
  [
    id 218
    name "CHEK2"
  ]
  node
  [
    id 219
    name "UBE2V2"
  ]
  node
  [
    id 220
    name "MAP2K1"
  ]
  node
  [
    id 221
    name "WEE1"
  ]
  node
  [
    id 222
    name "COPS2"
  ]
  node
  [
    id 223
    name "CPSF2"
  ]
  node
  [
    id 224
    name "AURKB"
  ]
  node
  [
    id 225
    name "FEN1"
  ]
  node
  [
    id 226
    name "NUP62"
  ]
  node
  [
    id 227
    name "CTBP2"
  ]
  node
  [
    id 228
    name "SEPT2"
  ]
  node
  [
    id 229
    name "GPN1"
  ]
  node
  [
    id 230
    name "NPLOC4"
  ]
  node
  [
    id 231
    name "DCTN1"
  ]
  node
  [
    id 232
    name "CKS1B"
  ]
  node
  [
    id 233
    name "TBL1XR1"
  ]
  node
  [
    id 234
    name "FANCA"
  ]
  node
  [
    id 235
    name "PTEN"
  ]
  node
  [
    id 236
    name "CDC25A"
  ]
  node
  [
    id 237
    name "IARS"
  ]
  node
  [
    id 238
    name "CTNND1"
  ]
  node
  [
    id 239
    name "LYN"
  ]
  edge
  [
    source 201
    target 0
    weight 0.616993333239066
  ]
  edge
  [
    source 202
    target 0
    weight 0.770572322106486
  ]
  edge
  [
    source 203
    target 0
    weight 0.635896126279763
  ]
  edge
  [
    source 204
    target 0
    weight 0.68268986785599
  ]
  edge
  [
    source 1
    target 0
    weight 0.609483962447108
  ]
  edge
  [
    source 205
    target 0
    weight 0.776376529503851
  ]
  edge
  [
    source 2
    target 0
    weight 0.630285769739997
  ]
  edge
  [
    source 3
    target 0
    weight 0.754839789403455
  ]
  edge
  [
    source 4
    target 0
    weight 0.827636020233267
  ]
  edge
  [
    source 5
    target 0
    weight 0.669209345262505
  ]
  edge
  [
    source 6
    target 0
    weight 0.605182903325823
  ]
  edge
  [
    source 206
    target 0
    weight 0.673944833350461
  ]
  edge
  [
    source 7
    target 0
    weight 0.714355097954427
  ]
  edge
  [
    source 8
    target 0
    weight 0.774006544380503
  ]
  edge
  [
    source 9
    target 0
    weight 0.670255206702438
  ]
  edge
  [
    source 207
    target 0
    weight 0.601150666489417
  ]
  edge
  [
    source 208
    target 0
    weight 0.773865059910735
  ]
  edge
  [
    source 209
    target 0
    weight 0.747689792263234
  ]
  edge
  [
    source 10
    target 0
    weight 0.626172463219902
  ]
  edge
  [
    source 11
    target 0
    weight 0.652198315688461
  ]
  edge
  [
    source 12
    target 0
    weight 0.61416179247371
  ]
  edge
  [
    source 13
    target 0
    weight 0.796986879890243
  ]
  edge
  [
    source 210
    target 0
    weight 0.77557656453634
  ]
  edge
  [
    source 211
    target 0
    weight 0.659125926104766
  ]
  edge
  [
    source 212
    target 0
    weight 0.683142305979021
  ]
  edge
  [
    source 213
    target 0
    weight 0.830665149307821
  ]
  edge
  [
    source 214
    target 0
    weight 0.679535110280216
  ]
  edge
  [
    source 14
    target 0
    weight 0.660610554836995
  ]
  edge
  [
    source 15
    target 0
    weight 0.631704705819106
  ]
  edge
  [
    source 16
    target 0
    weight 0.637170804678413
  ]
  edge
  [
    source 215
    target 0
    weight 0.773852200000737
  ]
  edge
  [
    source 216
    target 0
    weight 0.604977881923157
  ]
  edge
  [
    source 217
    target 0
    weight 0.805786336336269
  ]
  edge
  [
    source 218
    target 0
    weight 0.853072030154683
  ]
  edge
  [
    source 219
    target 0
    weight 0.720174024413669
  ]
  edge
  [
    source 220
    target 0
    weight 0.746652404905534
  ]
  edge
  [
    source 17
    target 0
    weight 0.791886635059639
  ]
  edge
  [
    source 18
    target 0
    weight 0.80282167385717
  ]
  edge
  [
    source 19
    target 0
    weight 0.662000596778617
  ]
  edge
  [
    source 20
    target 0
    weight 0.764037617037372
  ]
  edge
  [
    source 21
    target 0
    weight 0.660820979271872
  ]
  edge
  [
    source 221
    target 0
    weight 0.80452324365354
  ]
  edge
  [
    source 222
    target 0
    weight 0.716497737383464
  ]
  edge
  [
    source 22
    target 0
    weight 0.750563574977625
  ]
  edge
  [
    source 223
    target 0
    weight 0.666922634089163
  ]
  edge
  [
    source 224
    target 0
    weight 0.717741822062888
  ]
  edge
  [
    source 23
    target 0
    weight 0.642481526819293
  ]
  edge
  [
    source 24
    target 0
    weight 0.659644337208801
  ]
  edge
  [
    source 25
    target 0
    weight 0.61513450148555
  ]
  edge
  [
    source 26
    target 0
    weight 0.667558225858368
  ]
  edge
  [
    source 27
    target 0
    weight 0.793919526637283
  ]
  edge
  [
    source 28
    target 0
    weight 0.642257909835319
  ]
  edge
  [
    source 29
    target 0
    weight 0.700472470847278
  ]
  edge
  [
    source 30
    target 0
    weight 0.689405770729694
  ]
  edge
  [
    source 31
    target 0
    weight 0.776576718805749
  ]
  edge
  [
    source 32
    target 0
    weight 0.776530851903472
  ]
  edge
  [
    source 33
    target 0
    weight 0.80809237135592
  ]
  edge
  [
    source 34
    target 0
    weight 0.685453920086117
  ]
  edge
  [
    source 35
    target 0
    weight 0.81182914746904
  ]
  edge
  [
    source 36
    target 0
    weight 0.636753070193177
  ]
  edge
  [
    source 37
    target 0
    weight 0.705063403622708
  ]
  edge
  [
    source 38
    target 0
    weight 0.704536553711559
  ]
  edge
  [
    source 39
    target 0
    weight 0.791171736033575
  ]
  edge
  [
    source 40
    target 0
    weight 0.784217396530449
  ]
  edge
  [
    source 41
    target 0
    weight 0.797100971854725
  ]
  edge
  [
    source 42
    target 0
    weight 0.788052319670521
  ]
  edge
  [
    source 43
    target 0
    weight 0.641835826133684
  ]
  edge
  [
    source 44
    target 0
    weight 0.799232788892022
  ]
  edge
  [
    source 45
    target 0
    weight 0.734436080955635
  ]
  edge
  [
    source 46
    target 0
    weight 0.849863128172576
  ]
  edge
  [
    source 47
    target 0
    weight 0.600379090938705
  ]
  edge
  [
    source 48
    target 0
    weight 0.650930146526829
  ]
  edge
  [
    source 49
    target 0
    weight 0.688726051847566
  ]
  edge
  [
    source 50
    target 0
    weight 0.633217639789078
  ]
  edge
  [
    source 51
    target 0
    weight 0.741965241216933
  ]
  edge
  [
    source 52
    target 0
    weight 0.778944790146724
  ]
  edge
  [
    source 53
    target 0
    weight 0.668938307948681
  ]
  edge
  [
    source 54
    target 0
    weight 0.643550635851931
  ]
  edge
  [
    source 55
    target 0
    weight 0.60725955023354
  ]
  edge
  [
    source 56
    target 0
    weight 0.638041629440942
  ]
  edge
  [
    source 57
    target 0
    weight 0.623734101900836
  ]
  edge
  [
    source 58
    target 0
    weight 0.869612818460741
  ]
  edge
  [
    source 59
    target 0
    weight 0.631370724566521
  ]
  edge
  [
    source 60
    target 0
    weight 0.741204928910698
  ]
  edge
  [
    source 61
    target 0
    weight 0.681899863333524
  ]
  edge
  [
    source 62
    target 0
    weight 0.646174337929539
  ]
  edge
  [
    source 63
    target 0
    weight 0.810171069285497
  ]
  edge
  [
    source 64
    target 0
    weight 0.711281295457111
  ]
  edge
  [
    source 65
    target 0
    weight 0.627606453359591
  ]
  edge
  [
    source 66
    target 0
    weight 0.656289681452409
  ]
  edge
  [
    source 67
    target 0
    weight 0.71768512499951
  ]
  edge
  [
    source 68
    target 0
    weight 0.828178003763563
  ]
  edge
  [
    source 69
    target 0
    weight 0.697858761629532
  ]
  edge
  [
    source 70
    target 0
    weight 0.679974118389112
  ]
  edge
  [
    source 71
    target 0
    weight 0.695441016447476
  ]
  edge
  [
    source 72
    target 0
    weight 0.622469743803294
  ]
  edge
  [
    source 73
    target 0
    weight 0.806436763296566
  ]
  edge
  [
    source 74
    target 0
    weight 0.64269699279443
  ]
  edge
  [
    source 75
    target 0
    weight 0.868978329930464
  ]
  edge
  [
    source 76
    target 0
    weight 0.622355863769357
  ]
  edge
  [
    source 77
    target 0
    weight 0.63482361716321
  ]
  edge
  [
    source 78
    target 0
    weight 0.632624035477061
  ]
  edge
  [
    source 79
    target 0
    weight 0.653949245655847
  ]
  edge
  [
    source 80
    target 0
    weight 0.644116456786876
  ]
  edge
  [
    source 81
    target 0
    weight 0.76110329359768
  ]
  edge
  [
    source 82
    target 0
    weight 0.807144701651232
  ]
  edge
  [
    source 83
    target 0
    weight 0.64691640143315
  ]
  edge
  [
    source 84
    target 0
    weight 0.741360098218915
  ]
  edge
  [
    source 85
    target 0
    weight 0.836890282373746
  ]
  edge
  [
    source 86
    target 0
    weight 0.820127452031697
  ]
  edge
  [
    source 87
    target 0
    weight 0.785280735565005
  ]
  edge
  [
    source 88
    target 0
    weight 0.724831849768219
  ]
  edge
  [
    source 89
    target 0
    weight 0.699899606094197
  ]
  edge
  [
    source 90
    target 0
    weight 0.65459047478241
  ]
  edge
  [
    source 91
    target 0
    weight 0.791919343862723
  ]
  edge
  [
    source 92
    target 0
    weight 0.633013809452206
  ]
  edge
  [
    source 93
    target 0
    weight 0.684149212393771
  ]
  edge
  [
    source 94
    target 0
    weight 0.830723979745442
  ]
  edge
  [
    source 95
    target 0
    weight 0.677715857306914
  ]
  edge
  [
    source 96
    target 0
    weight 0.650487478732561
  ]
  edge
  [
    source 97
    target 0
    weight 0.707360103061589
  ]
  edge
  [
    source 98
    target 0
    weight 0.774355713273076
  ]
  edge
  [
    source 99
    target 0
    weight 0.825556977591205
  ]
  edge
  [
    source 100
    target 0
    weight 0.725918525363177
  ]
  edge
  [
    source 101
    target 0
    weight 0.712946800456851
  ]
  edge
  [
    source 102
    target 0
    weight 0.809495593593342
  ]
  edge
  [
    source 103
    target 0
    weight 0.679739200829916
  ]
  edge
  [
    source 104
    target 0
    weight 0.628634475161903
  ]
  edge
  [
    source 105
    target 0
    weight 0.623142534981063
  ]
  edge
  [
    source 106
    target 0
    weight 0.697675008031817
  ]
  edge
  [
    source 107
    target 0
    weight 0.622619971912217
  ]
  edge
  [
    source 108
    target 0
    weight 0.69686076314924
  ]
  edge
  [
    source 109
    target 0
    weight 0.689176739503581
  ]
  edge
  [
    source 110
    target 0
    weight 0.770139609681699
  ]
  edge
  [
    source 111
    target 0
    weight 0.745900328490718
  ]
  edge
  [
    source 112
    target 0
    weight 0.604415135278125
  ]
  edge
  [
    source 113
    target 0
    weight 0.798897724959453
  ]
  edge
  [
    source 114
    target 0
    weight 0.757963602849452
  ]
  edge
  [
    source 115
    target 0
    weight 0.744770701834734
  ]
  edge
  [
    source 116
    target 0
    weight 0.750234559895284
  ]
  edge
  [
    source 117
    target 0
    weight 0.762127795360143
  ]
  edge
  [
    source 118
    target 0
    weight 0.634652977382547
  ]
  edge
  [
    source 119
    target 0
    weight 0.631142089438203
  ]
  edge
  [
    source 120
    target 0
    weight 0.690853960398558
  ]
  edge
  [
    source 121
    target 0
    weight 0.673679771753702
  ]
  edge
  [
    source 122
    target 0
    weight 0.631105483670564
  ]
  edge
  [
    source 123
    target 0
    weight 0.692623807819891
  ]
  edge
  [
    source 124
    target 0
    weight 0.773354960804213
  ]
  edge
  [
    source 125
    target 0
    weight 0.608714222841515
  ]
  edge
  [
    source 126
    target 0
    weight 0.636278648230575
  ]
  edge
  [
    source 127
    target 0
    weight 0.796440236340294
  ]
  edge
  [
    source 128
    target 0
    weight 0.633238361648069
  ]
  edge
  [
    source 225
    target 0
    weight 0.673415761154596
  ]
  edge
  [
    source 129
    target 0
    weight 0.804869002826728
  ]
  edge
  [
    source 130
    target 0
    weight 0.654710583811144
  ]
  edge
  [
    source 131
    target 0
    weight 0.67496834214444
  ]
  edge
  [
    source 226
    target 0
    weight 0.730555191158995
  ]
  edge
  [
    source 132
    target 0
    weight 0.698552566203203
  ]
  edge
  [
    source 133
    target 0
    weight 0.683529215398445
  ]
  edge
  [
    source 134
    target 0
    weight 0.764788048140512
  ]
  edge
  [
    source 135
    target 0
    weight 0.683422100029821
  ]
  edge
  [
    source 136
    target 0
    weight 0.650531998368511
  ]
  edge
  [
    source 137
    target 0
    weight 0.743035964712115
  ]
  edge
  [
    source 227
    target 0
    weight 0.779182333164478
  ]
  edge
  [
    source 138
    target 0
    weight 0.708695547711002
  ]
  edge
  [
    source 139
    target 0
    weight 0.672936316368288
  ]
  edge
  [
    source 140
    target 0
    weight 0.736720488761523
  ]
  edge
  [
    source 228
    target 0
    weight 0.665836488939751
  ]
  edge
  [
    source 141
    target 0
    weight 0.674039523846034
  ]
  edge
  [
    source 142
    target 0
    weight 0.66433979263017
  ]
  edge
  [
    source 143
    target 0
    weight 0.659081041373193
  ]
  edge
  [
    source 144
    target 0
    weight 0.809172153724004
  ]
  edge
  [
    source 229
    target 0
    weight 0.680215109381843
  ]
  edge
  [
    source 230
    target 0
    weight 0.675782038275163
  ]
  edge
  [
    source 145
    target 0
    weight 0.819416710695399
  ]
  edge
  [
    source 146
    target 0
    weight 0.604964077310736
  ]
  edge
  [
    source 147
    target 0
    weight 0.731594241070789
  ]
  edge
  [
    source 148
    target 0
    weight 0.607761012121443
  ]
  edge
  [
    source 149
    target 0
    weight 0.820587736878588
  ]
  edge
  [
    source 150
    target 0
    weight 0.644997369613787
  ]
  edge
  [
    source 151
    target 0
    weight 0.81163248009862
  ]
  edge
  [
    source 152
    target 0
    weight 0.814730143694408
  ]
  edge
  [
    source 153
    target 0
    weight 0.618340319765753
  ]
  edge
  [
    source 154
    target 0
    weight 0.662279522775317
  ]
  edge
  [
    source 155
    target 0
    weight 0.724347160405125
  ]
  edge
  [
    source 231
    target 0
    weight 0.741717459064241
  ]
  edge
  [
    source 156
    target 0
    weight 0.835859807279489
  ]
  edge
  [
    source 157
    target 0
    weight 0.641677026178823
  ]
  edge
  [
    source 232
    target 0
    weight 0.706520195521827
  ]
  edge
  [
    source 158
    target 0
    weight 0.673900383326774
  ]
  edge
  [
    source 159
    target 0
    weight 0.620258672775521
  ]
  edge
  [
    source 160
    target 0
    weight 0.674245499477263
  ]
  edge
  [
    source 161
    target 0
    weight 0.66722213634969
  ]
  edge
  [
    source 162
    target 0
    weight 0.757938051818268
  ]
  edge
  [
    source 163
    target 0
    weight 0.688404178400952
  ]
  edge
  [
    source 233
    target 0
    weight 0.810914313167902
  ]
  edge
  [
    source 164
    target 0
    weight 0.82290815170548
  ]
  edge
  [
    source 165
    target 0
    weight 0.879427004306237
  ]
  edge
  [
    source 234
    target 0
    weight 0.692972907505728
  ]
  edge
  [
    source 235
    target 0
    weight 0.859649386169327
  ]
  edge
  [
    source 166
    target 0
    weight 0.607694497510039
  ]
  edge
  [
    source 167
    target 0
    weight 0.602542821905594
  ]
  edge
  [
    source 168
    target 0
    weight 0.806428470561043
  ]
  edge
  [
    source 169
    target 0
    weight 0.659767711095703
  ]
  edge
  [
    source 170
    target 0
    weight 0.784262929547372
  ]
  edge
  [
    source 171
    target 0
    weight 0.741738872569862
  ]
  edge
  [
    source 172
    target 0
    weight 0.81281678205466
  ]
  edge
  [
    source 173
    target 0
    weight 0.688240224926523
  ]
  edge
  [
    source 174
    target 0
    weight 0.61419993338003
  ]
  edge
  [
    source 175
    target 0
    weight 0.651120437437464
  ]
  edge
  [
    source 176
    target 0
    weight 0.700784281578485
  ]
  edge
  [
    source 177
    target 0
    weight 0.808219800097544
  ]
  edge
  [
    source 178
    target 0
    weight 0.745896672200893
  ]
  edge
  [
    source 179
    target 0
    weight 0.612455384193997
  ]
  edge
  [
    source 180
    target 0
    weight 0.618525508156828
  ]
  edge
  [
    source 181
    target 0
    weight 0.778979987055918
  ]
  edge
  [
    source 236
    target 0
    weight 0.842822110880599
  ]
  edge
  [
    source 182
    target 0
    weight 0.675838908143945
  ]
  edge
  [
    source 183
    target 0
    weight 0.649755284192737
  ]
  edge
  [
    source 184
    target 0
    weight 0.715977822100691
  ]
  edge
  [
    source 185
    target 0
    weight 0.627834766798943
  ]
  edge
  [
    source 186
    target 0
    weight 0.651571110405868
  ]
  edge
  [
    source 187
    target 0
    weight 0.826197745133695
  ]
  edge
  [
    source 188
    target 0
    weight 0.609309555911996
  ]
  edge
  [
    source 189
    target 0
    weight 0.800696545172183
  ]
  edge
  [
    source 237
    target 0
    weight 0.661247534398967
  ]
  edge
  [
    source 190
    target 0
    weight 0.613717255434119
  ]
  edge
  [
    source 191
    target 0
    weight 0.601825969384231
  ]
  edge
  [
    source 192
    target 0
    weight 0.820774423761797
  ]
  edge
  [
    source 238
    target 0
    weight 0.768963057357911
  ]
  edge
  [
    source 193
    target 0
    weight 0.751527092949856
  ]
  edge
  [
    source 194
    target 0
    weight 0.652505620481187
  ]
  edge
  [
    source 195
    target 0
    weight 0.67924076831035
  ]
  edge
  [
    source 196
    target 0
    weight 0.752701889407004
  ]
  edge
  [
    source 239
    target 0
    weight 0.733646883804254
  ]
  edge
  [
    source 197
    target 0
    weight 0.728335425543238
  ]
  edge
  [
    source 198
    target 0
    weight 0.657705094772207
  ]
  edge
  [
    source 199
    target 0
    weight 0.734076370856837
  ]
  edge
  [
    source 200
    target 0
    weight 0.792538320164403
  ]
]
