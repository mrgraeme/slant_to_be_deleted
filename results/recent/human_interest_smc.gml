Creator "igraph version 1.1.1 Thu Nov  9 10:38:58 2017"
Version 1
graph
[
  directed 0
  node
  [
    id 0
    name "CASP8"
  ]
  node
  [
    id 1
    name "PARP1"
  ]
  node
  [
    id 2
    name "PRKCD"
  ]
  node
  [
    id 3
    name "CDC16"
  ]
  node
  [
    id 4
    name "IQGAP1"
  ]
  node
  [
    id 5
    name "NFE2L2"
  ]
  node
  [
    id 6
    name "HUWE1"
  ]
  node
  [
    id 7
    name "SMARCB1"
  ]
  node
  [
    id 8
    name "ARRB2"
  ]
  node
  [
    id 9
    name "BIRC5"
  ]
  node
  [
    id 10
    name "APC"
  ]
  node
  [
    id 11
    name "GSK3B"
  ]
  node
  [
    id 12
    name "COPS5"
  ]
  node
  [
    id 13
    name "CCNA2"
  ]
  node
  [
    id 14
    name "SKP1"
  ]
  node
  [
    id 15
    name "CDK2"
  ]
  node
  [
    id 16
    name "NEO1"
  ]
  node
  [
    id 17
    name "ABL1"
  ]
  node
  [
    id 18
    name "FBXW7"
  ]
  node
  [
    id 19
    name "TGFBR1"
  ]
  node
  [
    id 20
    name "CCT3"
  ]
  node
  [
    id 21
    name "ARID1B"
  ]
  node
  [
    id 22
    name "SREBF1"
  ]
  node
  [
    id 23
    name "CARM1"
  ]
  node
  [
    id 24
    name "XPO1"
  ]
  node
  [
    id 25
    name "BUB1B"
  ]
  node
  [
    id 26
    name "MAPK1"
  ]
  node
  [
    id 27
    name "RBX1"
  ]
  node
  [
    id 28
    name "FBN2"
  ]
  node
  [
    id 29
    name "RPS6KA1"
  ]
  node
  [
    id 30
    name "POLA1"
  ]
  node
  [
    id 31
    name "ANAPC4"
  ]
  node
  [
    id 32
    name "EIF5B"
  ]
  node
  [
    id 33
    name "RARS"
  ]
  node
  [
    id 34
    name "JAK2"
  ]
  node
  [
    id 35
    name "CDK4"
  ]
  node
  [
    id 36
    name "SMAD4"
  ]
  node
  [
    id 37
    name "KDM1A"
  ]
  node
  [
    id 38
    name "CSNK2A1"
  ]
  node
  [
    id 39
    name "UBE2I"
  ]
  node
  [
    id 40
    name "NOTCH1"
  ]
  node
  [
    id 41
    name "MEN1"
  ]
  node
  [
    id 42
    name "KATNAL1"
  ]
  node
  [
    id 43
    name "CDC27"
  ]
  node
  [
    id 44
    name "RBMX"
  ]
  node
  [
    id 45
    name "MYC"
  ]
  node
  [
    id 46
    name "RPS6KB1"
  ]
  node
  [
    id 47
    name "ARID1A"
  ]
  node
  [
    id 48
    name "TP53BP2"
  ]
  node
  [
    id 49
    name "PIN1"
  ]
  node
  [
    id 50
    name "MAX"
  ]
  node
  [
    id 51
    name "SAP18"
  ]
  node
  [
    id 52
    name "ORC1"
  ]
  node
  [
    id 53
    name "RFWD2"
  ]
  node
  [
    id 54
    name "RBM22"
  ]
  node
  [
    id 55
    name "BRCA1"
  ]
  node
  [
    id 56
    name "EPS15"
  ]
  node
  [
    id 57
    name "DKC1"
  ]
  node
  [
    id 58
    name "STAM2"
  ]
  node
  [
    id 59
    name "HDAC9"
  ]
  node
  [
    id 60
    name "SUPT16H"
  ]
  node
  [
    id 61
    name "RUNX2"
  ]
  node
  [
    id 62
    name "NCOR1"
  ]
  node
  [
    id 63
    name "MLH1"
  ]
  node
  [
    id 64
    name "PXN"
  ]
  node
  [
    id 65
    name "CTU1"
  ]
  node
  [
    id 66
    name "MET"
  ]
  node
  [
    id 67
    name "CDH1"
  ]
  node
  [
    id 68
    name "LARS"
  ]
  node
  [
    id 69
    name "SMG1"
  ]
  node
  [
    id 70
    name "RB1"
  ]
  node
  [
    id 71
    name "RPA2"
  ]
  node
  [
    id 72
    name "KIT"
  ]
  node
  [
    id 73
    name "STAT1"
  ]
  node
  [
    id 74
    name "ANAPC11"
  ]
  node
  [
    id 75
    name "BRAF"
  ]
  node
  [
    id 76
    name "CDKN1B"
  ]
  node
  [
    id 77
    name "UBE2D3"
  ]
  node
  [
    id 78
    name "UBE2D1"
  ]
  node
  [
    id 79
    name "SOS1"
  ]
  node
  [
    id 80
    name "CLNS1A"
  ]
  node
  [
    id 81
    name "DNAL4"
  ]
  node
  [
    id 82
    name "CHTF18"
  ]
  node
  [
    id 83
    name "SRSF5"
  ]
  node
  [
    id 84
    name "MAP3K1"
  ]
  node
  [
    id 85
    name "SSRP1"
  ]
  node
  [
    id 86
    name "PRKACA"
  ]
  node
  [
    id 87
    name "NVL"
  ]
  node
  [
    id 88
    name "IGF1R"
  ]
  node
  [
    id 89
    name "SMARCE1"
  ]
  node
  [
    id 90
    name "ETS1"
  ]
  node
  [
    id 91
    name "JUND"
  ]
  node
  [
    id 92
    name "ERBB2"
  ]
  node
  [
    id 93
    name "KIF3B"
  ]
  node
  [
    id 94
    name "ACTR6"
  ]
  node
  [
    id 95
    name "CHEK1"
  ]
  node
  [
    id 96
    name "SKP2"
  ]
  node
  [
    id 97
    name "SUV39H1"
  ]
  node
  [
    id 98
    name "USP10"
  ]
  node
  [
    id 99
    name "RAF1"
  ]
  node
  [
    id 100
    name "CDC7"
  ]
  node
  [
    id 101
    name "UBA2"
  ]
  node
  [
    id 102
    name "HOXB5"
  ]
  node
  [
    id 103
    name "RAD18"
  ]
  node
  [
    id 104
    name "ATM"
  ]
  node
  [
    id 105
    name "ZPR1"
  ]
  node
  [
    id 106
    name "NCL"
  ]
  node
  [
    id 107
    name "MCM4"
  ]
  node
  [
    id 108
    name "GINS4"
  ]
  node
  [
    id 109
    name "CDK6"
  ]
  node
  [
    id 110
    name "PBRM1"
  ]
  node
  [
    id 111
    name "CDC25A"
  ]
  node
  [
    id 112
    name "WEE1"
  ]
  node
  [
    id 113
    name "PLK1"
  ]
  node
  [
    id 114
    name "FEN1"
  ]
  node
  [
    id 115
    name "MAP2K1"
  ]
  node
  [
    id 116
    name "CDK1"
  ]
  node
  [
    id 117
    name "SMAD1"
  ]
  node
  [
    id 118
    name "JUNB"
  ]
  node
  [
    id 119
    name "PTEN"
  ]
  node
  [
    id 120
    name "SMARCC1"
  ]
  node
  [
    id 121
    name "CTBP2"
  ]
  node
  [
    id 122
    name "POLE"
  ]
  node
  [
    id 123
    name "TBL1XR1"
  ]
  node
  [
    id 124
    name "ERN1"
  ]
  node
  [
    id 125
    name "AURKB"
  ]
  node
  [
    id 126
    name "CALR"
  ]
  node
  [
    id 127
    name "CHEK2"
  ]
  node
  [
    id 128
    name "HGS"
  ]
  node
  [
    id 129
    name "FANCA"
  ]
  node
  [
    id 130
    name "CTNND1"
  ]
  node
  [
    id 131
    name "SUPT4H1"
  ]
  edge
  [
    source 110
    target 0
    weight 0.751877027552352
  ]
  edge
  [
    source 110
    target 1
    weight 0.785791390443547
  ]
  edge
  [
    source 110
    target 2
    weight 0.838155009280252
  ]
  edge
  [
    source 110
    target 3
    weight 0.876054798285786
  ]
  edge
  [
    source 110
    target 4
    weight 0.799973619868114
  ]
  edge
  [
    source 110
    target 5
    weight 0.794972931177036
  ]
  edge
  [
    source 110
    target 6
    weight 0.816468766858931
  ]
  edge
  [
    source 110
    target 7
    weight 0.821099369616181
  ]
  edge
  [
    source 110
    target 8
    weight 0.780644953315394
  ]
  edge
  [
    source 110
    target 9
    weight 0.775087092443098
  ]
  edge
  [
    source 110
    target 10
    weight 0.889180695090039
  ]
  edge
  [
    source 110
    target 11
    weight 0.788108297305578
  ]
  edge
  [
    source 110
    target 12
    weight 0.751400873630733
  ]
  edge
  [
    source 110
    target 13
    weight 0.844159792841044
  ]
  edge
  [
    source 110
    target 14
    weight 0.768814668843665
  ]
  edge
  [
    source 110
    target 15
    weight 0.875941052987991
  ]
  edge
  [
    source 110
    target 16
    weight 0.763095055650267
  ]
  edge
  [
    source 110
    target 17
    weight 0.825505243327026
  ]
  edge
  [
    source 110
    target 18
    weight 0.8322017597244
  ]
  edge
  [
    source 110
    target 19
    weight 0.831825784523272
  ]
  edge
  [
    source 110
    target 20
    weight 0.78276493001626
  ]
  edge
  [
    source 110
    target 21
    weight 0.877799583787734
  ]
  edge
  [
    source 110
    target 22
    weight 0.76331861480229
  ]
  edge
  [
    source 110
    target 23
    weight 0.868924640975409
  ]
  edge
  [
    source 110
    target 24
    weight 0.7816099122366
  ]
  edge
  [
    source 110
    target 25
    weight 0.854677338154915
  ]
  edge
  [
    source 110
    target 26
    weight 0.7633982050401
  ]
  edge
  [
    source 110
    target 27
    weight 0.793419367103992
  ]
  edge
  [
    source 110
    target 28
    weight 0.804205090965846
  ]
  edge
  [
    source 110
    target 29
    weight 0.878072460432809
  ]
  edge
  [
    source 110
    target 30
    weight 0.786216640499627
  ]
  edge
  [
    source 110
    target 31
    weight 0.815795823423143
  ]
  edge
  [
    source 110
    target 32
    weight 0.789769653531279
  ]
  edge
  [
    source 110
    target 33
    weight 0.832859406374526
  ]
  edge
  [
    source 110
    target 34
    weight 0.827954486993449
  ]
  edge
  [
    source 110
    target 35
    weight 0.79670086566986
  ]
  edge
  [
    source 110
    target 36
    weight 0.865651180351241
  ]
  edge
  [
    source 110
    target 37
    weight 0.844562852114049
  ]
  edge
  [
    source 110
    target 38
    weight 0.862566383723385
  ]
  edge
  [
    source 110
    target 39
    weight 0.884037783149482
  ]
  edge
  [
    source 110
    target 40
    weight 0.829502119160594
  ]
  edge
  [
    source 110
    target 41
    weight 0.768058653335555
  ]
  edge
  [
    source 110
    target 42
    weight 0.792116375674242
  ]
  edge
  [
    source 110
    target 43
    weight 0.765099626909904
  ]
  edge
  [
    source 110
    target 44
    weight 0.806130311002147
  ]
  edge
  [
    source 110
    target 45
    weight 0.804870787090483
  ]
  edge
  [
    source 110
    target 46
    weight 0.810039635659735
  ]
  edge
  [
    source 110
    target 47
    weight 0.754652709693877
  ]
  edge
  [
    source 110
    target 48
    weight 0.801567074652009
  ]
  edge
  [
    source 110
    target 49
    weight 0.87484388230243
  ]
  edge
  [
    source 110
    target 50
    weight 0.759142512980932
  ]
  edge
  [
    source 110
    target 51
    weight 0.763374737924527
  ]
  edge
  [
    source 110
    target 52
    weight 0.809568336330483
  ]
  edge
  [
    source 110
    target 53
    weight 0.807735542063498
  ]
  edge
  [
    source 110
    target 54
    weight 0.807155506826136
  ]
  edge
  [
    source 110
    target 55
    weight 0.780204470523237
  ]
  edge
  [
    source 110
    target 56
    weight 0.805590600749661
  ]
  edge
  [
    source 110
    target 57
    weight 0.764925305084567
  ]
  edge
  [
    source 110
    target 58
    weight 0.819934913472177
  ]
  edge
  [
    source 110
    target 59
    weight 0.760683429678064
  ]
  edge
  [
    source 110
    target 60
    weight 0.835335170512073
  ]
  edge
  [
    source 110
    target 61
    weight 0.840265713829657
  ]
  edge
  [
    source 110
    target 62
    weight 0.808580567535437
  ]
  edge
  [
    source 110
    target 63
    weight 0.752910790799316
  ]
  edge
  [
    source 110
    target 64
    weight 0.839438854157492
  ]
  edge
  [
    source 110
    target 65
    weight 0.793617461083885
  ]
  edge
  [
    source 110
    target 66
    weight 0.804460981428748
  ]
  edge
  [
    source 110
    target 67
    weight 0.82290955780558
  ]
  edge
  [
    source 110
    target 68
    weight 0.778073417879455
  ]
  edge
  [
    source 110
    target 69
    weight 0.791006937196709
  ]
  edge
  [
    source 110
    target 70
    weight 0.77645632544263
  ]
  edge
  [
    source 110
    target 71
    weight 0.847088605582418
  ]
  edge
  [
    source 110
    target 72
    weight 0.777804198653821
  ]
  edge
  [
    source 110
    target 73
    weight 0.855414629364451
  ]
  edge
  [
    source 110
    target 74
    weight 0.782831653669523
  ]
  edge
  [
    source 110
    target 75
    weight 0.77441444192406
  ]
  edge
  [
    source 110
    target 76
    weight 0.85718294946105
  ]
  edge
  [
    source 110
    target 77
    weight 0.815677535920211
  ]
  edge
  [
    source 110
    target 78
    weight 0.778559140543272
  ]
  edge
  [
    source 110
    target 79
    weight 0.764917959344563
  ]
  edge
  [
    source 110
    target 80
    weight 0.845400238550898
  ]
  edge
  [
    source 110
    target 81
    weight 0.80289540886998
  ]
  edge
  [
    source 110
    target 82
    weight 0.77525007054061
  ]
  edge
  [
    source 110
    target 83
    weight 0.750074380591349
  ]
  edge
  [
    source 110
    target 84
    weight 0.78441555407964
  ]
  edge
  [
    source 110
    target 85
    weight 0.836780249097928
  ]
  edge
  [
    source 110
    target 86
    weight 0.870563791663188
  ]
  edge
  [
    source 110
    target 87
    weight 0.764105642796772
  ]
  edge
  [
    source 110
    target 88
    weight 0.822318161390739
  ]
  edge
  [
    source 110
    target 89
    weight 0.788161583914609
  ]
  edge
  [
    source 110
    target 90
    weight 0.86242764333854
  ]
  edge
  [
    source 110
    target 91
    weight 0.804756955668435
  ]
  edge
  [
    source 110
    target 92
    weight 0.862802311946009
  ]
  edge
  [
    source 110
    target 93
    weight 0.813478100161621
  ]
  edge
  [
    source 110
    target 94
    weight 0.794026730524414
  ]
  edge
  [
    source 110
    target 95
    weight 0.8559993273072
  ]
  edge
  [
    source 110
    target 96
    weight 0.81068123824838
  ]
  edge
  [
    source 110
    target 97
    weight 0.868571327958261
  ]
  edge
  [
    source 110
    target 98
    weight 0.778811991276846
  ]
  edge
  [
    source 110
    target 99
    weight 0.847170015282296
  ]
  edge
  [
    source 110
    target 100
    weight 0.854682683875173
  ]
  edge
  [
    source 110
    target 101
    weight 0.817223767577601
  ]
  edge
  [
    source 110
    target 102
    weight 0.763327226129204
  ]
  edge
  [
    source 110
    target 103
    weight 0.783906010398885
  ]
  edge
  [
    source 110
    target 104
    weight 0.804966308523677
  ]
  edge
  [
    source 110
    target 105
    weight 0.774665304229858
  ]
  edge
  [
    source 110
    target 106
    weight 0.81929687448883
  ]
  edge
  [
    source 110
    target 107
    weight 0.764350053893959
  ]
  edge
  [
    source 110
    target 108
    weight 0.771804421428878
  ]
  edge
  [
    source 110
    target 109
    weight 0.891113002129849
  ]
  edge
  [
    source 111
    target 110
    weight 0.820909699036195
  ]
  edge
  [
    source 112
    target 110
    weight 0.854659950487378
  ]
  edge
  [
    source 113
    target 110
    weight 0.83587420768602
  ]
  edge
  [
    source 114
    target 110
    weight 0.836090049011262
  ]
  edge
  [
    source 115
    target 110
    weight 0.762263764828797
  ]
  edge
  [
    source 116
    target 110
    weight 0.765493271947385
  ]
  edge
  [
    source 117
    target 110
    weight 0.784649556185946
  ]
  edge
  [
    source 118
    target 110
    weight 0.856482338217159
  ]
  edge
  [
    source 119
    target 110
    weight 0.879678461465504
  ]
  edge
  [
    source 120
    target 110
    weight 0.860808380151052
  ]
  edge
  [
    source 121
    target 110
    weight 0.812138017987433
  ]
  edge
  [
    source 122
    target 110
    weight 0.780396349186312
  ]
  edge
  [
    source 123
    target 110
    weight 0.790057945220459
  ]
  edge
  [
    source 124
    target 110
    weight 0.767701911496462
  ]
  edge
  [
    source 125
    target 110
    weight 0.775149020015631
  ]
  edge
  [
    source 126
    target 110
    weight 0.801665705643565
  ]
  edge
  [
    source 127
    target 110
    weight 0.890667417969758
  ]
  edge
  [
    source 128
    target 110
    weight 0.794225615942801
  ]
  edge
  [
    source 129
    target 110
    weight 0.783634384040484
  ]
  edge
  [
    source 130
    target 110
    weight 0.847597085795538
  ]
  edge
  [
    source 131
    target 110
    weight 0.761265316570986
  ]
]
