Creator "igraph version @VERSION@ Thu Jun  8 15:11:20 2017"
Version 1
graph
[
  directed 0
  node
  [
    id 0
    name "VHL"
  ]
  node
  [
    id 1
    name "APC"
  ]
  node
  [
    id 2
    name "RCHY1"
  ]
  node
  [
    id 3
    name "CSNK2A1"
  ]
  node
  [
    id 4
    name "BRCA1"
  ]
  node
  [
    id 5
    name "PTEN"
  ]
  node
  [
    id 6
    name "MORF4L2"
  ]
  node
  [
    id 7
    name "XPO1"
  ]
  node
  [
    id 8
    name "CHAF1A"
  ]
  node
  [
    id 9
    name "ARRB2"
  ]
  node
  [
    id 10
    name "ETS1"
  ]
  node
  [
    id 11
    name "TPR"
  ]
  node
  [
    id 12
    name "KDM1A"
  ]
  node
  [
    id 13
    name "ZPR1"
  ]
  node
  [
    id 14
    name "MCM7"
  ]
  node
  [
    id 15
    name "BECN1"
  ]
  node
  [
    id 16
    name "CARM1"
  ]
  node
  [
    id 17
    name "ERBB2"
  ]
  node
  [
    id 18
    name "KMT2D"
  ]
  node
  [
    id 19
    name "UBE2I"
  ]
  node
  [
    id 20
    name "PPP2R5D"
  ]
  node
  [
    id 21
    name "SREBF1"
  ]
  node
  [
    id 22
    name "SUPT16H"
  ]
  node
  [
    id 23
    name "STAT1"
  ]
  node
  [
    id 24
    name "CDK2"
  ]
  node
  [
    id 25
    name "UBE2V2"
  ]
  node
  [
    id 26
    name "CCT7"
  ]
  node
  [
    id 27
    name "ORC1"
  ]
  node
  [
    id 28
    name "CDC73"
  ]
  node
  [
    id 29
    name "UBA1"
  ]
  node
  [
    id 30
    name "PXN"
  ]
  node
  [
    id 31
    name "RNF40"
  ]
  node
  [
    id 32
    name "CDC16"
  ]
  node
  [
    id 33
    name "UBE2D3"
  ]
  node
  [
    id 34
    name "FBXW7"
  ]
  node
  [
    id 35
    name "PDGFRA"
  ]
  node
  [
    id 36
    name "MEN1"
  ]
  node
  [
    id 37
    name "JUND"
  ]
  node
  [
    id 38
    name "COPS2"
  ]
  node
  [
    id 39
    name "SLC9A3R1"
  ]
  node
  [
    id 40
    name "MAP3K1"
  ]
  node
  [
    id 41
    name "ZRANB1"
  ]
  node
  [
    id 42
    name "DDB1"
  ]
  node
  [
    id 43
    name "RBM22"
  ]
  node
  [
    id 44
    name "GSK3B"
  ]
  node
  [
    id 45
    name "GAB2"
  ]
  node
  [
    id 46
    name "RAN"
  ]
  node
  [
    id 47
    name "CLNS1A"
  ]
  node
  [
    id 48
    name "PARP1"
  ]
  node
  [
    id 49
    name "UBE2B"
  ]
  node
  [
    id 50
    name "SNRNP70"
  ]
  node
  [
    id 51
    name "CDC7"
  ]
  node
  [
    id 52
    name "NCL"
  ]
  node
  [
    id 53
    name "CRY2"
  ]
  node
  [
    id 54
    name "HUWE1"
  ]
  node
  [
    id 55
    name "SMARCA2"
  ]
  node
  [
    id 56
    name "SNRPA"
  ]
  node
  [
    id 57
    name "BUB1B"
  ]
  node
  [
    id 58
    name "PRKACA"
  ]
  node
  [
    id 59
    name "CUL4B"
  ]
  node
  [
    id 60
    name "NFKB1"
  ]
  node
  [
    id 61
    name "SUV39H1"
  ]
  node
  [
    id 62
    name "PIN1"
  ]
  node
  [
    id 63
    name "POLR1A"
  ]
  node
  [
    id 64
    name "CCT2"
  ]
  node
  [
    id 65
    name "RAD52"
  ]
  node
  [
    id 66
    name "KIT"
  ]
  node
  [
    id 67
    name "EIF4A3"
  ]
  node
  [
    id 68
    name "RFWD2"
  ]
  node
  [
    id 69
    name "RAD18"
  ]
  node
  [
    id 70
    name "SMARCB1"
  ]
  node
  [
    id 71
    name "IQGAP1"
  ]
  node
  [
    id 72
    name "EIF3A"
  ]
  node
  [
    id 73
    name "EPAS1"
  ]
  node
  [
    id 74
    name "UTP20"
  ]
  node
  [
    id 75
    name "KARS"
  ]
  node
  [
    id 76
    name "ATM"
  ]
  node
  [
    id 77
    name "CDT1"
  ]
  node
  [
    id 78
    name "TOP2A"
  ]
  node
  [
    id 79
    name "TAF6"
  ]
  node
  [
    id 80
    name "CDH1"
  ]
  node
  [
    id 81
    name "RBX1"
  ]
  node
  [
    id 82
    name "CDC25A"
  ]
  node
  [
    id 83
    name "CREBBP"
  ]
  node
  [
    id 84
    name "TRIP12"
  ]
  node
  [
    id 85
    name "MAPK1"
  ]
  node
  [
    id 86
    name "CTNNA1"
  ]
  node
  [
    id 87
    name "FEN1"
  ]
  node
  [
    id 88
    name "RPA2"
  ]
  node
  [
    id 89
    name "PKMYT1"
  ]
  node
  [
    id 90
    name "SSRP1"
  ]
  node
  [
    id 91
    name "SNAI1"
  ]
  node
  [
    id 92
    name "POLA1"
  ]
  node
  [
    id 93
    name "CASP8"
  ]
  node
  [
    id 94
    name "KRAS"
  ]
  node
  [
    id 95
    name "NFE2L2"
  ]
  node
  [
    id 96
    name "EIF4A1"
  ]
  node
  [
    id 97
    name "HDAC9"
  ]
  node
  [
    id 98
    name "BIRC5"
  ]
  node
  [
    id 99
    name "RUNX2"
  ]
  node
  [
    id 100
    name "EIF3B"
  ]
  node
  [
    id 101
    name "IGF1R"
  ]
  node
  [
    id 102
    name "CDK4"
  ]
  node
  [
    id 103
    name "BIRC6"
  ]
  node
  [
    id 104
    name "ING4"
  ]
  node
  [
    id 105
    name "TGFBR1"
  ]
  node
  [
    id 106
    name "GNL3L"
  ]
  node
  [
    id 107
    name "PLK1"
  ]
  node
  [
    id 108
    name "UBA2"
  ]
  node
  [
    id 109
    name "BRAF"
  ]
  node
  [
    id 110
    name "SPTBN1"
  ]
  node
  [
    id 111
    name "FLNC"
  ]
  node
  [
    id 112
    name "MCM4"
  ]
  node
  [
    id 113
    name "PIK3CA"
  ]
  node
  [
    id 114
    name "POLR2J"
  ]
  node
  [
    id 115
    name "FBXW11"
  ]
  node
  [
    id 116
    name "RARS"
  ]
  node
  [
    id 117
    name "USP10"
  ]
  node
  [
    id 118
    name "EPS15"
  ]
  node
  [
    id 119
    name "PSEN1"
  ]
  node
  [
    id 120
    name "RPS6KB1"
  ]
  node
  [
    id 121
    name "NDC80"
  ]
  node
  [
    id 122
    name "CCT3"
  ]
  node
  [
    id 123
    name "MLH1"
  ]
  node
  [
    id 124
    name "CDKN1B"
  ]
  node
  [
    id 125
    name "NCK2"
  ]
  node
  [
    id 126
    name "RHOA"
  ]
  node
  [
    id 127
    name "CDK6"
  ]
  node
  [
    id 128
    name "MAX"
  ]
  node
  [
    id 129
    name "UBE4B"
  ]
  node
  [
    id 130
    name "MSH6"
  ]
  node
  [
    id 131
    name "EXOSC2"
  ]
  node
  [
    id 132
    name "RPS6KA1"
  ]
  node
  [
    id 133
    name "WWP1"
  ]
  node
  [
    id 134
    name "PRDX1"
  ]
  node
  [
    id 135
    name "SKP1"
  ]
  node
  [
    id 136
    name "UBE2D1"
  ]
  node
  [
    id 137
    name "SOS1"
  ]
  node
  [
    id 138
    name "MARK2"
  ]
  node
  [
    id 139
    name "ABL1"
  ]
  node
  [
    id 140
    name "PPP2CB"
  ]
  node
  [
    id 141
    name "CDC27"
  ]
  node
  [
    id 142
    name "PRPF19"
  ]
  node
  [
    id 143
    name "DYNLL1"
  ]
  node
  [
    id 144
    name "CNOT1"
  ]
  node
  [
    id 145
    name "JAK2"
  ]
  node
  [
    id 146
    name "RAF1"
  ]
  node
  [
    id 147
    name "CCNA2"
  ]
  node
  [
    id 148
    name "UBB"
  ]
  node
  [
    id 149
    name "FGFR2"
  ]
  node
  [
    id 150
    name "COPS5"
  ]
  node
  [
    id 151
    name "COPS3"
  ]
  node
  [
    id 152
    name "SMG1"
  ]
  node
  [
    id 153
    name "UBE2M"
  ]
  node
  [
    id 154
    name "PRKCD"
  ]
  node
  [
    id 155
    name "SMAD1"
  ]
  node
  [
    id 156
    name "PINK1"
  ]
  node
  [
    id 157
    name "HCK"
  ]
  node
  [
    id 158
    name "SKP2"
  ]
  node
  [
    id 159
    name "EEF1A1"
  ]
  node
  [
    id 160
    name "EIF5B"
  ]
  node
  [
    id 161
    name "SRSF1"
  ]
  node
  [
    id 162
    name "KPNB1"
  ]
  node
  [
    id 163
    name "NOTCH1"
  ]
  node
  [
    id 164
    name "TAF9"
  ]
  node
  [
    id 165
    name "LYN"
  ]
  node
  [
    id 166
    name "RPTOR"
  ]
  node
  [
    id 167
    name "SUPT4H1"
  ]
  node
  [
    id 168
    name "DCTN1"
  ]
  node
  [
    id 169
    name "AKT1"
  ]
  node
  [
    id 170
    name "MTOR"
  ]
  node
  [
    id 171
    name "CTNND1"
  ]
  node
  [
    id 172
    name "NUP62"
  ]
  node
  [
    id 173
    name "NCOR1"
  ]
  node
  [
    id 174
    name "SRC"
  ]
  node
  [
    id 175
    name "AP2A1"
  ]
  node
  [
    id 176
    name "AURKB"
  ]
  node
  [
    id 177
    name "PRKDC"
  ]
  node
  [
    id 178
    name "SUMO2"
  ]
  node
  [
    id 179
    name "PPP1CC"
  ]
  node
  [
    id 180
    name "SP1"
  ]
  node
  [
    id 181
    name "HGS"
  ]
  node
  [
    id 182
    name "SUMO3"
  ]
  node
  [
    id 183
    name "CHEK2"
  ]
  node
  [
    id 184
    name "CALR"
  ]
  node
  [
    id 185
    name "HDAC2"
  ]
  node
  [
    id 186
    name "GRB2"
  ]
  node
  [
    id 187
    name "RAD17"
  ]
  node
  [
    id 188
    name "JUN"
  ]
  node
  [
    id 189
    name "TRAF6"
  ]
  node
  [
    id 190
    name "TYMS"
  ]
  node
  [
    id 191
    name "SFN"
  ]
  node
  [
    id 192
    name "TOP1"
  ]
  node
  [
    id 193
    name "UBE2G2"
  ]
  node
  [
    id 194
    name "RELA"
  ]
  node
  [
    id 195
    name "SH3KBP1"
  ]
  node
  [
    id 196
    name "BCL2"
  ]
  node
  [
    id 197
    name "JUNB"
  ]
  node
  [
    id 198
    name "CDK1"
  ]
  node
  [
    id 199
    name "YWHAB"
  ]
  node
  [
    id 200
    name "YWHAZ"
  ]
  node
  [
    id 201
    name "JAK1"
  ]
  node
  [
    id 202
    name "MYC"
  ]
  node
  [
    id 203
    name "ANAPC11"
  ]
  node
  [
    id 204
    name "PRKCI"
  ]
  node
  [
    id 205
    name "EXOSC10"
  ]
  node
  [
    id 206
    name "AMFR"
  ]
  node
  [
    id 207
    name "HDAC1"
  ]
  node
  [
    id 208
    name "TCEB1"
  ]
  node
  [
    id 209
    name "FANCD2"
  ]
  node
  [
    id 210
    name "SAP18"
  ]
  node
  [
    id 211
    name "HRAS"
  ]
  node
  [
    id 212
    name "CHEK1"
  ]
  node
  [
    id 213
    name "PARD3"
  ]
  node
  [
    id 214
    name "BLM"
  ]
  node
  [
    id 215
    name "TAF1"
  ]
  node
  [
    id 216
    name "MSN"
  ]
  node
  [
    id 217
    name "NPLOC4"
  ]
  node
  [
    id 218
    name "EGFR"
  ]
  node
  [
    id 219
    name "G3BP1"
  ]
  node
  [
    id 220
    name "PSEN2"
  ]
  node
  [
    id 221
    name "SMAD4"
  ]
  node
  [
    id 222
    name "RB1"
  ]
  node
  [
    id 223
    name "RAB1A"
  ]
  node
  [
    id 224
    name "IST1"
  ]
  node
  [
    id 225
    name "MAP2K1"
  ]
  node
  [
    id 226
    name "EPOR"
  ]
  node
  [
    id 227
    name "HSP90AA1"
  ]
  node
  [
    id 228
    name "ITGB1"
  ]
  node
  [
    id 229
    name "CKS1B"
  ]
  node
  [
    id 230
    name "SHC1"
  ]
  node
  [
    id 231
    name "ERN1"
  ]
  node
  [
    id 232
    name "FANCA"
  ]
  node
  [
    id 233
    name "SMARCE1"
  ]
  edge
  [
    source 164
    target 0
    weight 0.775292824043176
  ]
  edge
  [
    source 165
    target 0
    weight 0.770908962020241
  ]
  edge
  [
    source 165
    target 1
    weight 0.799225894161889
  ]
  edge
  [
    source 166
    target 1
    weight 0.783888598579737
  ]
  edge
  [
    source 5
    target 2
    weight 0.750139225949473
  ]
  edge
  [
    source 160
    target 1
    weight 0.768818640971582
  ]
  edge
  [
    source 167
    target 0
    weight 0.769415938740268
  ]
  edge
  [
    source 168
    target 1
    weight 0.795037856974319
  ]
  edge
  [
    source 169
    target 0
    weight 0.797554781458083
  ]
  edge
  [
    source 3
    target 0
    weight 0.782330231239545
  ]
  edge
  [
    source 30
    target 4
    weight 0.753712343624236
  ]
  edge
  [
    source 170
    target 0
    weight 0.767682761619916
  ]
  edge
  [
    source 23
    target 4
    weight 0.769418428464534
  ]
  edge
  [
    source 171
    target 5
    weight 0.806458647203215
  ]
  edge
  [
    source 6
    target 1
    weight 0.785137909599239
  ]
  edge
  [
    source 7
    target 5
    weight 0.781112031728685
  ]
  edge
  [
    source 172
    target 5
    weight 0.775332332317878
  ]
  edge
  [
    source 8
    target 5
    weight 0.752646025736448
  ]
  edge
  [
    source 9
    target 5
    weight 0.804692534628124
  ]
  edge
  [
    source 10
    target 1
    weight 0.783861584569784
  ]
  edge
  [
    source 11
    target 1
    weight 0.771022073920484
  ]
  edge
  [
    source 173
    target 4
    weight 0.753731628311818
  ]
  edge
  [
    source 174
    target 4
    weight 0.792185624719664
  ]
  edge
  [
    source 12
    target 5
    weight 0.772869964319425
  ]
  edge
  [
    source 42
    target 1
    weight 0.777272924530464
  ]
  edge
  [
    source 175
    target 5
    weight 0.764327643949319
  ]
  edge
  [
    source 13
    target 0
    weight 0.768707916492097
  ]
  edge
  [
    source 14
    target 5
    weight 0.792254750170868
  ]
  edge
  [
    source 176
    target 5
    weight 0.778375768592877
  ]
  edge
  [
    source 177
    target 1
    weight 0.777857851575212
  ]
  edge
  [
    source 170
    target 1
    weight 0.790170955019452
  ]
  edge
  [
    source 15
    target 1
    weight 0.758078789954474
  ]
  edge
  [
    source 178
    target 0
    weight 0.761119954630672
  ]
  edge
  [
    source 16
    target 5
    weight 0.764013434852965
  ]
  edge
  [
    source 19
    target 4
    weight 0.792868683954879
  ]
  edge
  [
    source 179
    target 0
    weight 0.764437497962311
  ]
  edge
  [
    source 180
    target 4
    weight 0.788555721214396
  ]
  edge
  [
    source 180
    target 0
    weight 0.793604167999066
  ]
  edge
  [
    source 181
    target 5
    weight 0.77158137273944
  ]
  edge
  [
    source 17
    target 5
    weight 0.783967643615432
  ]
  edge
  [
    source 18
    target 5
    weight 0.751499801138122
  ]
  edge
  [
    source 182
    target 5
    weight 0.799134230523779
  ]
  edge
  [
    source 182
    target 4
    weight 0.7522231722933
  ]
  edge
  [
    source 19
    target 1
    weight 0.785533833716167
  ]
  edge
  [
    source 10
    target 0
    weight 0.794186726617469
  ]
  edge
  [
    source 183
    target 1
    weight 0.78357491218639
  ]
  edge
  [
    source 183
    target 5
    weight 0.808017585548408
  ]
  edge
  [
    source 183
    target 0
    weight 0.778455918843864
  ]
  edge
  [
    source 20
    target 1
    weight 0.755451993427637
  ]
  edge
  [
    source 21
    target 0
    weight 0.810066583690664
  ]
  edge
  [
    source 22
    target 1
    weight 0.770241253649802
  ]
  edge
  [
    source 23
    target 5
    weight 0.758137791939928
  ]
  edge
  [
    source 184
    target 5
    weight 0.772178645077142
  ]
  edge
  [
    source 184
    target 0
    weight 0.765353105878374
  ]
  edge
  [
    source 24
    target 1
    weight 0.765142345849217
  ]
  edge
  [
    source 176
    target 1
    weight 0.805347415020718
  ]
  edge
  [
    source 176
    target 0
    weight 0.78467627674173
  ]
  edge
  [
    source 25
    target 5
    weight 0.798069356824824
  ]
  edge
  [
    source 85
    target 4
    weight 0.792170822640793
  ]
  edge
  [
    source 185
    target 1
    weight 0.776922798999214
  ]
  edge
  [
    source 186
    target 4
    weight 0.771324615422948
  ]
  edge
  [
    source 187
    target 1
    weight 0.771503424261687
  ]
  edge
  [
    source 188
    target 4
    weight 0.793850462159284
  ]
  edge
  [
    source 107
    target 4
    weight 0.751508745738457
  ]
  edge
  [
    source 189
    target 4
    weight 0.791026302475472
  ]
  edge
  [
    source 190
    target 1
    weight 0.778012485589175
  ]
  edge
  [
    source 191
    target 0
    weight 0.772315431965918
  ]
  edge
  [
    source 191
    target 1
    weight 0.794473831824362
  ]
  edge
  [
    source 189
    target 1
    weight 0.785178204845471
  ]
  edge
  [
    source 192
    target 5
    weight 0.786048351231776
  ]
  edge
  [
    source 26
    target 5
    weight 0.794002550483763
  ]
  edge
  [
    source 180
    target 1
    weight 0.773429396388594
  ]
  edge
  [
    source 27
    target 5
    weight 0.80472496962313
  ]
  edge
  [
    source 101
    target 1
    weight 0.764605506378901
  ]
  edge
  [
    source 28
    target 1
    weight 0.783999254339646
  ]
  edge
  [
    source 193
    target 0
    weight 0.767777969489091
  ]
  edge
  [
    source 29
    target 5
    weight 0.802127872356423
  ]
  edge
  [
    source 194
    target 0
    weight 0.799493264579151
  ]
  edge
  [
    source 7
    target 1
    weight 0.799593157064027
  ]
  edge
  [
    source 150
    target 4
    weight 0.779852085857306
  ]
  edge
  [
    source 30
    target 5
    weight 0.778805304079174
  ]
  edge
  [
    source 31
    target 5
    weight 0.77958820946787
  ]
  edge
  [
    source 32
    target 5
    weight 0.79627602601264
  ]
  edge
  [
    source 33
    target 5
    weight 0.793722396005339
  ]
  edge
  [
    source 34
    target 5
    weight 0.788773372396748
  ]
  edge
  [
    source 35
    target 5
    weight 0.758428828912717
  ]
  edge
  [
    source 36
    target 5
    weight 0.759498455485327
  ]
  edge
  [
    source 13
    target 5
    weight 0.778498512125296
  ]
  edge
  [
    source 37
    target 5
    weight 0.768830613954493
  ]
  edge
  [
    source 38
    target 5
    weight 0.773982710251287
  ]
  edge
  [
    source 39
    target 5
    weight 0.76707861089023
  ]
  edge
  [
    source 40
    target 5
    weight 0.805219708812241
  ]
  edge
  [
    source 41
    target 5
    weight 0.768228697054967
  ]
  edge
  [
    source 42
    target 5
    weight 0.771742612815154
  ]
  edge
  [
    source 43
    target 0
    weight 0.763210234296145
  ]
  edge
  [
    source 44
    target 5
    weight 0.764513413752897
  ]
  edge
  [
    source 45
    target 5
    weight 0.787431002867512
  ]
  edge
  [
    source 46
    target 5
    weight 0.751288971473709
  ]
  edge
  [
    source 47
    target 5
    weight 0.777217858245392
  ]
  edge
  [
    source 48
    target 5
    weight 0.78344226646606
  ]
  edge
  [
    source 49
    target 5
    weight 0.787884391999945
  ]
  edge
  [
    source 50
    target 5
    weight 0.772646549196948
  ]
  edge
  [
    source 51
    target 5
    weight 0.789740123796713
  ]
  edge
  [
    source 52
    target 5
    weight 0.784279024298231
  ]
  edge
  [
    source 53
    target 5
    weight 0.766681457472194
  ]
  edge
  [
    source 54
    target 5
    weight 0.769843878998902
  ]
  edge
  [
    source 55
    target 5
    weight 0.775539339276798
  ]
  edge
  [
    source 56
    target 5
    weight 0.771264266813113
  ]
  edge
  [
    source 57
    target 5
    weight 0.803596615294011
  ]
  edge
  [
    source 58
    target 5
    weight 0.791146571734502
  ]
  edge
  [
    source 59
    target 5
    weight 0.763917283086039
  ]
  edge
  [
    source 60
    target 5
    weight 0.766897311357949
  ]
  edge
  [
    source 61
    target 5
    weight 0.768376370402299
  ]
  edge
  [
    source 62
    target 5
    weight 0.805318639014206
  ]
  edge
  [
    source 63
    target 5
    weight 0.769087019441955
  ]
  edge
  [
    source 64
    target 5
    weight 0.772220950900255
  ]
  edge
  [
    source 65
    target 5
    weight 0.750172799193399
  ]
  edge
  [
    source 66
    target 5
    weight 0.775644133113743
  ]
  edge
  [
    source 67
    target 5
    weight 0.779455360362571
  ]
  edge
  [
    source 195
    target 1
    weight 0.785343896094333
  ]
  edge
  [
    source 68
    target 5
    weight 0.778689770659518
  ]
  edge
  [
    source 69
    target 5
    weight 0.799142788590088
  ]
  edge
  [
    source 70
    target 5
    weight 0.791238969611815
  ]
  edge
  [
    source 71
    target 5
    weight 0.755009988918372
  ]
  edge
  [
    source 72
    target 5
    weight 0.795779535988036
  ]
  edge
  [
    source 73
    target 5
    weight 0.754255312382951
  ]
  edge
  [
    source 74
    target 5
    weight 0.757524780174363
  ]
  edge
  [
    source 75
    target 5
    weight 0.79423454928083
  ]
  edge
  [
    source 76
    target 5
    weight 0.768690585081927
  ]
  edge
  [
    source 77
    target 5
    weight 0.775125985791239
  ]
  edge
  [
    source 78
    target 5
    weight 0.794069323009186
  ]
  edge
  [
    source 163
    target 0
    weight 0.803412785416904
  ]
  edge
  [
    source 196
    target 1
    weight 0.795647287649967
  ]
  edge
  [
    source 9
    target 1
    weight 0.769778652524062
  ]
  edge
  [
    source 197
    target 0
    weight 0.798888382075709
  ]
  edge
  [
    source 188
    target 1
    weight 0.791339615176076
  ]
  edge
  [
    source 79
    target 5
    weight 0.75463990499015
  ]
  edge
  [
    source 155
    target 4
    weight 0.761275503219605
  ]
  edge
  [
    source 80
    target 5
    weight 0.790048131515346
  ]
  edge
  [
    source 148
    target 4
    weight 0.775244036211735
  ]
  edge
  [
    source 198
    target 4
    weight 0.787400194666807
  ]
  edge
  [
    source 198
    target 1
    weight 0.799044913361769
  ]
  edge
  [
    source 81
    target 0
    weight 0.808448940512254
  ]
  edge
  [
    source 165
    target 4
    weight 0.753653646099534
  ]
  edge
  [
    source 25
    target 1
    weight 0.758529746055571
  ]
  edge
  [
    source 50
    target 1
    weight 0.777102056239946
  ]
  edge
  [
    source 177
    target 4
    weight 0.775469718147309
  ]
  edge
  [
    source 68
    target 0
    weight 0.784842693477491
  ]
  edge
  [
    source 87
    target 1
    weight 0.78084018082079
  ]
  edge
  [
    source 181
    target 1
    weight 0.786327763270014
  ]
  edge
  [
    source 182
    target 1
    weight 0.780170714582021
  ]
  edge
  [
    source 82
    target 5
    weight 0.795726758520432
  ]
  edge
  [
    source 83
    target 4
    weight 0.789615010237698
  ]
  edge
  [
    source 84
    target 5
    weight 0.766006874880109
  ]
  edge
  [
    source 22
    target 5
    weight 0.75476478670296
  ]
  edge
  [
    source 77
    target 4
    weight 0.754903344440177
  ]
  edge
  [
    source 77
    target 0
    weight 0.754241772442119
  ]
  edge
  [
    source 5
    target 1
    weight 0.783457076623269
  ]
  edge
  [
    source 33
    target 4
    weight 0.765979858873471
  ]
  edge
  [
    source 155
    target 0
    weight 0.794731844470687
  ]
  edge
  [
    source 85
    target 0
    weight 0.788880060135363
  ]
  edge
  [
    source 86
    target 1
    weight 0.798700440889951
  ]
  edge
  [
    source 87
    target 5
    weight 0.781427869707494
  ]
  edge
  [
    source 155
    target 1
    weight 0.789180116132156
  ]
  edge
  [
    source 199
    target 4
    weight 0.761030403841625
  ]
  edge
  [
    source 14
    target 1
    weight 0.777171013759566
  ]
  edge
  [
    source 88
    target 5
    weight 0.800122192373051
  ]
  edge
  [
    source 129
    target 4
    weight 0.758248758747877
  ]
  edge
  [
    source 38
    target 1
    weight 0.752268424789855
  ]
  edge
  [
    source 89
    target 5
    weight 0.767948112821268
  ]
  edge
  [
    source 200
    target 4
    weight 0.793014603117091
  ]
  edge
  [
    source 90
    target 5
    weight 0.760503800906573
  ]
  edge
  [
    source 192
    target 1
    weight 0.766761149368445
  ]
  edge
  [
    source 91
    target 1
    weight 0.765776388781527
  ]
  edge
  [
    source 200
    target 0
    weight 0.80286833888017
  ]
  edge
  [
    source 201
    target 1
    weight 0.783836731398043
  ]
  edge
  [
    source 200
    target 1
    weight 0.795548021280384
  ]
  edge
  [
    source 92
    target 5
    weight 0.788753927327541
  ]
  edge
  [
    source 40
    target 1
    weight 0.78819996473242
  ]
  edge
  [
    source 196
    target 0
    weight 0.795574314519893
  ]
  edge
  [
    source 202
    target 4
    weight 0.795280711747832
  ]
  edge
  [
    source 25
    target 0
    weight 0.772697460122413
  ]
  edge
  [
    source 93
    target 5
    weight 0.772686319810385
  ]
  edge
  [
    source 82
    target 1
    weight 0.784212323741843
  ]
  edge
  [
    source 42
    target 4
    weight 0.752903400210507
  ]
  edge
  [
    source 154
    target 1
    weight 0.784713859839557
  ]
  edge
  [
    source 94
    target 1
    weight 0.783753824936377
  ]
  edge
  [
    source 203
    target 0
    weight 0.780153628492296
  ]
  edge
  [
    source 204
    target 1
    weight 0.796548773313331
  ]
  edge
  [
    source 95
    target 5
    weight 0.799073524828969
  ]
  edge
  [
    source 46
    target 0
    weight 0.765055708598528
  ]
  edge
  [
    source 205
    target 1
    weight 0.752491841366733
  ]
  edge
  [
    source 96
    target 5
    weight 0.767984723860247
  ]
  edge
  [
    source 191
    target 5
    weight 0.808599163877978
  ]
  edge
  [
    source 78
    target 0
    weight 0.793636225215667
  ]
  edge
  [
    source 175
    target 1
    weight 0.750968674182132
  ]
  edge
  [
    source 97
    target 5
    weight 0.787192672937011
  ]
  edge
  [
    source 206
    target 0
    weight 0.765511490115662
  ]
  edge
  [
    source 94
    target 5
    weight 0.753262141952228
  ]
  edge
  [
    source 178
    target 4
    weight 0.789273851670038
  ]
  edge
  [
    source 179
    target 1
    weight 0.78759958512664
  ]
  edge
  [
    source 109
    target 4
    weight 0.751997716380408
  ]
  edge
  [
    source 98
    target 5
    weight 0.754380368254161
  ]
  edge
  [
    source 66
    target 1
    weight 0.772468287116829
  ]
  edge
  [
    source 21
    target 5
    weight 0.792998585984985
  ]
  edge
  [
    source 27
    target 0
    weight 0.753690865070617
  ]
  edge
  [
    source 11
    target 0
    weight 0.78002576616354
  ]
  edge
  [
    source 207
    target 4
    weight 0.783101548127216
  ]
  edge
  [
    source 99
    target 0
    weight 0.791314520740008
  ]
  edge
  [
    source 107
    target 0
    weight 0.783583200507256
  ]
  edge
  [
    source 198
    target 0
    weight 0.803403597742581
  ]
  edge
  [
    source 34
    target 0
    weight 0.806462053640058
  ]
  edge
  [
    source 208
    target 0
    weight 0.782299212675803
  ]
  edge
  [
    source 100
    target 5
    weight 0.781409007286186
  ]
  edge
  [
    source 101
    target 5
    weight 0.782499636335718
  ]
  edge
  [
    source 152
    target 1
    weight 0.778678451400795
  ]
  edge
  [
    source 55
    target 4
    weight 0.75916019914327
  ]
  edge
  [
    source 51
    target 1
    weight 0.776486538844747
  ]
  edge
  [
    source 209
    target 1
    weight 0.77532353601568
  ]
  edge
  [
    source 102
    target 5
    weight 0.798105068167021
  ]
  edge
  [
    source 103
    target 5
    weight 0.751544641060147
  ]
  edge
  [
    source 210
    target 1
    weight 0.768995195387897
  ]
  edge
  [
    source 196
    target 4
    weight 0.775618837983066
  ]
  edge
  [
    source 77
    target 1
    weight 0.776965430280212
  ]
  edge
  [
    source 55
    target 1
    weight 0.79064684993713
  ]
  edge
  [
    source 104
    target 1
    weight 0.755145727868769
  ]
  edge
  [
    source 211
    target 0
    weight 0.760877585543282
  ]
  edge
  [
    source 171
    target 0
    weight 0.768762300200748
  ]
  edge
  [
    source 212
    target 1
    weight 0.778571649517564
  ]
  edge
  [
    source 76
    target 1
    weight 0.796690600567689
  ]
  edge
  [
    source 136
    target 4
    weight 0.794686949373271
  ]
  edge
  [
    source 90
    target 1
    weight 0.78339105329502
  ]
  edge
  [
    source 213
    target 1
    weight 0.799626452871974
  ]
  edge
  [
    source 105
    target 1
    weight 0.794357716024948
  ]
  edge
  [
    source 202
    target 0
    weight 0.79683130241918
  ]
  edge
  [
    source 106
    target 5
    weight 0.785616838086018
  ]
  edge
  [
    source 204
    target 0
    weight 0.768227596057598
  ]
  edge
  [
    source 13
    target 1
    weight 0.772771768300439
  ]
  edge
  [
    source 214
    target 1
    weight 0.797325841295861
  ]
  edge
  [
    source 215
    target 1
    weight 0.7774773405997
  ]
  edge
  [
    source 216
    target 1
    weight 0.757235558922131
  ]
  edge
  [
    source 107
    target 5
    weight 0.782136283427579
  ]
  edge
  [
    source 217
    target 5
    weight 0.756028845367148
  ]
  edge
  [
    source 218
    target 4
    weight 0.792604398837978
  ]
  edge
  [
    source 218
    target 0
    weight 0.783262522010197
  ]
  edge
  [
    source 95
    target 4
    weight 0.752149578979762
  ]
  edge
  [
    source 219
    target 1
    weight 0.751512328905359
  ]
  edge
  [
    source 68
    target 1
    weight 0.800183608712973
  ]
  edge
  [
    source 158
    target 0
    weight 0.789609474546148
  ]
  edge
  [
    source 158
    target 1
    weight 0.783593050037425
  ]
  edge
  [
    source 147
    target 0
    weight 0.785025034975561
  ]
  edge
  [
    source 39
    target 1
    weight 0.765989592824564
  ]
  edge
  [
    source 217
    target 1
    weight 0.771160853238366
  ]
  edge
  [
    source 108
    target 0
    weight 0.772627109922954
  ]
  edge
  [
    source 109
    target 5
    weight 0.807264816524362
  ]
  edge
  [
    source 43
    target 1
    weight 0.769864223824605
  ]
  edge
  [
    source 58
    target 1
    weight 0.798174604939797
  ]
  edge
  [
    source 220
    target 0
    weight 0.761216688262218
  ]
  edge
  [
    source 220
    target 1
    weight 0.7813360181911
  ]
  edge
  [
    source 48
    target 0
    weight 0.799607299914454
  ]
  edge
  [
    source 110
    target 1
    weight 0.774287473916183
  ]
  edge
  [
    source 111
    target 1
    weight 0.750045825313262
  ]
  edge
  [
    source 112
    target 0
    weight 0.782620537159855
  ]
  edge
  [
    source 108
    target 5
    weight 0.752347599141643
  ]
  edge
  [
    source 113
    target 1
    weight 0.750039591357291
  ]
  edge
  [
    source 12
    target 0
    weight 0.758738647047991
  ]
  edge
  [
    source 114
    target 1
    weight 0.757069140575075
  ]
  edge
  [
    source 169
    target 1
    weight 0.761191509082542
  ]
  edge
  [
    source 221
    target 1
    weight 0.79783989609095
  ]
  edge
  [
    source 221
    target 4
    weight 0.770642090180365
  ]
  edge
  [
    source 28
    target 0
    weight 0.769400701045174
  ]
  edge
  [
    source 115
    target 0
    weight 0.796157139603813
  ]
  edge
  [
    source 9
    target 4
    weight 0.752073120519984
  ]
  edge
  [
    source 9
    target 0
    weight 0.790440955010243
  ]
  edge
  [
    source 211
    target 1
    weight 0.760798410976301
  ]
  edge
  [
    source 174
    target 1
    weight 0.757298424941782
  ]
  edge
  [
    source 116
    target 5
    weight 0.768650224782473
  ]
  edge
  [
    source 117
    target 1
    weight 0.796160926016894
  ]
  edge
  [
    source 173
    target 1
    weight 0.768136203534944
  ]
  edge
  [
    source 189
    target 0
    weight 0.784693284185468
  ]
  edge
  [
    source 10
    target 5
    weight 0.775976256539144
  ]
  edge
  [
    source 184
    target 1
    weight 0.775986611926512
  ]
  edge
  [
    source 188
    target 0
    weight 0.799252774933055
  ]
  edge
  [
    source 75
    target 1
    weight 0.7825737281915
  ]
  edge
  [
    source 222
    target 0
    weight 0.792640624514078
  ]
  edge
  [
    source 43
    target 5
    weight 0.755942673690992
  ]
  edge
  [
    source 49
    target 0
    weight 0.794663826797014
  ]
  edge
  [
    source 99
    target 5
    weight 0.785278559953104
  ]
  edge
  [
    source 173
    target 0
    weight 0.775946014723986
  ]
  edge
  [
    source 118
    target 5
    weight 0.757605265810079
  ]
  edge
  [
    source 119
    target 1
    weight 0.771419566146884
  ]
  edge
  [
    source 223
    target 1
    weight 0.759253274344151
  ]
  edge
  [
    source 49
    target 1
    weight 0.801400609360989
  ]
  edge
  [
    source 117
    target 4
    weight 0.761139117787821
  ]
  edge
  [
    source 202
    target 1
    weight 0.790895575580469
  ]
  edge
  [
    source 24
    target 0
    weight 0.789687186753595
  ]
  edge
  [
    source 120
    target 1
    weight 0.77018382472083
  ]
  edge
  [
    source 121
    target 0
    weight 0.762616661458423
  ]
  edge
  [
    source 161
    target 1
    weight 0.774535854918005
  ]
  edge
  [
    source 122
    target 5
    weight 0.794640273590376
  ]
  edge
  [
    source 94
    target 0
    weight 0.775900268606558
  ]
  edge
  [
    source 123
    target 1
    weight 0.766917406659387
  ]
  edge
  [
    source 124
    target 1
    weight 0.796546739008338
  ]
  edge
  [
    source 125
    target 1
    weight 0.755170013519202
  ]
  edge
  [
    source 61
    target 1
    weight 0.769201208027079
  ]
  edge
  [
    source 126
    target 1
    weight 0.770483171650987
  ]
  edge
  [
    source 85
    target 1
    weight 0.766148435828922
  ]
  edge
  [
    source 127
    target 1
    weight 0.795363542098044
  ]
  edge
  [
    source 27
    target 1
    weight 0.792624204030505
  ]
  edge
  [
    source 128
    target 1
    weight 0.753647537219702
  ]
  edge
  [
    source 129
    target 1
    weight 0.751431716843146
  ]
  edge
  [
    source 112
    target 1
    weight 0.771158975747259
  ]
  edge
  [
    source 130
    target 1
    weight 0.757671748002589
  ]
  edge
  [
    source 72
    target 1
    weight 0.781262105209999
  ]
  edge
  [
    source 131
    target 1
    weight 0.755960131066394
  ]
  edge
  [
    source 44
    target 1
    weight 0.796654288763042
  ]
  edge
  [
    source 132
    target 1
    weight 0.786543250421646
  ]
  edge
  [
    source 63
    target 1
    weight 0.776661761554052
  ]
  edge
  [
    source 133
    target 1
    weight 0.788807149896516
  ]
  edge
  [
    source 134
    target 1
    weight 0.774888685489529
  ]
  edge
  [
    source 135
    target 1
    weight 0.778172391845039
  ]
  edge
  [
    source 62
    target 1
    weight 0.789527457026685
  ]
  edge
  [
    source 46
    target 1
    weight 0.758109832064032
  ]
  edge
  [
    source 32
    target 1
    weight 0.796618352712435
  ]
  edge
  [
    source 36
    target 1
    weight 0.773266996076531
  ]
  edge
  [
    source 74
    target 1
    weight 0.75798897142995
  ]
  edge
  [
    source 56
    target 1
    weight 0.757345441459823
  ]
  edge
  [
    source 136
    target 1
    weight 0.77841529319239
  ]
  edge
  [
    source 137
    target 1
    weight 0.761223107182224
  ]
  edge
  [
    source 138
    target 1
    weight 0.752290700568555
  ]
  edge
  [
    source 73
    target 1
    weight 0.76711084797706
  ]
  edge
  [
    source 139
    target 1
    weight 0.754575490048367
  ]
  edge
  [
    source 140
    target 0
    weight 0.768180676465629
  ]
  edge
  [
    source 30
    target 1
    weight 0.796217496465595
  ]
  edge
  [
    source 224
    target 1
    weight 0.774205419649303
  ]
  edge
  [
    source 221
    target 0
    weight 0.777426666149534
  ]
  edge
  [
    source 141
    target 1
    weight 0.803407319439336
  ]
  edge
  [
    source 115
    target 5
    weight 0.773978147430333
  ]
  edge
  [
    source 124
    target 4
    weight 0.769199171963056
  ]
  edge
  [
    source 225
    target 1
    weight 0.803263439578786
  ]
  edge
  [
    source 158
    target 4
    weight 0.760940040536105
  ]
  edge
  [
    source 226
    target 5
    weight 0.784360910704977
  ]
  edge
  [
    source 36
    target 0
    weight 0.756754374895315
  ]
  edge
  [
    source 4
    target 0
    weight 0.766023694737936
  ]
  edge
  [
    source 37
    target 0
    weight 0.772628810537008
  ]
  edge
  [
    source 142
    target 0
    weight 0.763193991375276
  ]
  edge
  [
    source 44
    target 0
    weight 0.805455055584723
  ]
  edge
  [
    source 55
    target 0
    weight 0.802920253778576
  ]
  edge
  [
    source 69
    target 0
    weight 0.781322170204878
  ]
  edge
  [
    source 50
    target 0
    weight 0.752557971610102
  ]
  edge
  [
    source 133
    target 0
    weight 0.760754538912606
  ]
  edge
  [
    source 51
    target 0
    weight 0.764917946281566
  ]
  edge
  [
    source 141
    target 0
    weight 0.780353459843647
  ]
  edge
  [
    source 143
    target 0
    weight 0.775305854878494
  ]
  edge
  [
    source 19
    target 0
    weight 0.798865172379138
  ]
  edge
  [
    source 106
    target 0
    weight 0.763018682520367
  ]
  edge
  [
    source 60
    target 0
    weight 0.793919987436497
  ]
  edge
  [
    source 15
    target 0
    weight 0.761402376214904
  ]
  edge
  [
    source 47
    target 0
    weight 0.768380368999505
  ]
  edge
  [
    source 144
    target 1
    weight 0.75224820102761
  ]
  edge
  [
    source 227
    target 4
    weight 0.755303339937047
  ]
  edge
  [
    source 145
    target 1
    weight 0.774367528142699
  ]
  edge
  [
    source 140
    target 5
    weight 0.791251018012574
  ]
  edge
  [
    source 146
    target 1
    weight 0.761397210449279
  ]
  edge
  [
    source 147
    target 5
    weight 0.79524344935884
  ]
  edge
  [
    source 3
    target 1
    weight 0.790671035744152
  ]
  edge
  [
    source 135
    target 0
    weight 0.786863684526944
  ]
  edge
  [
    source 148
    target 5
    weight 0.754526966056098
  ]
  edge
  [
    source 24
    target 4
    weight 0.793086667409332
  ]
  edge
  [
    source 140
    target 1
    weight 0.788475210037363
  ]
  edge
  [
    source 41
    target 1
    weight 0.797429227154434
  ]
  edge
  [
    source 87
    target 4
    weight 0.752238064281825
  ]
  edge
  [
    source 60
    target 1
    weight 0.792116244951125
  ]
  edge
  [
    source 149
    target 1
    weight 0.755405984530292
  ]
  edge
  [
    source 150
    target 0
    weight 0.761101051753511
  ]
  edge
  [
    source 151
    target 5
    weight 0.767126451944003
  ]
  edge
  [
    source 88
    target 0
    weight 0.775554197390032
  ]
  edge
  [
    source 48
    target 1
    weight 0.774846290350769
  ]
  edge
  [
    source 228
    target 1
    weight 0.770700763361845
  ]
  edge
  [
    source 152
    target 5
    weight 0.763657738482142
  ]
  edge
  [
    source 4
    target 1
    weight 0.757608309603883
  ]
  edge
  [
    source 83
    target 0
    weight 0.775946374704948
  ]
  edge
  [
    source 194
    target 1
    weight 0.778183325048436
  ]
  edge
  [
    source 229
    target 5
    weight 0.768920761431669
  ]
  edge
  [
    source 124
    target 5
    weight 0.804986160948485
  ]
  edge
  [
    source 153
    target 0
    weight 0.771128037210357
  ]
  edge
  [
    source 83
    target 1
    weight 0.761377226008086
  ]
  edge
  [
    source 127
    target 5
    weight 0.777363763898576
  ]
  edge
  [
    source 154
    target 0
    weight 0.798071623296095
  ]
  edge
  [
    source 151
    target 1
    weight 0.769598140603739
  ]
  edge
  [
    source 132
    target 0
    weight 0.792945904239039
  ]
  edge
  [
    source 149
    target 0
    weight 0.759218537586023
  ]
  edge
  [
    source 174
    target 0
    weight 0.790358905413582
  ]
  edge
  [
    source 22
    target 0
    weight 0.763369935154052
  ]
  edge
  [
    source 230
    target 1
    weight 0.757924723622364
  ]
  edge
  [
    source 145
    target 0
    weight 0.782029460232003
  ]
  edge
  [
    source 57
    target 1
    weight 0.793383138975627
  ]
  edge
  [
    source 214
    target 0
    weight 0.774632766479786
  ]
  edge
  [
    source 120
    target 5
    weight 0.796196607861665
  ]
  edge
  [
    source 95
    target 1
    weight 0.792889601118134
  ]
  edge
  [
    source 35
    target 1
    weight 0.791644348916466
  ]
  edge
  [
    source 99
    target 1
    weight 0.777759827668229
  ]
  edge
  [
    source 33
    target 0
    weight 0.807876266443086
  ]
  edge
  [
    source 81
    target 1
    weight 0.80061571884089
  ]
  edge
  [
    source 154
    target 5
    weight 0.797052950393256
  ]
  edge
  [
    source 80
    target 1
    weight 0.795423067209617
  ]
  edge
  [
    source 155
    target 5
    weight 0.776556282027
  ]
  edge
  [
    source 139
    target 0
    weight 0.768011569880131
  ]
  edge
  [
    source 72
    target 0
    weight 0.771069289278939
  ]
  edge
  [
    source 44
    target 4
    weight 0.789245178812536
  ]
  edge
  [
    source 218
    target 1
    weight 0.75608652135166
  ]
  edge
  [
    source 69
    target 1
    weight 0.800600747655019
  ]
  edge
  [
    source 104
    target 5
    weight 0.772052323140264
  ]
  edge
  [
    source 88
    target 1
    weight 0.765142542022993
  ]
  edge
  [
    source 98
    target 1
    weight 0.77227783750264
  ]
  edge
  [
    source 156
    target 5
    weight 0.78506515348619
  ]
  edge
  [
    source 169
    target 4
    weight 0.78984923573003
  ]
  edge
  [
    source 190
    target 5
    weight 0.792634311309663
  ]
  edge
  [
    source 132
    target 5
    weight 0.783712929204265
  ]
  edge
  [
    source 117
    target 0
    weight 0.768525984389396
  ]
  edge
  [
    source 49
    target 4
    weight 0.750848147982623
  ]
  edge
  [
    source 78
    target 1
    weight 0.800153215987911
  ]
  edge
  [
    source 65
    target 4
    weight 0.761934910609838
  ]
  edge
  [
    source 231
    target 5
    weight 0.758929681141179
  ]
  edge
  [
    source 23
    target 1
    weight 0.765027183484255
  ]
  edge
  [
    source 164
    target 5
    weight 0.779313276523781
  ]
  edge
  [
    source 93
    target 1
    weight 0.793826120263553
  ]
  edge
  [
    source 157
    target 1
    weight 0.753909511244705
  ]
  edge
  [
    source 109
    target 1
    weight 0.787233886721486
  ]
  edge
  [
    source 135
    target 4
    weight 0.795715075866997
  ]
  edge
  [
    source 171
    target 1
    weight 0.805335746322406
  ]
  edge
  [
    source 117
    target 5
    weight 0.752382300422509
  ]
  edge
  [
    source 6
    target 5
    weight 0.758977953575607
  ]
  edge
  [
    source 17
    target 1
    weight 0.795830448861083
  ]
  edge
  [
    source 34
    target 1
    weight 0.778516922283837
  ]
  edge
  [
    source 115
    target 1
    weight 0.800429790170983
  ]
  edge
  [
    source 146
    target 0
    weight 0.789446292933228
  ]
  edge
  [
    source 42
    target 0
    weight 0.769781046539787
  ]
  edge
  [
    source 146
    target 4
    weight 0.795800927028531
  ]
  edge
  [
    source 7
    target 4
    weight 0.752273833227574
  ]
  edge
  [
    source 90
    target 0
    weight 0.767282387035833
  ]
  edge
  [
    source 133
    target 5
    weight 0.782935925627416
  ]
  edge
  [
    source 163
    target 1
    weight 0.789675746685734
  ]
  edge
  [
    source 62
    target 4
    weight 0.755648966459783
  ]
  edge
  [
    source 158
    target 5
    weight 0.804680089911847
  ]
  edge
  [
    source 71
    target 1
    weight 0.797699910103458
  ]
  edge
  [
    source 98
    target 0
    weight 0.7742035714521
  ]
  edge
  [
    source 58
    target 0
    weight 0.796465695316096
  ]
  edge
  [
    source 62
    target 0
    weight 0.793421837560255
  ]
  edge
  [
    source 81
    target 4
    weight 0.75922714319218
  ]
  edge
  [
    source 92
    target 1
    weight 0.791624017123164
  ]
  edge
  [
    source 26
    target 0
    weight 0.781884797087186
  ]
  edge
  [
    source 159
    target 5
    weight 0.783377358293108
  ]
  edge
  [
    source 73
    target 0
    weight 0.800058225461953
  ]
  edge
  [
    source 128
    target 0
    weight 0.775910512259864
  ]
  edge
  [
    source 70
    target 4
    weight 0.751768540032375
  ]
  edge
  [
    source 76
    target 4
    weight 0.759160541187884
  ]
  edge
  [
    source 147
    target 1
    weight 0.771115140745863
  ]
  edge
  [
    source 214
    target 4
    weight 0.775104895979148
  ]
  edge
  [
    source 21
    target 1
    weight 0.792679026023873
  ]
  edge
  [
    source 7
    target 0
    weight 0.799123152610648
  ]
  edge
  [
    source 150
    target 1
    weight 0.776083979895617
  ]
  edge
  [
    source 121
    target 1
    weight 0.754276401907655
  ]
  edge
  [
    source 130
    target 5
    weight 0.768610796580381
  ]
  edge
  [
    source 141
    target 5
    weight 0.79207615326439
  ]
  edge
  [
    source 102
    target 1
    weight 0.782006482278671
  ]
  edge
  [
    source 33
    target 1
    weight 0.797183161668454
  ]
  edge
  [
    source 47
    target 1
    weight 0.800532551608838
  ]
  edge
  [
    source 136
    target 0
    weight 0.799848687375712
  ]
  edge
  [
    source 87
    target 0
    weight 0.799729147213975
  ]
  edge
  [
    source 160
    target 5
    weight 0.771660968121974
  ]
  edge
  [
    source 161
    target 5
    weight 0.774570966982948
  ]
  edge
  [
    source 139
    target 4
    weight 0.782372532016075
  ]
  edge
  [
    source 162
    target 0
    weight 0.766798410347701
  ]
  edge
  [
    source 125
    target 0
    weight 0.751198901846011
  ]
  edge
  [
    source 163
    target 5
    weight 0.781933083790633
  ]
  edge
  [
    source 217
    target 0
    weight 0.765535225394947
  ]
  edge
  [
    source 232
    target 1
    weight 0.787575421354671
  ]
  edge
  [
    source 4
    target 3
    weight 0.794439565806531
  ]
  edge
  [
    source 105
    target 5
    weight 0.77570211065551
  ]
  edge
  [
    source 60
    target 4
    weight 0.763544184832643
  ]
  edge
  [
    source 107
    target 1
    weight 0.789540847945752
  ]
  edge
  [
    source 58
    target 4
    weight 0.758412639707315
  ]
  edge
  [
    source 233
    target 4
    weight 0.752709627915255
  ]
  edge
  [
    source 194
    target 4
    weight 0.776139594490969
  ]
  edge
  [
    source 23
    target 0
    weight 0.790514785181239
  ]
  edge
  [
    source 136
    target 5
    weight 0.779623957880082
  ]
  edge
  [
    source 199
    target 1
    weight 0.792624108307424
  ]
]
