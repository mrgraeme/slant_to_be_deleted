Creator "igraph version @VERSION@ Fri Jun  2 12:22:48 2017"
Version 1
graph
[
  directed 0
  node
  [
    id 0
    name "SPAC1B2.04"
  ]
  node
  [
    id 1
    name "SPAC3G9.03"
  ]
  node
  [
    id 2
    name "SPBC56F2.02"
  ]
  node
  [
    id 3
    name "SPBC19G7.03c"
  ]
  node
  [
    id 4
    name "SPAC23A1.08c"
  ]
  node
  [
    id 5
    name "SPBC1685.09"
  ]
  node
  [
    id 6
    name "SPBC800.04c"
  ]
  node
  [
    id 7
    name "SPBC2F12.07c"
  ]
  node
  [
    id 8
    name "SPBC1711.06"
  ]
  node
  [
    id 9
    name "SPBP8B7.03c"
  ]
  node
  [
    id 10
    name "SPBC1921.01c"
  ]
  node
  [
    id 11
    name "SPBC839.04"
  ]
  node
  [
    id 12
    name "SPBC83.02c"
  ]
  node
  [
    id 13
    name "SPAC19B12.04"
  ]
  node
  [
    id 14
    name "SPAC1952.07"
  ]
  node
  [
    id 15
    name "SPAPB1E7.02c"
  ]
  node
  [
    id 16
    name "SPBC56F2.07c"
  ]
  node
  [
    id 17
    name "SPBC1289.03c"
  ]
  node
  [
    id 18
    name "SPAC29A4.08c"
  ]
  node
  [
    id 19
    name "SPBC4C3.05c"
  ]
  node
  [
    id 20
    name "SPBC119.08"
  ]
  node
  [
    id 21
    name "SPAC343.03"
  ]
  node
  [
    id 22
    name "SPBC17G9.04c"
  ]
  node
  [
    id 23
    name "SPCC790.02"
  ]
  node
  [
    id 24
    name "SPBC409.05"
  ]
  node
  [
    id 25
    name "SPAC1B1.03c"
  ]
  node
  [
    id 26
    name "SPCC63.05"
  ]
  node
  [
    id 27
    name "SPAC3G6.06c"
  ]
  node
  [
    id 28
    name "SPAC12B10.01c"
  ]
  node
  [
    id 29
    name "SPBP16F5.04"
  ]
  node
  [
    id 30
    name "SPBP8B7.21"
  ]
  node
  [
    id 31
    name "SPBC776.09"
  ]
  node
  [
    id 32
    name "SPAC9.05"
  ]
  node
  [
    id 33
    name "SPCC5E4.06"
  ]
  node
  [
    id 34
    name "SPAC20H4.04"
  ]
  node
  [
    id 35
    name "SPBC902.02c"
  ]
  node
  [
    id 36
    name "SPBC1703.04"
  ]
  node
  [
    id 37
    name "SPBC16D10.04c"
  ]
  node
  [
    id 38
    name "SPAC3H5.05c"
  ]
  node
  [
    id 39
    name "SPAC8E11.03c"
  ]
  node
  [
    id 40
    name "SPAC25G10.08"
  ]
  node
  [
    id 41
    name "SPBC342.02"
  ]
  node
  [
    id 42
    name "SPBC1A4.08c"
  ]
  node
  [
    id 43
    name "SPAC20G8.06"
  ]
  node
  [
    id 44
    name "SPBC530.06c"
  ]
  node
  [
    id 45
    name "SPBC23E6.07c"
  ]
  node
  [
    id 46
    name "SPBC36.09"
  ]
  node
  [
    id 47
    name "SPBC1347.01c"
  ]
  node
  [
    id 48
    name "SPCC4G3.05c"
  ]
  node
  [
    id 49
    name "SPBC1709.08"
  ]
  node
  [
    id 50
    name "SPAC2C4.03c"
  ]
  node
  [
    id 51
    name "SPBC29A3.07c"
  ]
  node
  [
    id 52
    name "SPAC57A10.09c"
  ]
  node
  [
    id 53
    name "SPBC3E7.08c"
  ]
  node
  [
    id 54
    name "SPBC9B6.06"
  ]
  node
  [
    id 55
    name "SPAC29A4.04c"
  ]
  node
  [
    id 56
    name "SPAC19G12.01c"
  ]
  node
  [
    id 57
    name "SPBC609.05"
  ]
  node
  [
    id 58
    name "SPAC20G8.09c"
  ]
  node
  [
    id 59
    name "SPAC29E6.02"
  ]
  node
  [
    id 60
    name "SPBC1685.02c"
  ]
  node
  [
    id 61
    name "SPBC56F2.04"
  ]
  node
  [
    id 62
    name "SPAC15A10.02"
  ]
  node
  [
    id 63
    name "SPBC21H7.02"
  ]
  node
  [
    id 64
    name "SPBC32F12.05c"
  ]
  node
  [
    id 65
    name "SPBC2G5.07c"
  ]
  node
  [
    id 66
    name "SPAPB8E5.07c"
  ]
  node
  [
    id 67
    name "SPBC11C11.09c"
  ]
  node
  [
    id 68
    name "SPBC29A10.03c"
  ]
  node
  [
    id 69
    name "SPAC227.05"
  ]
  node
  [
    id 70
    name "SPAPB1A10.06c"
  ]
  node
  [
    id 71
    name "SPBC29A3.04"
  ]
  node
  [
    id 72
    name "SPAC6F6.07c"
  ]
  node
  [
    id 73
    name "SPBC31E1.06"
  ]
  node
  [
    id 74
    name "SPAC1B3.09c"
  ]
  node
  [
    id 75
    name "SPAC16E8.06c"
  ]
  node
  [
    id 76
    name "SPAC1142.04"
  ]
  node
  [
    id 77
    name "SPAC29B12.02c"
  ]
  node
  [
    id 78
    name "SPBC25D12.03c"
  ]
  node
  [
    id 79
    name "SPAC323.02c"
  ]
  node
  [
    id 80
    name "SPBC9B6.05c"
  ]
  node
  [
    id 81
    name "SPBC1105.03c"
  ]
  node
  [
    id 82
    name "SPBC19C7.06"
  ]
  node
  [
    id 83
    name "SPBC1604.06c"
  ]
  node
  [
    id 84
    name "SPBC1734.06"
  ]
  node
  [
    id 85
    name "SPAC513.01c"
  ]
  node
  [
    id 86
    name "SPAP19A11.05c"
  ]
  node
  [
    id 87
    name "SPAC2F7.08c"
  ]
  node
  [
    id 88
    name "SPAC22H10.03c"
  ]
  node
  [
    id 89
    name "SPBC1703.05"
  ]
  node
  [
    id 90
    name "SPAC1006.09"
  ]
  node
  [
    id 91
    name "SPAC3G9.04"
  ]
  node
  [
    id 92
    name "SPAC1296.02"
  ]
  node
  [
    id 93
    name "SPAC31A2.05c"
  ]
  node
  [
    id 94
    name "SPBC725.09c"
  ]
  node
  [
    id 95
    name "SPAC630.08c"
  ]
  node
  [
    id 96
    name "SPAC12G12.04"
  ]
  node
  [
    id 97
    name "SPBC13G1.03c"
  ]
  node
  [
    id 98
    name "SPCC1494.06c"
  ]
  node
  [
    id 99
    name "SPBC4B4.09"
  ]
  node
  [
    id 100
    name "SPBC14F5.08"
  ]
  node
  [
    id 101
    name "SPAC1071.02"
  ]
  node
  [
    id 102
    name "SPAC25A8.01c"
  ]
  node
  [
    id 103
    name "SPAC27D7.06"
  ]
  node
  [
    id 104
    name "SPAC31A2.02"
  ]
  node
  [
    id 105
    name "SPAC1805.01c"
  ]
  node
  [
    id 106
    name "SPAC3H5.08c"
  ]
  node
  [
    id 107
    name "SPAC22F3.08c"
  ]
  node
  [
    id 108
    name "SPBC16A3.05c"
  ]
  node
  [
    id 109
    name "SPAC9G1.02"
  ]
  node
  [
    id 110
    name "SPAC1610.01"
  ]
  node
  [
    id 111
    name "SPAC4G9.02"
  ]
  node
  [
    id 112
    name "SPAC1834.07"
  ]
  node
  [
    id 113
    name "SPBC26H8.05c"
  ]
  node
  [
    id 114
    name "SPBC16H5.05c"
  ]
  node
  [
    id 115
    name "SPBC12D12.06"
  ]
  node
  [
    id 116
    name "SPAC589.02c"
  ]
  node
  [
    id 117
    name "SPAC17D4.01"
  ]
  node
  [
    id 118
    name "SPMIT.01"
  ]
  node
  [
    id 119
    name "SPCP31B10.08c"
  ]
  node
  [
    id 120
    name "SPCP31B10.07"
  ]
  node
  [
    id 121
    name "SPCC970.09"
  ]
  node
  [
    id 122
    name "SPCC970.01"
  ]
  node
  [
    id 123
    name "SPCC962.04"
  ]
  node
  [
    id 124
    name "SPCC737.02c"
  ]
  node
  [
    id 125
    name "SPCC550.02c"
  ]
  node
  [
    id 126
    name "SPCC4B3.09c"
  ]
  node
  [
    id 127
    name "SPCC285.08"
  ]
  node
  [
    id 128
    name "SPCC188.03"
  ]
  node
  [
    id 129
    name "SPCC16A11.02"
  ]
  node
  [
    id 130
    name "SPCC11E10.06c"
  ]
  node
  [
    id 131
    name "SPCC1020.04c"
  ]
  node
  [
    id 132
    name "SPBC800.06"
  ]
  node
  [
    id 133
    name "SPBC4B4.03"
  ]
  node
  [
    id 134
    name "SPBC18H10.03"
  ]
  node
  [
    id 135
    name "SPBC1778.08c"
  ]
  node
  [
    id 136
    name "SPBC27B12.03c"
  ]
  node
  [
    id 137
    name "SPBC2G5.04c"
  ]
  node
  [
    id 138
    name "SPBC17D1.02"
  ]
  node
  [
    id 139
    name "SPBC17D11.08"
  ]
  node
  [
    id 140
    name "SPBC3B8.03"
  ]
  node
  [
    id 141
    name "SPBC685.04c"
  ]
  node
  [
    id 142
    name "SPBP8B7.23"
  ]
  node
  [
    id 143
    name "SPCC11E10.08"
  ]
  node
  [
    id 144
    name "SPCC74.02c"
  ]
  node
  [
    id 145
    name "SPCC794.01c"
  ]
  node
  [
    id 146
    name "SPCPB16A4.04c"
  ]
  node
  [
    id 147
    name "SPBC1778.09"
  ]
  node
  [
    id 148
    name "SPCC1827.04"
  ]
  edge
  [
    source 118
    target 0
    weight 0.844872570005278
  ]
  edge
  [
    source 119
    target 1
    weight 0.816308347265455
  ]
  edge
  [
    source 119
    target 2
    weight 0.829063618267419
  ]
  edge
  [
    source 119
    target 3
    weight 0.750073812523193
  ]
  edge
  [
    source 119
    target 4
    weight 0.839277144495567
  ]
  edge
  [
    source 119
    target 5
    weight 0.750602215690525
  ]
  edge
  [
    source 119
    target 6
    weight 0.815467848690327
  ]
  edge
  [
    source 119
    target 7
    weight 0.840037989472653
  ]
  edge
  [
    source 119
    target 8
    weight 0.824679541676416
  ]
  edge
  [
    source 119
    target 9
    weight 0.824679541676416
  ]
  edge
  [
    source 119
    target 10
    weight 0.806060472802277
  ]
  edge
  [
    source 119
    target 11
    weight 0.840037989472653
  ]
  edge
  [
    source 119
    target 12
    weight 0.815467848690327
  ]
  edge
  [
    source 119
    target 13
    weight 0.750073812523193
  ]
  edge
  [
    source 120
    target 14
    weight 0.753214633760991
  ]
  edge
  [
    source 121
    target 15
    weight 0.767442555354082
  ]
  edge
  [
    source 121
    target 16
    weight 0.760849276709321
  ]
  edge
  [
    source 121
    target 17
    weight 0.81720647081953
  ]
  edge
  [
    source 121
    target 18
    weight 0.759542390369438
  ]
  edge
  [
    source 121
    target 19
    weight 0.779541998871183
  ]
  edge
  [
    source 121
    target 20
    weight 0.79148948715776
  ]
  edge
  [
    source 121
    target 21
    weight 0.770870155979898
  ]
  edge
  [
    source 121
    target 22
    weight 0.800095844851451
  ]
  edge
  [
    source 121
    target 23
    weight 0.79857410934073
  ]
  edge
  [
    source 121
    target 24
    weight 0.806036520504806
  ]
  edge
  [
    source 121
    target 25
    weight 0.771794190547852
  ]
  edge
  [
    source 121
    target 26
    weight 0.799100521341071
  ]
  edge
  [
    source 121
    target 27
    weight 0.770495563542904
  ]
  edge
  [
    source 121
    target 28
    weight 0.803968542311344
  ]
  edge
  [
    source 121
    target 29
    weight 0.759697623928563
  ]
  edge
  [
    source 121
    target 30
    weight 0.800744047238405
  ]
  edge
  [
    source 121
    target 31
    weight 0.774195834538487
  ]
  edge
  [
    source 122
    target 32
    weight 0.785160777149057
  ]
  edge
  [
    source 122
    target 33
    weight 0.774798226519788
  ]
  edge
  [
    source 122
    target 34
    weight 0.796115471038398
  ]
  edge
  [
    source 122
    target 35
    weight 0.777054406624325
  ]
  edge
  [
    source 122
    target 36
    weight 0.780962156007059
  ]
  edge
  [
    source 122
    target 37
    weight 0.809843713906342
  ]
  edge
  [
    source 123
    target 12
    weight 0.787496862385277
  ]
  edge
  [
    source 123
    target 3
    weight 0.831623819644951
  ]
  edge
  [
    source 123
    target 10
    weight 0.781825777804526
  ]
  edge
  [
    source 123
    target 6
    weight 0.787496862385277
  ]
  edge
  [
    source 123
    target 38
    weight 0.824022046338415
  ]
  edge
  [
    source 122
    target 39
    weight 0.785743377900538
  ]
  edge
  [
    source 26
    target 23
    weight 0.785628591889251
  ]
  edge
  [
    source 40
    target 23
    weight 0.798883295606675
  ]
  edge
  [
    source 23
    target 17
    weight 0.773422227776093
  ]
  edge
  [
    source 28
    target 23
    weight 0.815843740399281
  ]
  edge
  [
    source 29
    target 23
    weight 0.76791604929548
  ]
  edge
  [
    source 23
    target 20
    weight 0.806724529385174
  ]
  edge
  [
    source 41
    target 23
    weight 0.763611554106811
  ]
  edge
  [
    source 23
    target 18
    weight 0.771707523785971
  ]
  edge
  [
    source 42
    target 23
    weight 0.768601445030443
  ]
  edge
  [
    source 23
    target 19
    weight 0.786200852619508
  ]
  edge
  [
    source 23
    target 21
    weight 0.792695354295445
  ]
  edge
  [
    source 124
    target 0
    weight 0.758078964730219
  ]
  edge
  [
    source 26
    target 20
    weight 0.766509223348092
  ]
  edge
  [
    source 42
    target 26
    weight 0.815483753760963
  ]
  edge
  [
    source 27
    target 26
    weight 0.784248414100641
  ]
  edge
  [
    source 43
    target 26
    weight 0.813945020445224
  ]
  edge
  [
    source 26
    target 17
    weight 0.825755746140541
  ]
  edge
  [
    source 41
    target 26
    weight 0.801285185754375
  ]
  edge
  [
    source 44
    target 26
    weight 0.776753769224757
  ]
  edge
  [
    source 30
    target 26
    weight 0.816576536400042
  ]
  edge
  [
    source 45
    target 26
    weight 0.789194463396164
  ]
  edge
  [
    source 26
    target 16
    weight 0.802110996722085
  ]
  edge
  [
    source 46
    target 26
    weight 0.794370877689902
  ]
  edge
  [
    source 28
    target 26
    weight 0.804521750427978
  ]
  edge
  [
    source 26
    target 19
    weight 0.799129710224994
  ]
  edge
  [
    source 47
    target 26
    weight 0.77181521478421
  ]
  edge
  [
    source 26
    target 22
    weight 0.812216846369796
  ]
  edge
  [
    source 26
    target 14
    weight 0.819110272602103
  ]
  edge
  [
    source 31
    target 26
    weight 0.798264776728968
  ]
  edge
  [
    source 33
    target 32
    weight 0.770758038873716
  ]
  edge
  [
    source 48
    target 33
    weight 0.778455823656825
  ]
  edge
  [
    source 121
    target 49
    weight 0.757072488820854
  ]
  edge
  [
    source 125
    target 50
    weight 0.804849892186903
  ]
  edge
  [
    source 121
    target 41
    weight 0.787441896825748
  ]
  edge
  [
    source 125
    target 51
    weight 0.762672465951634
  ]
  edge
  [
    source 48
    target 34
    weight 0.793765734002316
  ]
  edge
  [
    source 52
    target 48
    weight 0.76354299169149
  ]
  edge
  [
    source 48
    target 39
    weight 0.822752610376722
  ]
  edge
  [
    source 53
    target 48
    weight 0.808027111985437
  ]
  edge
  [
    source 126
    target 54
    weight 0.79376047255086
  ]
  edge
  [
    source 98
    target 55
    weight 0.755911533921241
  ]
  edge
  [
    source 56
    target 21
    weight 0.766652139180813
  ]
  edge
  [
    source 127
    target 43
    weight 0.764293895446875
  ]
  edge
  [
    source 40
    target 26
    weight 0.823626970983922
  ]
  edge
  [
    source 68
    target 36
    weight 0.759702012704324
  ]
  edge
  [
    source 34
    target 33
    weight 0.766619286463071
  ]
  edge
  [
    source 23
    target 16
    weight 0.766455252558498
  ]
  edge
  [
    source 45
    target 29
    weight 0.790358191535531
  ]
  edge
  [
    source 72
    target 13
    weight 0.778023532504737
  ]
  edge
  [
    source 128
    target 57
    weight 0.754520071230545
  ]
  edge
  [
    source 43
    target 28
    weight 0.766268812484216
  ]
  edge
  [
    source 60
    target 4
    weight 0.78194501808242
  ]
  edge
  [
    source 84
    target 15
    weight 0.762763806592754
  ]
  edge
  [
    source 43
    target 17
    weight 0.810478894991003
  ]
  edge
  [
    source 129
    target 58
    weight 0.826583815697462
  ]
  edge
  [
    source 98
    target 16
    weight 0.756526893919868
  ]
  edge
  [
    source 98
    target 58
    weight 0.804917227988115
  ]
  edge
  [
    source 10
    target 3
    weight 0.79497810385181
  ]
  edge
  [
    source 49
    target 19
    weight 0.756729167896068
  ]
  edge
  [
    source 125
    target 59
    weight 0.755015188505692
  ]
  edge
  [
    source 60
    target 12
    weight 0.758926272196534
  ]
  edge
  [
    source 129
    target 61
    weight 0.843085185163801
  ]
  edge
  [
    source 130
    target 62
    weight 0.751876279551194
  ]
  edge
  [
    source 130
    target 63
    weight 0.787885321068598
  ]
  edge
  [
    source 99
    target 64
    weight 0.762528255846822
  ]
  edge
  [
    source 5
    target 3
    weight 0.766785301721388
  ]
  edge
  [
    source 131
    target 52
    weight 0.752895489540941
  ]
  edge
  [
    source 72
    target 38
    weight 0.788803373314801
  ]
  edge
  [
    source 131
    target 65
    weight 0.811416973029469
  ]
  edge
  [
    source 80
    target 59
    weight 0.805227555154719
  ]
  edge
  [
    source 98
    target 41
    weight 0.763258904878287
  ]
  edge
  [
    source 30
    target 22
    weight 0.772806542035941
  ]
  edge
  [
    source 66
    target 22
    weight 0.754830991407744
  ]
  edge
  [
    source 57
    target 30
    weight 0.775621855905527
  ]
  edge
  [
    source 41
    target 30
    weight 0.764525140147097
  ]
  edge
  [
    source 49
    target 30
    weight 0.783801693706945
  ]
  edge
  [
    source 30
    target 14
    weight 0.796268261712196
  ]
  edge
  [
    source 67
    target 5
    weight 0.784123447855324
  ]
  edge
  [
    source 68
    target 35
    weight 0.767662564232579
  ]
  edge
  [
    source 12
    target 9
    weight 0.800407323759663
  ]
  edge
  [
    source 57
    target 46
    weight 0.763845142523499
  ]
  edge
  [
    source 10
    target 9
    weight 0.809321650655406
  ]
  edge
  [
    source 9
    target 7
    weight 0.823483148961321
  ]
  edge
  [
    source 9
    target 4
    weight 0.814979041735941
  ]
  edge
  [
    source 9
    target 2
    weight 0.812618937954674
  ]
  edge
  [
    source 38
    target 9
    weight 0.790128702565762
  ]
  edge
  [
    source 11
    target 9
    weight 0.823483148961321
  ]
  edge
  [
    source 45
    target 39
    weight 0.761205124202987
  ]
  edge
  [
    source 29
    target 16
    weight 0.788811904939155
  ]
  edge
  [
    source 41
    target 29
    weight 0.77647164941749
  ]
  edge
  [
    source 29
    target 14
    weight 0.789660821762974
  ]
  edge
  [
    source 43
    target 29
    weight 0.817412312154866
  ]
  edge
  [
    source 29
    target 28
    weight 0.781529000658415
  ]
  edge
  [
    source 47
    target 29
    weight 0.754084781752761
  ]
  edge
  [
    source 57
    target 29
    weight 0.811626439855001
  ]
  edge
  [
    source 8
    target 7
    weight 0.829571896254466
  ]
  edge
  [
    source 80
    target 50
    weight 0.759194683302677
  ]
  edge
  [
    source 80
    target 64
    weight 0.788339173377437
  ]
  edge
  [
    source 22
    target 16
    weight 0.760332296944429
  ]
  edge
  [
    source 69
    target 35
    weight 0.756045814825291
  ]
  edge
  [
    source 49
    target 28
    weight 0.810720974573711
  ]
  edge
  [
    source 70
    target 66
    weight 0.771532882073256
  ]
  edge
  [
    source 67
    target 11
    weight 0.82402720789763
  ]
  edge
  [
    source 34
    target 32
    weight 0.808671385066152
  ]
  edge
  [
    source 71
    target 11
    weight 0.828556657297498
  ]
  edge
  [
    source 71
    target 12
    weight 0.759932461453053
  ]
  edge
  [
    source 12
    target 7
    weight 0.82146431519168
  ]
  edge
  [
    source 12
    target 6
    weight 0.806754225938523
  ]
  edge
  [
    source 13
    target 12
    weight 0.761526333830641
  ]
  edge
  [
    source 12
    target 10
    weight 0.827826121388493
  ]
  edge
  [
    source 72
    target 11
    weight 0.755287908480816
  ]
  edge
  [
    source 38
    target 12
    weight 0.760428498659907
  ]
  edge
  [
    source 132
    target 73
    weight 0.783846433783142
  ]
  edge
  [
    source 132
    target 74
    weight 0.788704866522303
  ]
  edge
  [
    source 132
    target 75
    weight 0.797407472043562
  ]
  edge
  [
    source 8
    target 6
    weight 0.779691866432109
  ]
  edge
  [
    source 6
    target 3
    weight 0.761526333830641
  ]
  edge
  [
    source 6
    target 2
    weight 0.829589639348097
  ]
  edge
  [
    source 71
    target 6
    weight 0.759932461453053
  ]
  edge
  [
    source 36
    target 35
    weight 0.788198478823315
  ]
  edge
  [
    source 6
    target 1
    weight 0.754941983608433
  ]
  edge
  [
    source 13
    target 6
    weight 0.761526333830641
  ]
  edge
  [
    source 43
    target 31
    weight 0.823413651953452
  ]
  edge
  [
    source 41
    target 31
    weight 0.770666274050705
  ]
  edge
  [
    source 45
    target 31
    weight 0.765887995615005
  ]
  edge
  [
    source 6
    target 4
    weight 0.827101754027604
  ]
  edge
  [
    source 31
    target 20
    weight 0.77943716622759
  ]
  edge
  [
    source 31
    target 14
    weight 0.771414103193499
  ]
  edge
  [
    source 10
    target 6
    weight 0.827826121388493
  ]
  edge
  [
    source 98
    target 76
    weight 0.832219251579907
  ]
  edge
  [
    source 57
    target 25
    weight 0.754183999266019
  ]
  edge
  [
    source 94
    target 24
    weight 0.768017300268908
  ]
  edge
  [
    source 94
    target 15
    weight 0.780384426779291
  ]
  edge
  [
    source 17
    target 14
    weight 0.787644036344497
  ]
  edge
  [
    source 42
    target 29
    weight 0.793203127404096
  ]
  edge
  [
    source 12
    target 1
    weight 0.754941983608433
  ]
  edge
  [
    source 77
    target 57
    weight 0.766515886396938
  ]
  edge
  [
    source 57
    target 22
    weight 0.786197681838821
  ]
  edge
  [
    source 57
    target 19
    weight 0.80938355041528
  ]
  edge
  [
    source 57
    target 43
    weight 0.795191209566611
  ]
  edge
  [
    source 78
    target 57
    weight 0.797899053700003
  ]
  edge
  [
    source 57
    target 21
    weight 0.770832779604507
  ]
  edge
  [
    source 42
    target 16
    weight 0.782043644927845
  ]
  edge
  [
    source 46
    target 22
    weight 0.773459477633021
  ]
  edge
  [
    source 43
    target 16
    weight 0.762837537950772
  ]
  edge
  [
    source 79
    target 16
    weight 0.751821842973212
  ]
  edge
  [
    source 40
    target 16
    weight 0.785335805715687
  ]
  edge
  [
    source 41
    target 16
    weight 0.78912010296218
  ]
  edge
  [
    source 17
    target 16
    weight 0.772397330276372
  ]
  edge
  [
    source 43
    target 42
    weight 0.797233206242009
  ]
  edge
  [
    source 45
    target 36
    weight 0.761573818898775
  ]
  edge
  [
    source 61
    target 55
    weight 0.750938087170249
  ]
  edge
  [
    source 66
    target 61
    weight 0.814763909320747
  ]
  edge
  [
    source 46
    target 23
    weight 0.777178732528704
  ]
  edge
  [
    source 78
    target 26
    weight 0.793304390938775
  ]
  edge
  [
    source 7
    target 2
    weight 0.826526846380534
  ]
  edge
  [
    source 38
    target 2
    weight 0.756476410134479
  ]
  edge
  [
    source 71
    target 2
    weight 0.78504184575763
  ]
  edge
  [
    source 5
    target 2
    weight 0.752629983242328
  ]
  edge
  [
    source 60
    target 2
    weight 0.752412899866526
  ]
  edge
  [
    source 79
    target 43
    weight 0.77328167401309
  ]
  edge
  [
    source 8
    target 2
    weight 0.830737327897973
  ]
  edge
  [
    source 125
    target 80
    weight 0.808690557422452
  ]
  edge
  [
    source 57
    target 26
    weight 0.825191783009461
  ]
  edge
  [
    source 30
    target 28
    weight 0.757797644055801
  ]
  edge
  [
    source 57
    target 42
    weight 0.795809987290463
  ]
  edge
  [
    source 45
    target 19
    weight 0.762294681913056
  ]
  edge
  [
    source 43
    target 19
    weight 0.792127629619163
  ]
  edge
  [
    source 12
    target 4
    weight 0.827101754027604
  ]
  edge
  [
    source 27
    target 19
    weight 0.801131194707378
  ]
  edge
  [
    source 52
    target 19
    weight 0.773444228861515
  ]
  edge
  [
    source 46
    target 19
    weight 0.760568773900966
  ]
  edge
  [
    source 30
    target 15
    weight 0.761498030685322
  ]
  edge
  [
    source 40
    target 30
    weight 0.754513800738412
  ]
  edge
  [
    source 39
    target 30
    weight 0.751785183637744
  ]
  edge
  [
    source 49
    target 43
    weight 0.804693165641882
  ]
  edge
  [
    source 26
    target 24
    weight 0.780199071659051
  ]
  edge
  [
    source 133
    target 62
    weight 0.788512902747313
  ]
  edge
  [
    source 99
    target 59
    weight 0.750414824547754
  ]
  edge
  [
    source 133
    target 63
    weight 0.773025735822612
  ]
  edge
  [
    source 49
    target 29
    weight 0.803835897879838
  ]
  edge
  [
    source 73
    target 70
    weight 0.82055914502966
  ]
  edge
  [
    source 30
    target 18
    weight 0.75621308905747
  ]
  edge
  [
    source 28
    target 24
    weight 0.812563597638606
  ]
  edge
  [
    source 77
    target 52
    weight 0.773210928992218
  ]
  edge
  [
    source 24
    target 18
    weight 0.769326191692227
  ]
  edge
  [
    source 43
    target 24
    weight 0.762664552529907
  ]
  edge
  [
    source 27
    target 24
    weight 0.769897385862145
  ]
  edge
  [
    source 45
    target 24
    weight 0.751101050042817
  ]
  edge
  [
    source 99
    target 50
    weight 0.796828296294199
  ]
  edge
  [
    source 53
    target 39
    weight 0.775386352061523
  ]
  edge
  [
    source 24
    target 21
    weight 0.803303550018049
  ]
  edge
  [
    source 53
    target 37
    weight 0.754313193275022
  ]
  edge
  [
    source 53
    target 34
    weight 0.750754219119178
  ]
  edge
  [
    source 42
    target 41
    weight 0.798976350041583
  ]
  edge
  [
    source 20
    target 15
    weight 0.762707619265699
  ]
  edge
  [
    source 38
    target 6
    weight 0.760428498659907
  ]
  edge
  [
    source 46
    target 42
    weight 0.773164244902925
  ]
  edge
  [
    source 46
    target 18
    weight 0.827744153732671
  ]
  edge
  [
    source 46
    target 43
    weight 0.785575081776573
  ]
  edge
  [
    source 28
    target 20
    weight 0.783333635928407
  ]
  edge
  [
    source 94
    target 14
    weight 0.773045824918538
  ]
  edge
  [
    source 41
    target 40
    weight 0.817463803805819
  ]
  edge
  [
    source 41
    target 17
    weight 0.75420405160528
  ]
  edge
  [
    source 41
    target 22
    weight 0.773308011837966
  ]
  edge
  [
    source 47
    target 45
    weight 0.781895136773794
  ]
  edge
  [
    source 78
    target 41
    weight 0.756097108271306
  ]
  edge
  [
    source 81
    target 54
    weight 0.808813254129395
  ]
  edge
  [
    source 133
    target 52
    weight 0.76044221213146
  ]
  edge
  [
    source 10
    target 2
    weight 0.822913879686985
  ]
  edge
  [
    source 64
    target 59
    weight 0.772725895679632
  ]
  edge
  [
    source 64
    target 50
    weight 0.788962597888772
  ]
  edge
  [
    source 82
    target 41
    weight 0.808778875243559
  ]
  edge
  [
    source 75
    target 73
    weight 0.768871742201242
  ]
  edge
  [
    source 40
    target 29
    weight 0.778044871236413
  ]
  edge
  [
    source 94
    target 22
    weight 0.764003860135814
  ]
  edge
  [
    source 83
    target 73
    weight 0.788931397449566
  ]
  edge
  [
    source 84
    target 23
    weight 0.800228773815121
  ]
  edge
  [
    source 40
    target 28
    weight 0.758467161316703
  ]
  edge
  [
    source 120
    target 85
    weight 0.813340761875993
  ]
  edge
  [
    source 29
    target 26
    weight 0.823128489175872
  ]
  edge
  [
    source 60
    target 13
    weight 0.831844955449375
  ]
  edge
  [
    source 57
    target 23
    weight 0.761194106503541
  ]
  edge
  [
    source 57
    target 52
    weight 0.804900402115191
  ]
  edge
  [
    source 9
    target 8
    weight 0.805955480005395
  ]
  edge
  [
    source 9
    target 6
    weight 0.800407323759663
  ]
  edge
  [
    source 38
    target 7
    weight 0.75937728704292
  ]
  edge
  [
    source 122
    target 53
    weight 0.799530312679434
  ]
  edge
  [
    source 43
    target 18
    weight 0.758773408043015
  ]
  edge
  [
    source 30
    target 29
    weight 0.769697578468134
  ]
  edge
  [
    source 11
    target 4
    weight 0.818546547528397
  ]
  edge
  [
    source 71
    target 38
    weight 0.790540452895448
  ]
  edge
  [
    source 72
    target 71
    weight 0.77980756467516
  ]
  edge
  [
    source 38
    target 11
    weight 0.76073561654966
  ]
  edge
  [
    source 41
    target 39
    weight 0.769005808243702
  ]
  edge
  [
    source 76
    target 61
    weight 0.776233000646732
  ]
  edge
  [
    source 66
    target 15
    weight 0.761194670710922
  ]
  edge
  [
    source 42
    target 39
    weight 0.762939079038915
  ]
  edge
  [
    source 11
    target 10
    weight 0.818507848905745
  ]
  edge
  [
    source 78
    target 42
    weight 0.796037069537647
  ]
  edge
  [
    source 86
    target 54
    weight 0.790871201563594
  ]
  edge
  [
    source 78
    target 43
    weight 0.763239845998335
  ]
  edge
  [
    source 49
    target 24
    weight 0.776542131533635
  ]
  edge
  [
    source 42
    target 19
    weight 0.757052782528525
  ]
  edge
  [
    source 78
    target 45
    weight 0.81303673170589
  ]
  edge
  [
    source 52
    target 45
    weight 0.79920638213075
  ]
  edge
  [
    source 45
    target 37
    weight 0.76581678904544
  ]
  edge
  [
    source 45
    target 27
    weight 0.839013416885803
  ]
  edge
  [
    source 45
    target 43
    weight 0.75764442840153
  ]
  edge
  [
    source 59
    target 50
    weight 0.817256303582003
  ]
  edge
  [
    source 45
    target 14
    weight 0.823522317347015
  ]
  edge
  [
    source 78
    target 29
    weight 0.807926468785969
  ]
  edge
  [
    source 63
    target 52
    weight 0.757161364893369
  ]
  edge
  [
    source 87
    target 63
    weight 0.792616207284009
  ]
  edge
  [
    source 7
    target 6
    weight 0.82146431519168
  ]
  edge
  [
    source 67
    target 1
    weight 0.806079230061404
  ]
  edge
  [
    source 27
    target 14
    weight 0.846837712854576
  ]
  edge
  [
    source 11
    target 8
    weight 0.82975067718648
  ]
  edge
  [
    source 45
    target 15
    weight 0.831076487497378
  ]
  edge
  [
    source 127
    target 42
    weight 0.751329817995409
  ]
  edge
  [
    source 84
    target 21
    weight 0.790480447113213
  ]
  edge
  [
    source 121
    target 14
    weight 0.809421791557197
  ]
  edge
  [
    source 42
    target 22
    weight 0.780044589891705
  ]
  edge
  [
    source 42
    target 37
    weight 0.760805743025304
  ]
  edge
  [
    source 49
    target 42
    weight 0.769844573580916
  ]
  edge
  [
    source 72
    target 5
    weight 0.808703269974025
  ]
  edge
  [
    source 38
    target 3
    weight 0.824490337163498
  ]
  edge
  [
    source 66
    target 58
    weight 0.81854161966948
  ]
  edge
  [
    source 121
    target 40
    weight 0.814581674353126
  ]
  edge
  [
    source 30
    target 20
    weight 0.77800784639137
  ]
  edge
  [
    source 25
    target 18
    weight 0.767504975318706
  ]
  edge
  [
    source 88
    target 17
    weight 0.753853312108217
  ]
  edge
  [
    source 42
    target 40
    weight 0.771526940067191
  ]
  edge
  [
    source 61
    target 27
    weight 0.750519365872974
  ]
  edge
  [
    source 10
    target 4
    weight 0.839681036588384
  ]
  edge
  [
    source 47
    target 14
    weight 0.814379296207174
  ]
  edge
  [
    source 71
    target 4
    weight 0.805006736250051
  ]
  edge
  [
    source 94
    target 25
    weight 0.780060617575432
  ]
  edge
  [
    source 13
    target 10
    weight 0.750102802461879
  ]
  edge
  [
    source 134
    target 89
    weight 0.759356207139437
  ]
  edge
  [
    source 7
    target 4
    weight 0.8181009454696
  ]
  edge
  [
    source 127
    target 29
    weight 0.772081643007905
  ]
  edge
  [
    source 77
    target 68
    weight 0.771825059677782
  ]
  edge
  [
    source 25
    target 22
    weight 0.788176230656835
  ]
  edge
  [
    source 52
    target 15
    weight 0.780195272220528
  ]
  edge
  [
    source 71
    target 10
    weight 0.804081741017262
  ]
  edge
  [
    source 22
    target 15
    weight 0.800952199805392
  ]
  edge
  [
    source 43
    target 22
    weight 0.797544992630203
  ]
  edge
  [
    source 40
    target 22
    weight 0.767708229484595
  ]
  edge
  [
    source 98
    target 42
    weight 0.761401707135244
  ]
  edge
  [
    source 22
    target 17
    weight 0.759016934575109
  ]
  edge
  [
    source 25
    target 15
    weight 0.776815753749539
  ]
  edge
  [
    source 79
    target 22
    weight 0.794803646803348
  ]
  edge
  [
    source 31
    target 30
    weight 0.751545214877234
  ]
  edge
  [
    source 42
    target 14
    weight 0.803681159043714
  ]
  edge
  [
    source 109
    target 90
    weight 0.753859399668153
  ]
  edge
  [
    source 24
    target 20
    weight 0.795000711391728
  ]
  edge
  [
    source 67
    target 9
    weight 0.811709069017563
  ]
  edge
  [
    source 78
    target 58
    weight 0.758051134623086
  ]
  edge
  [
    source 4
    target 3
    weight 0.791989369925844
  ]
  edge
  [
    source 72
    target 7
    weight 0.754266585291749
  ]
  edge
  [
    source 123
    target 13
    weight 0.831623819644951
  ]
  edge
  [
    source 94
    target 21
    weight 0.786252721006803
  ]
  edge
  [
    source 2
    target 1
    weight 0.830865370684532
  ]
  edge
  [
    source 57
    target 27
    weight 0.789674120112495
  ]
  edge
  [
    source 130
    target 52
    weight 0.768233678068611
  ]
  edge
  [
    source 37
    target 36
    weight 0.758150836551735
  ]
  edge
  [
    source 11
    target 7
    weight 0.806754225938523
  ]
  edge
  [
    source 91
    target 49
    weight 0.772796817098402
  ]
  edge
  [
    source 123
    target 4
    weight 0.782173963782645
  ]
  edge
  [
    source 118
    target 92
    weight 0.842373146475596
  ]
  edge
  [
    source 56
    target 35
    weight 0.757681127125262
  ]
  edge
  [
    source 9
    target 5
    weight 0.75978075013169
  ]
  edge
  [
    source 135
    target 43
    weight 0.779403287468663
  ]
  edge
  [
    source 48
    target 36
    weight 0.794755568338116
  ]
  edge
  [
    source 28
    target 17
    weight 0.789475455706059
  ]
  edge
  [
    source 30
    target 23
    weight 0.793904111983267
  ]
  edge
  [
    source 67
    target 8
    weight 0.811859671675671
  ]
  edge
  [
    source 72
    target 8
    weight 0.756141313119718
  ]
  edge
  [
    source 19
    target 16
    weight 0.774187382250146
  ]
  edge
  [
    source 25
    target 21
    weight 0.786350412766558
  ]
  edge
  [
    source 43
    target 41
    weight 0.80567792817594
  ]
  edge
  [
    source 38
    target 8
    weight 0.791365656284811
  ]
  edge
  [
    source 8
    target 5
    weight 0.759799106158578
  ]
  edge
  [
    source 49
    target 27
    weight 0.769353320650976
  ]
  edge
  [
    source 49
    target 15
    weight 0.782804642716216
  ]
  edge
  [
    source 49
    target 18
    weight 0.780481162678466
  ]
  edge
  [
    source 10
    target 7
    weight 0.818507848905745
  ]
  edge
  [
    source 123
    target 2
    weight 0.784631295181481
  ]
  edge
  [
    source 27
    target 21
    weight 0.761614262695006
  ]
  edge
  [
    source 52
    target 36
    weight 0.786792624190539
  ]
  edge
  [
    source 135
    target 17
    weight 0.782163616717122
  ]
  edge
  [
    source 49
    target 16
    weight 0.752780012642767
  ]
  edge
  [
    source 10
    target 5
    weight 0.75091129418356
  ]
  edge
  [
    source 43
    target 30
    weight 0.788573066089272
  ]
  edge
  [
    source 79
    target 66
    weight 0.750170699851611
  ]
  edge
  [
    source 93
    target 36
    weight 0.766492394820771
  ]
  edge
  [
    source 28
    target 15
    weight 0.783064224626779
  ]
  edge
  [
    source 131
    target 91
    weight 0.761011296992683
  ]
  edge
  [
    source 45
    target 42
    weight 0.781117790510583
  ]
  edge
  [
    source 72
    target 67
    weight 0.794051959013932
  ]
  edge
  [
    source 60
    target 5
    weight 0.782810090921622
  ]
  edge
  [
    source 13
    target 5
    weight 0.813590286539433
  ]
  edge
  [
    source 5
    target 4
    weight 0.780046608280368
  ]
  edge
  [
    source 5
    target 1
    weight 0.786421899791651
  ]
  edge
  [
    source 78
    target 27
    weight 0.799850672216961
  ]
  edge
  [
    source 60
    target 38
    weight 0.824059893310642
  ]
  edge
  [
    source 49
    target 46
    weight 0.782257762542343
  ]
  edge
  [
    source 78
    target 15
    weight 0.819558249297644
  ]
  edge
  [
    source 57
    target 49
    weight 0.789933587742189
  ]
  edge
  [
    source 83
    target 70
    weight 0.790355966756128
  ]
  edge
  [
    source 4
    target 1
    weight 0.804939399968173
  ]
  edge
  [
    source 71
    target 9
    weight 0.835522578158619
  ]
  edge
  [
    source 47
    target 20
    weight 0.778156863925363
  ]
  edge
  [
    source 121
    target 43
    weight 0.795041906569847
  ]
  edge
  [
    source 60
    target 3
    weight 0.822664136723443
  ]
  edge
  [
    source 38
    target 5
    weight 0.839624676419717
  ]
  edge
  [
    source 24
    target 17
    weight 0.758138826971863
  ]
  edge
  [
    source 47
    target 39
    weight 0.75352704991661
  ]
  edge
  [
    source 47
    target 17
    weight 0.788143394943046
  ]
  edge
  [
    source 71
    target 1
    weight 0.831274756955623
  ]
  edge
  [
    source 48
    target 32
    weight 0.788121842010135
  ]
  edge
  [
    source 129
    target 66
    weight 0.761683665004065
  ]
  edge
  [
    source 43
    target 40
    weight 0.806442708992548
  ]
  edge
  [
    source 17
    target 15
    weight 0.766270841072959
  ]
  edge
  [
    source 18
    target 17
    weight 0.773317375297584
  ]
  edge
  [
    source 21
    target 17
    weight 0.756747173470862
  ]
  edge
  [
    source 23
    target 14
    weight 0.793391044039692
  ]
  edge
  [
    source 49
    target 26
    weight 0.807342371913513
  ]
  edge
  [
    source 119
    target 71
    weight 0.795636686168481
  ]
  edge
  [
    source 67
    target 4
    weight 0.795534302056986
  ]
  edge
  [
    source 38
    target 10
    weight 0.776564239650002
  ]
  edge
  [
    source 39
    target 32
    weight 0.791196706012785
  ]
  edge
  [
    source 62
    target 52
    weight 0.788699309980997
  ]
  edge
  [
    source 29
    target 19
    weight 0.762988238118594
  ]
  edge
  [
    source 60
    target 6
    weight 0.758926272196534
  ]
  edge
  [
    source 53
    target 35
    weight 0.768685588013251
  ]
  edge
  [
    source 40
    target 17
    weight 0.770406098343529
  ]
  edge
  [
    source 46
    target 30
    weight 0.754166779413526
  ]
  edge
  [
    source 121
    target 94
    weight 0.808477062858184
  ]
  edge
  [
    source 10
    target 8
    weight 0.824736228080205
  ]
  edge
  [
    source 46
    target 29
    weight 0.778707394968837
  ]
  edge
  [
    source 84
    target 78
    weight 0.752534561860907
  ]
  edge
  [
    source 136
    target 95
    weight 0.856340696392855
  ]
  edge
  [
    source 52
    target 30
    weight 0.77709410697196
  ]
  edge
  [
    source 78
    target 16
    weight 0.78950755135166
  ]
  edge
  [
    source 12
    target 11
    weight 0.83864665872634
  ]
  edge
  [
    source 21
    target 19
    weight 0.766656488304278
  ]
  edge
  [
    source 86
    target 81
    weight 0.78396426728675
  ]
  edge
  [
    source 29
    target 17
    weight 0.818992174247553
  ]
  edge
  [
    source 12
    target 2
    weight 0.829589639348097
  ]
  edge
  [
    source 96
    target 47
    weight 0.789060815818175
  ]
  edge
  [
    source 94
    target 57
    weight 0.783783838404497
  ]
  edge
  [
    source 94
    target 17
    weight 0.821233235394041
  ]
  edge
  [
    source 43
    target 21
    weight 0.783203603950971
  ]
  edge
  [
    source 11
    target 6
    weight 0.83864665872634
  ]
  edge
  [
    source 99
    target 51
    weight 0.753633985540752
  ]
  edge
  [
    source 8
    target 4
    weight 0.815450777512029
  ]
  edge
  [
    source 45
    target 16
    weight 0.750599021030284
  ]
  edge
  [
    source 121
    target 42
    weight 0.798432924013581
  ]
  edge
  [
    source 27
    target 18
    weight 0.796382515834665
  ]
  edge
  [
    source 119
    target 38
    weight 0.775404513335927
  ]
  edge
  [
    source 121
    target 57
    weight 0.815063105513775
  ]
  edge
  [
    source 13
    target 3
    weight 0.807524820776422
  ]
  edge
  [
    source 71
    target 67
    weight 0.814717854448232
  ]
  edge
  [
    source 98
    target 61
    weight 0.799682556855305
  ]
  edge
  [
    source 57
    target 45
    weight 0.809327131081786
  ]
  edge
  [
    source 11
    target 2
    weight 0.81719401873889
  ]
  edge
  [
    source 27
    target 15
    weight 0.81708145574608
  ]
  edge
  [
    source 57
    target 17
    weight 0.761594163058804
  ]
  edge
  [
    source 72
    target 9
    weight 0.755475528905053
  ]
  edge
  [
    source 52
    target 39
    weight 0.772372729776125
  ]
  edge
  [
    source 26
    target 15
    weight 0.799155926849802
  ]
  edge
  [
    source 10
    target 1
    weight 0.816308347265455
  ]
  edge
  [
    source 39
    target 34
    weight 0.783732052960239
  ]
  edge
  [
    source 24
    target 23
    weight 0.816109787250899
  ]
  edge
  [
    source 43
    target 25
    weight 0.792471950505906
  ]
  edge
  [
    source 11
    target 5
    weight 0.76186715493597
  ]
  edge
  [
    source 42
    target 30
    weight 0.775702285264153
  ]
  edge
  [
    source 94
    target 43
    weight 0.77039078894477
  ]
  edge
  [
    source 137
    target 97
    weight 0.753344828352341
  ]
  edge
  [
    source 94
    target 27
    weight 0.771958675332774
  ]
  edge
  [
    source 61
    target 58
    weight 0.816979800153748
  ]
  edge
  [
    source 129
    target 98
    weight 0.761453674650257
  ]
  edge
  [
    source 27
    target 25
    weight 0.754501414368637
  ]
  edge
  [
    source 39
    target 33
    weight 0.775489863345304
  ]
  edge
  [
    source 125
    target 99
    weight 0.751911713673346
  ]
  edge
  [
    source 96
    target 20
    weight 0.764079524253952
  ]
  edge
  [
    source 27
    target 17
    weight 0.791754080740308
  ]
  edge
  [
    source 21
    target 18
    weight 0.753930044358406
  ]
  edge
  [
    source 49
    target 14
    weight 0.81736800564409
  ]
  edge
  [
    source 87
    target 52
    weight 0.800411282011467
  ]
  edge
  [
    source 40
    target 14
    weight 0.791294894523959
  ]
  edge
  [
    source 12
    target 3
    weight 0.761526333830641
  ]
  edge
  [
    source 58
    target 42
    weight 0.769352251700662
  ]
  edge
  [
    source 121
    target 46
    weight 0.757362342648222
  ]
  edge
  [
    source 125
    target 64
    weight 0.80192140877584
  ]
  edge
  [
    source 43
    target 27
    weight 0.785022606860647
  ]
  edge
  [
    source 52
    target 42
    weight 0.779178532380075
  ]
  edge
  [
    source 38
    target 1
    weight 0.793177719386076
  ]
  edge
  [
    source 37
    target 35
    weight 0.783544021630165
  ]
  edge
  [
    source 47
    target 27
    weight 0.810836579176904
  ]
  edge
  [
    source 38
    target 13
    weight 0.816475608563916
  ]
  edge
  [
    source 46
    target 14
    weight 0.768116814566902
  ]
  edge
  [
    source 43
    target 15
    weight 0.785247745495784
  ]
  edge
  [
    source 4
    target 2
    weight 0.822963732496314
  ]
  edge
  [
    source 11
    target 1
    weight 0.827082268490125
  ]
  edge
  [
    source 49
    target 17
    weight 0.801198164697281
  ]
  edge
  [
    source 135
    target 49
    weight 0.77462003498717
  ]
  edge
  [
    source 7
    target 5
    weight 0.760142574996296
  ]
  edge
  [
    source 47
    target 15
    weight 0.775648250505883
  ]
  edge
  [
    source 21
    target 14
    weight 0.772261922036827
  ]
  edge
  [
    source 43
    target 20
    weight 0.7701914984466
  ]
  edge
  [
    source 48
    target 37
    weight 0.750131883772699
  ]
  edge
  [
    source 67
    target 7
    weight 0.823941120908456
  ]
  edge
  [
    source 13
    target 4
    weight 0.75060659507834
  ]
  edge
  [
    source 43
    target 14
    weight 0.810952863831296
  ]
  edge
  [
    source 69
    target 56
    weight 0.791335475190571
  ]
  edge
  [
    source 43
    target 23
    weight 0.787603085030775
  ]
  edge
  [
    source 49
    target 40
    weight 0.762083757803958
  ]
  edge
  [
    source 83
    target 61
    weight 0.751988570823382
  ]
  edge
  [
    source 75
    target 55
    weight 0.77514106418987
  ]
  edge
  [
    source 9
    target 1
    weight 0.832555577869088
  ]
  edge
  [
    source 92
    target 0
    weight 0.813559128802058
  ]
  edge
  [
    source 30
    target 17
    weight 0.752257371529217
  ]
  edge
  [
    source 123
    target 60
    weight 0.80676630111033
  ]
  edge
  [
    source 63
    target 62
    weight 0.798248668138182
  ]
  edge
  [
    source 25
    target 17
    weight 0.841064334439475
  ]
  edge
  [
    source 38
    target 4
    weight 0.796386154050849
  ]
  edge
  [
    source 40
    target 31
    weight 0.762737911738324
  ]
  edge
  [
    source 132
    target 70
    weight 0.783090216691847
  ]
  edge
  [
    source 20
    target 17
    weight 0.778885436861849
  ]
  edge
  [
    source 47
    target 28
    weight 0.775964428637321
  ]
  edge
  [
    source 76
    target 58
    weight 0.773343424590306
  ]
  edge
  [
    source 99
    target 80
    weight 0.753904130314449
  ]
  edge
  [
    source 49
    target 20
    weight 0.800776472933292
  ]
  edge
  [
    source 71
    target 7
    weight 0.828556657297498
  ]
  edge
  [
    source 100
    target 63
    weight 0.760588940182564
  ]
  edge
  [
    source 12
    target 8
    weight 0.779691866432109
  ]
  edge
  [
    source 18
    target 14
    weight 0.779465402543149
  ]
  edge
  [
    source 72
    target 1
    weight 0.756292790605805
  ]
  edge
  [
    source 78
    target 14
    weight 0.750417971884685
  ]
  edge
  [
    source 7
    target 1
    weight 0.826782458576247
  ]
  edge
  [
    source 25
    target 14
    weight 0.750299979235777
  ]
  edge
  [
    source 24
    target 14
    weight 0.752506445152938
  ]
  edge
  [
    source 47
    target 43
    weight 0.777351858816784
  ]
  edge
  [
    source 71
    target 8
    weight 0.828426016515901
  ]
  edge
  [
    source 8
    target 1
    weight 0.832555577869088
  ]
  edge
  [
    source 122
    target 56
    weight 0.774904210180709
  ]
  edge
  [
    source 79
    target 26
    weight 0.762411280189599
  ]
  edge
  [
    source 49
    target 47
    weight 0.759967695467405
  ]
  edge
  [
    source 42
    target 31
    weight 0.799381537464997
  ]
  edge
  [
    source 70
    target 61
    weight 0.774247746181763
  ]
  edge
  [
    source 110
    target 28
    weight 1
  ]
  edge
  [
    source 101
    target 14
    weight 1
  ]
  edge
  [
    source 101
    target 88
    weight 1
  ]
  edge
  [
    source 102
    target 14
    weight 1
  ]
  edge
  [
    source 102
    target 101
    weight 1
  ]
  edge
  [
    source 87
    target 28
    weight 1
  ]
  edge
  [
    source 87
    target 34
    weight 1
  ]
  edge
  [
    source 102
    target 87
    weight 1
  ]
  edge
  [
    source 93
    target 14
    weight 1
  ]
  edge
  [
    source 103
    target 93
    weight 1
  ]
  edge
  [
    source 102
    target 93
    weight 1
  ]
  edge
  [
    source 87
    target 1
    weight 1
  ]
  edge
  [
    source 102
    target 91
    weight 1
  ]
  edge
  [
    source 101
    target 91
    weight 1
  ]
  edge
  [
    source 104
    target 91
    weight 1
  ]
  edge
  [
    source 101
    target 15
    weight 1
  ]
  edge
  [
    source 102
    target 15
    weight 1
  ]
  edge
  [
    source 20
    target 1
    weight 1
  ]
  edge
  [
    source 101
    target 20
    weight 1
  ]
  edge
  [
    source 105
    target 20
    weight 1
  ]
  edge
  [
    source 106
    target 20
    weight 1
  ]
  edge
  [
    source 107
    target 20
    weight 1
  ]
  edge
  [
    source 115
    target 107
    weight 1
  ]
  edge
  [
    source 115
    target 101
    weight 1
  ]
  edge
  [
    source 115
    target 15
    weight 1
  ]
  edge
  [
    source 100
    target 88
    weight 1
  ]
  edge
  [
    source 100
    target 87
    weight 1
  ]
  edge
  [
    source 37
    target 15
    weight 1
  ]
  edge
  [
    source 114
    target 91
    weight 1
  ]
  edge
  [
    source 84
    target 14
    weight 1
  ]
  edge
  [
    source 138
    target 107
    weight 1
  ]
  edge
  [
    source 139
    target 102
    weight 1
  ]
  edge
  [
    source 108
    target 22
    weight 1
  ]
  edge
  [
    source 39
    target 10
    weight 1
  ]
  edge
  [
    source 93
    target 3
    weight 1
  ]
  edge
  [
    source 109
    target 51
    weight 1
  ]
  edge
  [
    source 110
    target 51
    weight 1
  ]
  edge
  [
    source 101
    target 51
    weight 1
  ]
  edge
  [
    source 111
    target 64
    weight 1
  ]
  edge
  [
    source 140
    target 93
    weight 1
  ]
  edge
  [
    source 112
    target 53
    weight 1
  ]
  edge
  [
    source 53
    target 14
    weight 1
  ]
  edge
  [
    source 102
    target 53
    weight 1
  ]
  edge
  [
    source 24
    target 15
    weight 1
  ]
  edge
  [
    source 133
    target 113
    weight 1
  ]
  edge
  [
    source 133
    target 15
    weight 1
  ]
  edge
  [
    source 133
    target 87
    weight 1
  ]
  edge
  [
    source 133
    target 102
    weight 1
  ]
  edge
  [
    source 133
    target 91
    weight 1
  ]
  edge
  [
    source 101
    target 57
    weight 1
  ]
  edge
  [
    source 57
    target 20
    weight 1
  ]
  edge
  [
    source 102
    target 57
    weight 1
  ]
  edge
  [
    source 91
    target 57
    weight 1
  ]
  edge
  [
    source 104
    target 57
    weight 1
  ]
  edge
  [
    source 57
    target 15
    weight 1
  ]
  edge
  [
    source 88
    target 57
    weight 1
  ]
  edge
  [
    source 141
    target 114
    weight 1
  ]
  edge
  [
    source 94
    target 20
    weight 1
  ]
  edge
  [
    source 51
    target 12
    weight 1
  ]
  edge
  [
    source 20
    target 12
    weight 1
  ]
  edge
  [
    source 45
    target 35
    weight 1
  ]
  edge
  [
    source 35
    target 15
    weight 1
  ]
  edge
  [
    source 142
    target 101
    weight 1
  ]
  edge
  [
    source 130
    target 20
    weight 1
  ]
  edge
  [
    source 143
    target 113
    weight 1
  ]
  edge
  [
    source 143
    target 15
    weight 1
  ]
  edge
  [
    source 143
    target 32
    weight 1
  ]
  edge
  [
    source 143
    target 115
    weight 1
  ]
  edge
  [
    source 143
    target 1
    weight 1
  ]
  edge
  [
    source 143
    target 116
    weight 1
  ]
  edge
  [
    source 143
    target 12
    weight 1
  ]
  edge
  [
    source 144
    target 91
    weight 1
  ]
  edge
  [
    source 145
    target 87
    weight 1
  ]
  edge
  [
    source 123
    target 115
    weight 1
  ]
  edge
  [
    source 123
    target 51
    weight 1
  ]
  edge
  [
    source 122
    target 48
    weight 1
  ]
  edge
  [
    source 122
    target 84
    weight 1
  ]
  edge
  [
    source 146
    target 57
    weight 1
  ]
  edge
  [
    source 146
    target 28
    weight 1
  ]
  edge
  [
    source 146
    target 93
    weight 1
  ]
  edge
  [
    source 117
    target 93
    weight 1
  ]
  edge
  [
    source 93
    target 27
    weight 1
  ]
  edge
  [
    source 32
    target 14
    weight 1
  ]
  edge
  [
    source 15
    target 14
    weight 1
  ]
  edge
  [
    source 115
    target 109
    weight 1
  ]
  edge
  [
    source 100
    target 91
    weight 1
  ]
  edge
  [
    source 37
    target 27
    weight 1
  ]
  edge
  [
    source 100
    target 36
    weight 1
  ]
  edge
  [
    source 147
    target 87
    weight 1
  ]
  edge
  [
    source 147
    target 39
    weight 1
  ]
  edge
  [
    source 59
    target 22
    weight 1
  ]
  edge
  [
    source 51
    target 15
    weight 1
  ]
  edge
  [
    source 133
    target 107
    weight 1
  ]
  edge
  [
    source 57
    target 14
    weight 1
  ]
  edge
  [
    source 94
    target 87
    weight 1
  ]
  edge
  [
    source 87
    target 12
    weight 1
  ]
  edge
  [
    source 101
    target 30
    weight 1
  ]
  edge
  [
    source 36
    target 30
    weight 1
  ]
  edge
  [
    source 148
    target 28
    weight 1
  ]
]
