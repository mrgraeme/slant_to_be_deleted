Creator "igraph version @VERSION@ Fri Jun  2 12:22:48 2017"
Version 1
graph
[
  directed 0
  node
  [
    id 0
    name "WBGene00001974"
  ]
  node
  [
    id 1
    name "WBGene00007028"
  ]
  node
  [
    id 2
    name "WBGene00001973"
  ]
  node
  [
    id 3
    name "WBGene00001865"
  ]
  node
  [
    id 4
    name "WBGene00010785"
  ]
  node
  [
    id 5
    name "WBGene00006542"
  ]
  node
  [
    id 6
    name "WBGene00000226"
  ]
  node
  [
    id 7
    name "WBGene00012361"
  ]
  node
  [
    id 8
    name "WBGene00004447"
  ]
  node
  [
    id 9
    name "WBGene00007042"
  ]
  node
  [
    id 10
    name "WBGene00012887"
  ]
  node
  [
    id 11
    name "WBGene00017435"
  ]
  node
  [
    id 12
    name "WBGene00006387"
  ]
  node
  [
    id 13
    name "WBGene00022182"
  ]
  node
  [
    id 14
    name "WBGene00000406"
  ]
  node
  [
    id 15
    name "WBGene00017313"
  ]
  node
  [
    id 16
    name "WBGene00004700"
  ]
  node
  [
    id 17
    name "WBGene00000187"
  ]
  node
  [
    id 18
    name "WBGene00004274"
  ]
  node
  [
    id 19
    name "WBGene00010964"
  ]
  node
  [
    id 20
    name "WBGene00019983"
  ]
  node
  [
    id 21
    name "WBGene00001837"
  ]
  node
  [
    id 22
    name "WBGene00016622"
  ]
  node
  [
    id 23
    name "WBGene00009966"
  ]
  node
  [
    id 24
    name "WBGene00007027"
  ]
  node
  [
    id 25
    name "WBGene00001827"
  ]
  node
  [
    id 26
    name "WBGene00015525"
  ]
  node
  [
    id 27
    name "WBGene00001423"
  ]
  node
  [
    id 28
    name "WBGene00006393"
  ]
  node
  [
    id 29
    name "WBGene00001920"
  ]
  node
  [
    id 30
    name "WBGene00007030"
  ]
  node
  [
    id 31
    name "WBGene00006391"
  ]
  node
  [
    id 32
    name "WBGene00011814"
  ]
  node
  [
    id 33
    name "WBGene00012187"
  ]
  node
  [
    id 34
    name "WBGene00001851"
  ]
  node
  [
    id 35
    name "WBGene00004042"
  ]
  node
  [
    id 36
    name "WBGene00005014"
  ]
  node
  [
    id 37
    name "WBGene00000506"
  ]
  node
  [
    id 38
    name "WBGene00006579"
  ]
  node
  [
    id 39
    name "WBGene00006382"
  ]
  node
  [
    id 40
    name "WBGene00001101"
  ]
  node
  [
    id 41
    name "WBGene00000413"
  ]
  node
  [
    id 42
    name "WBGene00004101"
  ]
  node
  [
    id 43
    name "WBGene00004385"
  ]
  node
  [
    id 44
    name "WBGene00004414"
  ]
  node
  [
    id 45
    name "WBGene00002046"
  ]
  node
  [
    id 46
    name "WBGene00020110"
  ]
  node
  [
    id 47
    name "WBGene00002010"
  ]
  node
  [
    id 48
    name "WBGene00004807"
  ]
  node
  [
    id 49
    name "WBGene00003001"
  ]
  node
  [
    id 50
    name "WBGene00004270"
  ]
  node
  [
    id 51
    name "WBGene00000911"
  ]
  node
  [
    id 52
    name "WBGene00002072"
  ]
  node
  [
    id 53
    name "WBGene00000202"
  ]
  node
  [
    id 54
    name "WBGene00007694"
  ]
  node
  [
    id 55
    name "WBGene00006389"
  ]
  node
  [
    id 56
    name "WBGene00001225"
  ]
  node
  [
    id 57
    name "WBGene00006396"
  ]
  node
  [
    id 58
    name "WBGene00006817"
  ]
  node
  [
    id 59
    name "WBGene00009542"
  ]
  node
  [
    id 60
    name "WBGene00001102"
  ]
  node
  [
    id 61
    name "WBGene00000794"
  ]
  node
  [
    id 62
    name "WBGene00006577"
  ]
  node
  [
    id 63
    name "WBGene00002717"
  ]
  node
  [
    id 64
    name "WBGene00003241"
  ]
  node
  [
    id 65
    name "WBGene00002324"
  ]
  node
  [
    id 66
    name "WBGene00000156"
  ]
  node
  [
    id 67
    name "WBGene00000099"
  ]
  node
  [
    id 68
    name "WBGene00008140"
  ]
  node
  [
    id 69
    name "WBGene00002348"
  ]
  node
  [
    id 70
    name "WBGene00006392"
  ]
  node
  [
    id 71
    name "WBGene00004419"
  ]
  node
  [
    id 72
    name "WBGene00006931"
  ]
  node
  [
    id 73
    name "WBGene00001237"
  ]
  node
  [
    id 74
    name "WBGene00004481"
  ]
  node
  [
    id 75
    name "WBGene00018909"
  ]
  node
  [
    id 76
    name "WBGene00006388"
  ]
  node
  [
    id 77
    name "WBGene00002363"
  ]
  node
  [
    id 78
    name "WBGene00004917"
  ]
  node
  [
    id 79
    name "WBGene00004920"
  ]
  node
  [
    id 80
    name "WBGene00000275"
  ]
  node
  [
    id 81
    name "WBGene00001337"
  ]
  node
  [
    id 82
    name "WBGene00004752"
  ]
  node
  [
    id 83
    name "WBGene00000839"
  ]
  node
  [
    id 84
    name "WBGene00004421"
  ]
  node
  [
    id 85
    name "WBGene00001609"
  ]
  node
  [
    id 86
    name "WBGene00008688"
  ]
  node
  [
    id 87
    name "WBGene00002191"
  ]
  node
  [
    id 88
    name "WBGene00001502"
  ]
  node
  [
    id 89
    name "WBGene00004435"
  ]
  node
  [
    id 90
    name "WBGene00001016"
  ]
  node
  [
    id 91
    name "WBGene00012348"
  ]
  node
  [
    id 92
    name "WBGene00003119"
  ]
  node
  [
    id 93
    name "WBGene00003073"
  ]
  node
  [
    id 94
    name "WBGene00000405"
  ]
  node
  [
    id 95
    name "WBGene00001001"
  ]
  node
  [
    id 96
    name "WBGene00011067"
  ]
  node
  [
    id 97
    name "WBGene00019120"
  ]
  node
  [
    id 98
    name "WBGene00000145"
  ]
  node
  [
    id 99
    name "WBGene00004320"
  ]
  node
  [
    id 100
    name "WBGene00007201"
  ]
  node
  [
    id 101
    name "WBGene00002076"
  ]
  node
  [
    id 102
    name "WBGene00006707"
  ]
  node
  [
    id 103
    name "WBGene00000294"
  ]
  node
  [
    id 104
    name "WBGene00019004"
  ]
  node
  [
    id 105
    name "WBGene00000201"
  ]
  node
  [
    id 106
    name "WBGene00009368"
  ]
  node
  [
    id 107
    name "WBGene00000371"
  ]
  node
  [
    id 108
    name "WBGene00007355"
  ]
  node
  [
    id 109
    name "WBGene00000898"
  ]
  node
  [
    id 110
    name "WBGene00002335"
  ]
  node
  [
    id 111
    name "WBGene00003186"
  ]
  node
  [
    id 112
    name "WBGene00001500"
  ]
  node
  [
    id 113
    name "WBGene00001226"
  ]
  node
  [
    id 114
    name "WBGene00000386"
  ]
  node
  [
    id 115
    name "WBGene00000772"
  ]
  node
  [
    id 116
    name "WBGene00000147"
  ]
  node
  [
    id 117
    name "WBGene00001972"
  ]
  node
  [
    id 118
    name "WBGene00002881"
  ]
  node
  [
    id 119
    name "WBGene00000301"
  ]
  node
  [
    id 120
    name "WBGene00003918"
  ]
  node
  [
    id 121
    name "WBGene00004194"
  ]
  node
  [
    id 122
    name "WBGene00003395"
  ]
  node
  [
    id 123
    name "WBGene00003930"
  ]
  node
  [
    id 124
    name "WBGene00004860"
  ]
  node
  [
    id 125
    name "WBGene00001516"
  ]
  node
  [
    id 126
    name "WBGene00002996"
  ]
  node
  [
    id 127
    name "WBGene00022854"
  ]
  node
  [
    id 128
    name "WBGene00022373"
  ]
  node
  [
    id 129
    name "WBGene00022219"
  ]
  node
  [
    id 130
    name "WBGene00022021"
  ]
  node
  [
    id 131
    name "WBGene00021363"
  ]
  node
  [
    id 132
    name "WBGene00021097"
  ]
  node
  [
    id 133
    name "WBGene00020480"
  ]
  node
  [
    id 134
    name "WBGene00019820"
  ]
  node
  [
    id 135
    name "WBGene00022170"
  ]
  node
  [
    id 136
    name "WBGene00018866"
  ]
  node
  [
    id 137
    name "WBGene00018319"
  ]
  node
  [
    id 138
    name "WBGene00018625"
  ]
  node
  [
    id 139
    name "WBGene00016603"
  ]
  node
  [
    id 140
    name "WBGene00004483"
  ]
  node
  [
    id 141
    name "WBGene00006394"
  ]
  node
  [
    id 142
    name "WBGene00022774"
  ]
  node
  [
    id 143
    name "WBGene00013529"
  ]
  node
  [
    id 144
    name "WBGene00012999"
  ]
  node
  [
    id 145
    name "WBGene00012792"
  ]
  node
  [
    id 146
    name "WBGene00012694"
  ]
  node
  [
    id 147
    name "WBGene00012650"
  ]
  node
  [
    id 148
    name "WBGene00010562"
  ]
  node
  [
    id 149
    name "WBGene00011758"
  ]
  node
  [
    id 150
    name "WBGene00007008"
  ]
  node
  [
    id 151
    name "WBGene00012730"
  ]
  node
  [
    id 152
    name "WBGene00006938"
  ]
  node
  [
    id 153
    name "WBGene00007009"
  ]
  node
  [
    id 154
    name "WBGene00011775"
  ]
  node
  [
    id 155
    name "WBGene00009956"
  ]
  node
  [
    id 156
    name "WBGene00022232"
  ]
  node
  [
    id 157
    name "WBGene00010056"
  ]
  node
  [
    id 158
    name "WBGene00021857"
  ]
  node
  [
    id 159
    name "WBGene00007277"
  ]
  node
  [
    id 160
    name "WBGene00006993"
  ]
  node
  [
    id 161
    name "WBGene00006829"
  ]
  node
  [
    id 162
    name "WBGene00006789"
  ]
  node
  [
    id 163
    name "WBGene00006595"
  ]
  node
  [
    id 164
    name "WBGene00004431"
  ]
  node
  [
    id 165
    name "WBGene00014234"
  ]
  node
  [
    id 166
    name "WBGene00077680"
  ]
  node
  [
    id 167
    name "WBGene00004496"
  ]
  node
  [
    id 168
    name "WBGene00004753"
  ]
  node
  [
    id 169
    name "WBGene00011043"
  ]
  node
  [
    id 170
    name "WBGene00000366"
  ]
  node
  [
    id 171
    name "WBGene00013768"
  ]
  node
  [
    id 172
    name "WBGene00004440"
  ]
  node
  [
    id 173
    name "WBGene00022193"
  ]
  node
  [
    id 174
    name "WBGene00004679"
  ]
  node
  [
    id 175
    name "WBGene00016582"
  ]
  node
  [
    id 176
    name "WBGene00020442"
  ]
  node
  [
    id 177
    name "WBGene00019121"
  ]
  node
  [
    id 178
    name "WBGene00003803"
  ]
  node
  [
    id 179
    name "WBGene00001051"
  ]
  node
  [
    id 180
    name "WBGene00000203"
  ]
  node
  [
    id 181
    name "WBGene00006519"
  ]
  node
  [
    id 182
    name "WBGene00001501"
  ]
  node
  [
    id 183
    name "WBGene00001998"
  ]
  node
  [
    id 184
    name "WBGene00003024"
  ]
  node
  [
    id 185
    name "WBGene00003378"
  ]
  node
  [
    id 186
    name "WBGene00003605"
  ]
  node
  [
    id 187
    name "WBGene00003795"
  ]
  node
  [
    id 188
    name "WBGene00004728"
  ]
  node
  [
    id 189
    name "WBGene00004769"
  ]
  node
  [
    id 190
    name "WBGene00004947"
  ]
  node
  [
    id 191
    name "WBGene00006736"
  ]
  node
  [
    id 192
    name "WBGene00006805"
  ]
  node
  [
    id 193
    name "WBGene00008119"
  ]
  node
  [
    id 194
    name "WBGene00012193"
  ]
  node
  [
    id 195
    name "WBGene00015104"
  ]
  node
  [
    id 196
    name "WBGene00020636"
  ]
  node
  [
    id 197
    name "WBGene00022027"
  ]
  node
  [
    id 198
    name "WBGene00022765"
  ]
  node
  [
    id 199
    name "WBGene00004856"
  ]
  node
  [
    id 200
    name "WBGene00018149"
  ]
  edge
  [
    source 127
    target 0
    weight 0.763114326616623
  ]
  edge
  [
    source 127
    target 1
    weight 0.758141481510878
  ]
  edge
  [
    source 127
    target 2
    weight 0.763114326616623
  ]
  edge
  [
    source 127
    target 3
    weight 0.766678457648893
  ]
  edge
  [
    source 127
    target 4
    weight 0.756051191787723
  ]
  edge
  [
    source 127
    target 5
    weight 0.770013013268268
  ]
  edge
  [
    source 127
    target 6
    weight 0.77197453037425
  ]
  edge
  [
    source 128
    target 7
    weight 0.755595108946005
  ]
  edge
  [
    source 129
    target 8
    weight 0.759333420436958
  ]
  edge
  [
    source 13
    target 9
    weight 0.764301691341129
  ]
  edge
  [
    source 130
    target 10
    weight 0.754944683424051
  ]
  edge
  [
    source 130
    target 11
    weight 0.758931798122643
  ]
  edge
  [
    source 131
    target 12
    weight 0.796916457411614
  ]
  edge
  [
    source 127
    target 13
    weight 0.760312379976875
  ]
  edge
  [
    source 132
    target 14
    weight 0.766810321973088
  ]
  edge
  [
    source 133
    target 15
    weight 0.75907386699045
  ]
  edge
  [
    source 20
    target 16
    weight 0.750417313396484
  ]
  edge
  [
    source 134
    target 17
    weight 0.809795023351004
  ]
  edge
  [
    source 132
    target 18
    weight 0.756287379614208
  ]
  edge
  [
    source 135
    target 19
    weight 0.843437467495874
  ]
  edge
  [
    source 136
    target 10
    weight 0.774457216287784
  ]
  edge
  [
    source 132
    target 20
    weight 0.766238691209323
  ]
  edge
  [
    source 137
    target 21
    weight 0.79969620122275
  ]
  edge
  [
    source 137
    target 22
    weight 0.75846801366694
  ]
  edge
  [
    source 138
    target 23
    weight 0.751021192223636
  ]
  edge
  [
    source 11
    target 10
    weight 0.766801152526682
  ]
  edge
  [
    source 22
    target 0
    weight 0.752060795532071
  ]
  edge
  [
    source 139
    target 0
    weight 0.759139937960031
  ]
  edge
  [
    source 139
    target 24
    weight 0.784723057425829
  ]
  edge
  [
    source 139
    target 2
    weight 0.758956534544325
  ]
  edge
  [
    source 139
    target 25
    weight 0.755203441066747
  ]
  edge
  [
    source 136
    target 26
    weight 0.771122041480336
  ]
  edge
  [
    source 140
    target 27
    weight 0.7538703109559
  ]
  edge
  [
    source 141
    target 28
    weight 0.845853349492037
  ]
  edge
  [
    source 62
    target 29
    weight 0.755004266146302
  ]
  edge
  [
    source 142
    target 30
    weight 0.777543261530758
  ]
  edge
  [
    source 70
    target 31
    weight 0.77769071099547
  ]
  edge
  [
    source 143
    target 32
    weight 0.759291694703724
  ]
  edge
  [
    source 144
    target 33
    weight 0.752911731662808
  ]
  edge
  [
    source 48
    target 34
    weight 0.750658370641761
  ]
  edge
  [
    source 145
    target 35
    weight 0.754477883338747
  ]
  edge
  [
    source 146
    target 36
    weight 0.761067528627417
  ]
  edge
  [
    source 146
    target 24
    weight 0.759862971589831
  ]
  edge
  [
    source 146
    target 37
    weight 0.766760034590083
  ]
  edge
  [
    source 147
    target 38
    weight 0.761258456136829
  ]
  edge
  [
    source 146
    target 39
    weight 0.750386989446181
  ]
  edge
  [
    source 64
    target 40
    weight 0.75353357697995
  ]
  edge
  [
    source 41
    target 38
    weight 0.753580044100135
  ]
  edge
  [
    source 57
    target 39
    weight 0.769333512056661
  ]
  edge
  [
    source 148
    target 42
    weight 0.79647030122982
  ]
  edge
  [
    source 149
    target 43
    weight 0.772670527325751
  ]
  edge
  [
    source 150
    target 29
    weight 0.752907991134089
  ]
  edge
  [
    source 84
    target 44
    weight 0.752762318279483
  ]
  edge
  [
    source 58
    target 41
    weight 0.757118071244713
  ]
  edge
  [
    source 151
    target 45
    weight 0.753826503261284
  ]
  edge
  [
    source 147
    target 35
    weight 0.760032565780142
  ]
  edge
  [
    source 22
    target 2
    weight 0.752060795532071
  ]
  edge
  [
    source 132
    target 46
    weight 0.782404446451649
  ]
  edge
  [
    source 152
    target 47
    weight 0.763961830419961
  ]
  edge
  [
    source 4
    target 3
    weight 0.754236380835456
  ]
  edge
  [
    source 4
    target 2
    weight 0.777453336105525
  ]
  edge
  [
    source 48
    target 20
    weight 0.755178454184794
  ]
  edge
  [
    source 153
    target 49
    weight 0.781089536377303
  ]
  edge
  [
    source 154
    target 50
    weight 0.756291867219256
  ]
  edge
  [
    source 77
    target 51
    weight 0.754669886663563
  ]
  edge
  [
    source 43
    target 23
    weight 0.75321651853031
  ]
  edge
  [
    source 155
    target 52
    weight 0.852266042058143
  ]
  edge
  [
    source 156
    target 53
    weight 0.754102639837203
  ]
  edge
  [
    source 157
    target 54
    weight 0.851268137849961
  ]
  edge
  [
    source 4
    target 0
    weight 0.777453336105525
  ]
  edge
  [
    source 55
    target 31
    weight 0.756004035571063
  ]
  edge
  [
    source 68
    target 6
    weight 0.774092153068569
  ]
  edge
  [
    source 68
    target 3
    weight 0.799412733575275
  ]
  edge
  [
    source 158
    target 56
    weight 0.766602491825191
  ]
  edge
  [
    source 147
    target 41
    weight 0.794441749719758
  ]
  edge
  [
    source 26
    target 11
    weight 0.777098241040469
  ]
  edge
  [
    source 62
    target 57
    weight 0.760115845535163
  ]
  edge
  [
    source 108
    target 36
    weight 0.751426022635779
  ]
  edge
  [
    source 159
    target 58
    weight 0.842045235104362
  ]
  edge
  [
    source 154
    target 59
    weight 0.776739174348443
  ]
  edge
  [
    source 100
    target 53
    weight 0.853742384930207
  ]
  edge
  [
    source 77
    target 60
    weight 0.759245543469876
  ]
  edge
  [
    source 68
    target 61
    weight 0.758213457825735
  ]
  edge
  [
    source 108
    target 62
    weight 0.773269375046032
  ]
  edge
  [
    source 63
    target 9
    weight 0.792169397921689
  ]
  edge
  [
    source 58
    target 1
    weight 0.750015160680255
  ]
  edge
  [
    source 29
    target 1
    weight 0.759784854338549
  ]
  edge
  [
    source 24
    target 1
    weight 0.762678187645079
  ]
  edge
  [
    source 153
    target 60
    weight 0.769788175790092
  ]
  edge
  [
    source 150
    target 52
    weight 0.792984292175583
  ]
  edge
  [
    source 152
    target 14
    weight 0.780567793418068
  ]
  edge
  [
    source 160
    target 64
    weight 0.763702002368216
  ]
  edge
  [
    source 69
    target 65
    weight 0.766476297250475
  ]
  edge
  [
    source 69
    target 3
    weight 0.753322992624609
  ]
  edge
  [
    source 161
    target 66
    weight 0.76307762338474
  ]
  edge
  [
    source 67
    target 58
    weight 0.779144204013319
  ]
  edge
  [
    source 68
    target 4
    weight 0.782290656374863
  ]
  edge
  [
    source 162
    target 69
    weight 0.803724548670244
  ]
  edge
  [
    source 70
    target 57
    weight 0.775313416732345
  ]
  edge
  [
    source 163
    target 61
    weight 0.750233806399436
  ]
  edge
  [
    source 164
    target 71
    weight 0.762714381201448
  ]
  edge
  [
    source 57
    target 29
    weight 0.751753747622921
  ]
  edge
  [
    source 57
    target 31
    weight 0.755102687773647
  ]
  edge
  [
    source 165
    target 72
    weight 0.850408455459388
  ]
  edge
  [
    source 166
    target 73
    weight 0.841675359043247
  ]
  edge
  [
    source 167
    target 74
    weight 0.82280507579579
  ]
  edge
  [
    source 104
    target 75
    weight 0.762835448276183
  ]
  edge
  [
    source 153
    target 67
    weight 0.762736082904474
  ]
  edge
  [
    source 76
    target 55
    weight 0.836114552951623
  ]
  edge
  [
    source 39
    target 29
    weight 0.753034061716213
  ]
  edge
  [
    source 153
    target 77
    weight 0.752426971257959
  ]
  edge
  [
    source 37
    target 36
    weight 0.756972118590723
  ]
  edge
  [
    source 57
    target 12
    weight 0.753092801375814
  ]
  edge
  [
    source 145
    target 67
    weight 0.800965734948807
  ]
  edge
  [
    source 79
    target 78
    weight 0.769355134296662
  ]
  edge
  [
    source 127
    target 24
    weight 0.753701559439761
  ]
  edge
  [
    source 138
    target 79
    weight 0.75921408353379
  ]
  edge
  [
    source 147
    target 80
    weight 0.756970711793473
  ]
  edge
  [
    source 88
    target 81
    weight 0.786523432166506
  ]
  edge
  [
    source 147
    target 67
    weight 0.769087486789737
  ]
  edge
  [
    source 168
    target 82
    weight 0.762617974343242
  ]
  edge
  [
    source 93
    target 81
    weight 0.757929051985905
  ]
  edge
  [
    source 83
    target 48
    weight 0.780774226013785
  ]
  edge
  [
    source 89
    target 84
    weight 0.777787051001947
  ]
  edge
  [
    source 85
    target 49
    weight 0.776721596139325
  ]
  edge
  [
    source 169
    target 86
    weight 0.75848720399065
  ]
  edge
  [
    source 87
    target 65
    weight 0.820062972833306
  ]
  edge
  [
    source 88
    target 34
    weight 0.752118544878581
  ]
  edge
  [
    source 170
    target 66
    weight 0.773933440591723
  ]
  edge
  [
    source 140
    target 84
    weight 0.778214222684803
  ]
  edge
  [
    source 171
    target 41
    weight 0.794464353237798
  ]
  edge
  [
    source 146
    target 62
    weight 0.761538193184383
  ]
  edge
  [
    source 172
    target 89
    weight 0.752814322488542
  ]
  edge
  [
    source 90
    target 3
    weight 0.752509584088622
  ]
  edge
  [
    source 164
    target 44
    weight 0.804070150029521
  ]
  edge
  [
    source 84
    target 71
    weight 0.750682455937631
  ]
  edge
  [
    source 173
    target 91
    weight 0.794217004634924
  ]
  edge
  [
    source 169
    target 92
    weight 0.770758200428281
  ]
  edge
  [
    source 174
    target 93
    weight 0.843792642852574
  ]
  edge
  [
    source 29
    target 24
    weight 0.758116129245556
  ]
  edge
  [
    source 62
    target 39
    weight 0.779054096568388
  ]
  edge
  [
    source 108
    target 39
    weight 0.763736486696625
  ]
  edge
  [
    source 99
    target 83
    weight 0.771762650774458
  ]
  edge
  [
    source 94
    target 69
    weight 0.771239506854944
  ]
  edge
  [
    source 50
    target 17
    weight 0.768731465632137
  ]
  edge
  [
    source 100
    target 95
    weight 0.862820757724196
  ]
  edge
  [
    source 6
    target 3
    weight 0.788502036114825
  ]
  edge
  [
    source 132
    target 48
    weight 0.760034853929471
  ]
  edge
  [
    source 175
    target 96
    weight 0.760193107131801
  ]
  edge
  [
    source 89
    target 71
    weight 0.783226888239492
  ]
  edge
  [
    source 176
    target 90
    weight 0.771151671107646
  ]
  edge
  [
    source 130
    target 26
    weight 0.769297799336571
  ]
  edge
  [
    source 177
    target 97
    weight 0.853742384930207
  ]
  edge
  [
    source 98
    target 48
    weight 0.763240896281339
  ]
  edge
  [
    source 99
    target 48
    weight 0.762259377355561
  ]
  edge
  [
    source 156
    target 100
    weight 0.756314746319413
  ]
  edge
  [
    source 178
    target 101
    weight 0.79139915813742
  ]
  edge
  [
    source 148
    target 102
    weight 0.774547275310193
  ]
  edge
  [
    source 179
    target 67
    weight 0.776332361829968
  ]
  edge
  [
    source 30
    target 13
    weight 0.769992748397793
  ]
  edge
  [
    source 31
    target 12
    weight 0.75465291930945
  ]
  edge
  [
    source 94
    target 65
    weight 0.75444472104938
  ]
  edge
  [
    source 103
    target 88
    weight 0.759009436972133
  ]
  edge
  [
    source 142
    target 1
    weight 0.781888497743141
  ]
  edge
  [
    source 41
    target 2
    weight 0.753666840445804
  ]
  edge
  [
    source 106
    target 41
    weight 0.756143206451849
  ]
  edge
  [
    source 142
    target 13
    weight 0.754917587075625
  ]
  edge
  [
    source 87
    target 69
    weight 0.775098873445258
  ]
  edge
  [
    source 106
    target 80
    weight 0.756114735819162
  ]
  edge
  [
    source 176
    target 104
    weight 0.771015295394777
  ]
  edge
  [
    source 180
    target 105
    weight 0.753284708381022
  ]
  edge
  [
    source 76
    target 31
    weight 0.756004035571063
  ]
  edge
  [
    source 77
    target 64
    weight 0.75352344599218
  ]
  edge
  [
    source 171
    target 106
    weight 0.780110195836988
  ]
  edge
  [
    source 41
    target 0
    weight 0.753666840445804
  ]
  edge
  [
    source 2
    target 0
    weight 0.851479481186549
  ]
  edge
  [
    source 181
    target 107
    weight 0.79913364075305
  ]
  edge
  [
    source 146
    target 108
    weight 0.755610189646892
  ]
  edge
  [
    source 109
    target 25
    weight 0.750653923676639
  ]
  edge
  [
    source 111
    target 110
    weight 0.773996063943202
  ]
  edge
  [
    source 111
    target 64
    weight 0.764804583291281
  ]
  edge
  [
    source 182
    target 112
    weight 0.779064108613635
  ]
  edge
  [
    source 61
    target 3
    weight 0.767214309307269
  ]
  edge
  [
    source 153
    target 85
    weight 0.794698897322704
  ]
  edge
  [
    source 88
    target 87
    weight 0.759346533544315
  ]
  edge
  [
    source 162
    target 65
    weight 0.764961309360544
  ]
  edge
  [
    source 66
    target 40
    weight 0.750868386899641
  ]
  edge
  [
    source 162
    target 14
    weight 0.776387742348642
  ]
  edge
  [
    source 160
    target 60
    weight 0.751754427012111
  ]
  edge
  [
    source 158
    target 113
    weight 0.78669684754782
  ]
  edge
  [
    source 95
    target 53
    weight 0.856275947896038
  ]
  edge
  [
    source 114
    target 109
    weight 1
  ]
  edge
  [
    source 115
    target 109
    weight 1
  ]
  edge
  [
    source 109
    target 107
    weight 1
  ]
  edge
  [
    source 109
    target 60
    weight 1
  ]
  edge
  [
    source 85
    target 60
    weight 1
  ]
  edge
  [
    source 116
    target 85
    weight 1
  ]
  edge
  [
    source 114
    target 85
    weight 1
  ]
  edge
  [
    source 117
    target 109
    weight 1
  ]
  edge
  [
    source 117
    target 85
    weight 1
  ]
  edge
  [
    source 109
    target 0
    weight 1
  ]
  edge
  [
    source 183
    target 109
    weight 1
  ]
  edge
  [
    source 110
    target 60
    weight 1
  ]
  edge
  [
    source 116
    target 110
    weight 1
  ]
  edge
  [
    source 110
    target 107
    weight 1
  ]
  edge
  [
    source 114
    target 110
    weight 1
  ]
  edge
  [
    source 117
    target 110
    weight 1
  ]
  edge
  [
    source 118
    target 107
    weight 1
  ]
  edge
  [
    source 118
    target 110
    weight 1
  ]
  edge
  [
    source 118
    target 117
    weight 1
  ]
  edge
  [
    source 126
    target 110
    weight 1
  ]
  edge
  [
    source 126
    target 85
    weight 1
  ]
  edge
  [
    source 126
    target 118
    weight 1
  ]
  edge
  [
    source 184
    target 110
    weight 1
  ]
  edge
  [
    source 110
    target 64
    weight 1
  ]
  edge
  [
    source 185
    target 109
    weight 1
  ]
  edge
  [
    source 122
    target 85
    weight 1
  ]
  edge
  [
    source 122
    target 118
    weight 1
  ]
  edge
  [
    source 186
    target 118
    weight 1
  ]
  edge
  [
    source 187
    target 118
    weight 1
  ]
  edge
  [
    source 187
    target 109
    weight 1
  ]
  edge
  [
    source 120
    target 85
    weight 1
  ]
  edge
  [
    source 123
    target 118
    weight 1
  ]
  edge
  [
    source 123
    target 109
    weight 1
  ]
  edge
  [
    source 123
    target 110
    weight 1
  ]
  edge
  [
    source 121
    target 85
    weight 1
  ]
  edge
  [
    source 121
    target 110
    weight 1
  ]
  edge
  [
    source 121
    target 118
    weight 1
  ]
  edge
  [
    source 109
    target 44
    weight 1
  ]
  edge
  [
    source 118
    target 74
    weight 1
  ]
  edge
  [
    source 188
    target 118
    weight 1
  ]
  edge
  [
    source 189
    target 85
    weight 1
  ]
  edge
  [
    source 124
    target 119
    weight 1
  ]
  edge
  [
    source 124
    target 60
    weight 1
  ]
  edge
  [
    source 124
    target 120
    weight 1
  ]
  edge
  [
    source 124
    target 121
    weight 1
  ]
  edge
  [
    source 124
    target 122
    weight 1
  ]
  edge
  [
    source 124
    target 123
    weight 1
  ]
  edge
  [
    source 124
    target 114
    weight 1
  ]
  edge
  [
    source 190
    target 110
    weight 1
  ]
  edge
  [
    source 190
    target 118
    weight 1
  ]
  edge
  [
    source 190
    target 123
    weight 1
  ]
  edge
  [
    source 190
    target 117
    weight 1
  ]
  edge
  [
    source 118
    target 5
    weight 1
  ]
  edge
  [
    source 109
    target 5
    weight 1
  ]
  edge
  [
    source 191
    target 109
    weight 1
  ]
  edge
  [
    source 192
    target 118
    weight 1
  ]
  edge
  [
    source 193
    target 118
    weight 1
  ]
  edge
  [
    source 194
    target 110
    weight 1
  ]
  edge
  [
    source 195
    target 109
    weight 1
  ]
  edge
  [
    source 196
    target 118
    weight 1
  ]
  edge
  [
    source 196
    target 109
    weight 1
  ]
  edge
  [
    source 197
    target 109
    weight 1
  ]
  edge
  [
    source 197
    target 118
    weight 1
  ]
  edge
  [
    source 197
    target 124
    weight 1
  ]
  edge
  [
    source 197
    target 110
    weight 1
  ]
  edge
  [
    source 198
    target 109
    weight 1
  ]
  edge
  [
    source 198
    target 124
    weight 1
  ]
  edge
  [
    source 198
    target 110
    weight 1
  ]
  edge
  [
    source 107
    target 85
    weight 1
  ]
  edge
  [
    source 118
    target 60
    weight 1
  ]
  edge
  [
    source 118
    target 114
    weight 1
  ]
  edge
  [
    source 118
    target 49
    weight 1
  ]
  edge
  [
    source 122
    target 110
    weight 1
  ]
  edge
  [
    source 122
    target 109
    weight 1
  ]
  edge
  [
    source 121
    target 109
    weight 1
  ]
  edge
  [
    source 140
    target 109
    weight 1
  ]
  edge
  [
    source 199
    target 109
    weight 1
  ]
  edge
  [
    source 125
    target 124
    weight 1
  ]
  edge
  [
    source 126
    target 124
    weight 1
  ]
  edge
  [
    source 124
    target 117
    weight 1
  ]
  edge
  [
    source 124
    target 110
    weight 1
  ]
  edge
  [
    source 190
    target 122
    weight 1
  ]
  edge
  [
    source 200
    target 118
    weight 1
  ]
  edge
  [
    source 200
    target 109
    weight 1
  ]
]
